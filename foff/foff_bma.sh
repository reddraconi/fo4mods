#!/bin/bash

AUTHOR=reddraconi
PROJECT=foff
DEV_PATH="/media/fo4mods/foff"
DATA_PATH="/media/SteamLibrary/steamapps/common/Fallout 4/Data"

declare -a folders=(Materials Meshes Textures Scripts Sound Video)

function usage(){
  echo "Usage: $0 [mount|unmount]";
  exit 1;
}

if [[ $# != 1 ]]; then
  usage
fi

for f in "${folders[@]}"; do
  DATA_MOD_PATH="${DATA_PATH}/${f}/${AUTHOR}/${PROJECT}"
  #echo "Using ${DATA_MOD_PATH}"

  if [[ "$1" == "mount" ]]; then
    if [[ ! -d "${DATA_MOD_PATH}" ]]; then
      mkdir -p "${DATA_MOD_PATH}"
    else
      echo "${DATA_MOD_PATH} exists. Not creating."
    fi

    if [[ -d "${DEV_PATH}/${f}" ]]; then
      sudo mount --bind "${DEV_PATH}/${f}" "${DATA_MOD_PATH}" --verbose
    fi

    if [[ -d "${DEV_PATH}/${f}/OPAL" ]] && [[ -d "${DATA_PATH}/OPAL" ]]; then
      sudo mount --bind "${DEV_PATH}/${f}/OPAL" "${DATA_PATH}/OPAL" --verbose
    fi
  elif [[ "$1" == "unmount" ]]; then
    sudo umount "${DATA_PATH}/${f}/${AUTHOR}/${PROJECT}" --verbose

    if [[ -d "${DEV_PATH}/${f}/OPAL" ]] && [[ -d "${DATA_PATH}/OPAL" ]]; then
      sudo umount "${DATA_PATH}/OPAL" --verbose
    fi
  else
    usage
  fi
done
