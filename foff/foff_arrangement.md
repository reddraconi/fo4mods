# Folder Structure Base

The type dictates the subfolder within `$FO4Data/Meshes/reddraconi/foff`,
`$FO4Data/Textures/reddraconi/foff`, etc. The Collection code dictates the
internal folder organization and can be used to split out assets later.

The exception are skins, which are placed in `npc` directories. If there is an
NPC-specific variation of a skin, their name will be the last segment of the
filename.

Material files have extra identifiers used during material swap creation. Their
adjectives should follow the base in the preferred order of color, variant1
&hellip; variant _n_. For example:

```text
Base Item: Holotape                  |   Holotape
Modifiers: Red, Clean                |   Blue, Dirty, Top Secret
Filename: holotape_red_clean.BGSM    |   holotape_blue_dirty_topsecret.BGSM
```

If textures or materials are shared by multiple meshes, they should be pulled
out to a `_shared` directory.

Item names are normalized using the following rules:

- Convert everything to lowercase
- Remove all characters not in [a-z0-9]
- Trim leading and trailing whitespace characters
- Convert all spaces to underscores

| Collection                | Collection Code |
|---------------------------|-----------------|
| Grandad's Beer Collection | GDB             |
| Hunter's Gear             | HG              |
| Holotape Collection       | HC              |
| Magazine Collection       | MC              |
| Pine Harbor               | PH              |
| SuperMutant Gear          | SMG             |
| Jumpsuit Collection       | JC              |


| Collection    | Contents                                               |
|---------------|--------------------------------------------------------|
| Swamp Clutter | Holotape Collection, Magazine Collection               |
| Swamp Foods   | Grandad's Beer Collection, Grandad's Liquor Collection |
| Swamp Gear    | Hunter's Gear, Jumpsuit Collection, Supermutant Gear   |


| Type     | Code | Object(s)                                                |
|----------|:----:|----------------------------------------------------------|
| Item     |  GDB | Bucksweat `{,Empty}`                                     |
| Item     |  GDB | Turpentine `{,Empty}`                                    |
| Item     |  GDB | Pine Tar `{,Empty}`                                      |
| Item     |  GDB | Swamp Water `{,Empty}`                                   |
| Clothing |  HG  | Hunter's Knit Cap                                        |
| Item     |  HG  | Hunter's Knit Cap `{M,F}`                                |
| Static   |  PH  | SMRF Sign                                                |
| Static   |  PH  | Tour Bus `{Blue, White}`                                 |
| Static   |  PH  | Pine Harbor Flag                                         |
| Clothing |  SMG | Army Helmet                                              |
| Clothing |  SMG | Glasses                                                  |
| Clothing |  SMG | Warhawk Hat                                              |
| Skin     |  SMG | Army Fatigues Skin                                       |
| Skin     |  SMG | Clean Undies Skin                                        |
| Skin     |  SMG | Damaged Hazmat Suit Skin                                 |
| Skin     |  SMG | Elder Maxon Skin                                         |
| Skin     |  SMG | Grognak Costume Skin                                     |
| Skin     |  SMG | Hazmat Suit Skin                                         |
| Skin     |  SMG | Kellogg Skin                                             |
| Skin     |  SMG | Jumper Skin                                              |
| Skin     |  SMG | Mayoral Hat                                              |
| Skin     |  SMG | Raider Armor Skin                                        |
| Skin     |  SMG | Willis Undies Skin                                       |
| Clothing |  JC  | Speed Stripes                                            |
| Clothing |  JC  | RobCo 50th Anniversary                                   |
| Clothing |  JC  | Future Future Future Perfect                             |
| Clothing |  JC  |                                                          |
| Clothing |      | Slave Collar (Panther Swamp)                             |
| Item     |      | Scientist Outfit                                         |
| Clothing |      | Scientist Outfit `{M,F}`                                 |
| Item     |  MC  | Cat's Paw `{01..1}`                                      |
| Item     |  HC  | Holotape Grey `{Dirty,Clean}`                            |
| Item     |  HC  | Holotape Red `{Dirty, Clean}` Classified                 |
| Item     |  HC  | Holotape Blue `{Dirty, Clean}` TopSecret                 |
| Static   |      | Curtain `{Beachy, Bubbles, Roses}`                       |
| Static   |      | Curtain Sherbert `{,Patterned}`                          |
| Static   |      | Curtain Thyme `{,Striped,Thatched}`                      |
| Static   |      | Regal Cloth                                              |
| Static   |      | Dualie Pickup Truck                                      |
| Static   |      | Capsule `{Celery,Chocolate,Goldenrod,Lilac,Rouge,Thyme}` |

```text
Example: Hunter's Knit Cap:
  (Mesh) $FO4Data/Meshes/reddraconi/foff/clothing/HG_hunters_knit_cap.nif
  (Mat)  $FO4Data/Materials/reddraconi/foff/clothing/HG_hunters_knit_cap.bgsm
  (Tex)  $FO4Data/Textures/reddraconi/foff/clothing/HG_hunters_knit_cap_d.dds
         $FO4Data/Textures/reddraconi/foff/clothing/HG_hunters_knit_cap_n.dds
         $FO4Data/Textures/reddraconi/foff/clothing/HG_hunters_knit_cap_s.dds
```
