## FOFF Base ###################################################################

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Materials\reddraconi\foff" `
"E:\git\fo4mods\foff\Materials" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Meshes\reddraconi\foff" `
"E:\git\fo4mods\foff\Meshes" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Textures\reddraconi\foff" `
"E:\git\fo4mods\foff\Textures" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Scripts\reddraconi\foff" `
"E:\git\fo4mods\foff\Scripts" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Scripts\Source\reddraconi\foff" `
"E:\git\fo4mods\foff\Scripts\source\" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Sound\reddraconi\foff" `
"E:\git\fo4mods\foff\Sound" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Video\reddraconi\foff" `
"E:\git\fo4mods\foff\Video" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\OPAL" `
"E:\git\fo4mods\foff\OPAL"

## GDBC ########################################################################

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Materials\reddraconi\gdbc" `
"E:\git\fo4mods\gdbc\Materials" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Meshes\reddraconi\gdbc" `
"E:\git\fo4mods\gdbc\Meshes" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Textures\reddraconi\gdbc" `
"E:\git\fo4mods\gdbc\Textures" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Scripts\reddraconi\gdbc" `
"E:\git\fo4mods\gdbc\Scripts" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Sound\reddraconi\gdbc" `
"E:\git\fo4mods\gdbc\Sound" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Video\reddraconi\gdbc" `
"E:\git\fo4mods\gdbc\Video" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\OPAL" `
"E:\git\fo4mods\gdbc\OPAL"

## Hawthornes ##################################################################

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Materials\reddraconi\hawt" `
"E:\git\fo4mods\hawt\Materials" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Meshes\reddraconi\hawt" `
"E:\git\fo4mods\hawt\Meshes" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Textures\reddraconi\hawt" `
"E:\git\fo4mods\hawt\Textures" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Scripts\reddraconi\hawt" `
"E:\git\fo4mods\hawt\Scripts" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Sound\reddraconi\hawt" `
"E:\git\fo4mods\hawt\Sound" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\Video\reddraconi\hawt" `
"E:\git\fo4mods\hawt\Video" `
 /E /MT:16 /R:10 /W:30 /NJH /NJS /Z /MIR

robocopy `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\OPAL" `
"E:\git\fo4mods\hawt\OPAL"

cp `
"C:\Program Files (x86)\Steam\steamapps\common\Fallout 4\Data\reddraconi_foff.esp" `
"E:\git\fo4mods\foff\reddraconi_foff.esp"
