---
title: "Fallout: Kinship"
author: Reddraconi
date: 20231214
lang: en-US
---

[&larr;Previous Chapter](109.html)

# Chapter 10: Nothing Comes Easy

[//]: #(Nothing Comes Easy by Infected Mushroom on Classical Mushroom)

"Fer fuck's sake," Erik slurred holding head &mdash; a new laceration threatened to bleed into his eyes. He kept wiping
at it as he got up beside the freshly dented desk to see what kind of shape the others were in.

Ian and Harmony were both whimpering in a combination of discomfort and fear &mdash; Jenkins landed on Ian's freshly
splinted leg and a limp Rush had pinned Harmony down onto the cold tile floor after shielding them from the blast.
Erik stepped over Ian who looked up with a pitiful whimper, "why is he so heavy?" "Dead weight's funny like that."
He bent down, forcing the room to spin a little, and pried Jenkins off of Ian's legs. "Axel, you okay? We got wounded
and work to do." Jenkins groaned, "Ugh. Why're you bleeding all over? Are *you* okay?" Erik nodded and wiped his
forehead again, smearing blood across his face.

"Shit," Jenkins muttered as he reached to the desk to lever himself up. "Rush. Rush! Wake up!" Erik's shouting was
useless. "Gimme a hand here?" Jenkins nodded, wobbling over to help Erik to roll Rush off of a struggling Harmony,
"Hold his head. He could have a broken neck," Jenkins warned. She sat up after she was freed and began to hit Rush
feverishly on his legs, "Cousin Rush. Wake up!"

Jenkins shrugged pried open one of Rush's eyelids, trying to look at his eyes in the light of his Pip-Boy. "He's out,"
"Must've smacked the desk pretty hard." Jenkins smiled to Harmony, "We can't move him until I've examined him. Why don't
you go check on Ian for me?" She nodded curtly. Now that she was out of the way, Jenkins could perform a better
examination. He straddled the man, feeling under him for anything along his spine out-of-place or broken. As Jenkins was
hovering over Rush's head, he came to with a groan. He looked up from Jenkins' crotch, blinked a couple of times and
grumbled, "feels nice." Jenkins smiled with relief, moving to his side, "How do you feel? Can you see straight? How many
fingers am I holding up?" Rush squinted and replied "three. Maybe." "Close enough, let's get you sitting up, big boy."

Both men grabbed an arm and pulled him up into a sitting position. The quick movement made the room tilt and wobble
strangely to a pale Rush. Harmony looked over from Ian, "uh oh." "Uh oh, What?" Jenkins asked as Rush doubled over and
heaved onto his boots. "That's definitely a concussion." Jenkins muttered in mild disgust. Erik knelt down next to Rush
and rubbed his back in small circles as what little stomach contents he had surged up. "Go check on Dorian, Jenkins.
Rush'll be okay&hellip;It ain't like he's got a crushed skull&hellip;"

Jenkins sighed with a nod, knocking some of Rush's sick from his steel boots. "How're you two," he asked as he squatted
down next to Harmony. She didn't respond but wrinkled her nose and nodded as she gently patted Ian's leg, causing him to
wince. "Oh, that doesn't look right. Let's see." Jenkins kneeled over Ian's splinted leg and felt for the break in his
shin. It had swelled to fifty per-cent larger than when he'd reset it. "Yeah, _that's_ not good, 'lil man. It doesn't
help *my* fat ass landed on you. I"m sorry."

"Okay buddy, enough of that," Erik muttered as Rush dry heaved again, resuming rubbing and patting Rush's back.
"Jenkins, the lab's just down the hall. My coat's in the back room&mdash;there's a pair of stims in th' inside pocket.
I'll stay here with the young'uns and Rush here." "Fine," Jenkins frowned, "Mop up that cut on yead and don't try to
tourniquet anyone while I'm gone."

* * *

Franklin prepared to through his fifth apartment near Rush's abode. He entered through the sliding door, surprised to
find this one was pristine and almost dust-free. It smelled faintly of roses, lavender, and a hint of dogs. As he tread
slowly into the double-wide apartment, he realized someone had been living there recently; plates with morsels of food
stood on the small square kitchen table, children's toys and books were scattered near a bookshelf, and all three beds
were made to military spec. "Who the hell's livin' in a shithole like this with *kids*," Franklin thought aloud.

The elder yelled for him. "Franklin, lunch time!" Franklin strode back through the door and yelled back, "Aye, sir!"
Stretching in the artificial sunlight of the complex before walking back to their temporary base.

As he swung open the door, the Elder was wearing a wide grin. "Found somethin'?" "A stash of Fancy Lads<sup>tm</sup> and
what blueprints for something you may be interested in. You?" "A bushel of holotapes, found some notes on some sort of
research they were doin' here; didn't understand it, and uh, a plasma rifle under a bed next to what I assume is Erik's
bag?"

"You got it with you?" "Yeah," Franklin hoisted it off his shoulder, "here." The elder opened it the main pocket and dug
around. "It's Erik's." "How'd you know?" The elder pulled out a photo of himself, Erik, Franklin, his deceased brother
Chase, and Jenkins on Erik's first hunting trip at age ten. "Good luck, he says." Franklin took the photo from the Elder
and smiled at the memory: a chilly winter day blew in from the North bringing with it fowl and a more than a few
creatures on the prowl for mates. They stumbled across a pair of bucks fighting over a doe and decided to finish the
fight for them. That was Erik's first kill. They bled one on the spot, drawing up war paint on all of them as a rite of
passage." Franklin smiled inwardly and handed the photo back. "I miss that. Good times."

Elder Redding nodded, "good eating too." "Speaking of, you mentioned lunch?" "How about snack cakes and Nuka with half
an MRE?" "Beats starving," Franklin scoffed. As they milled around in the apartment, gathering chairs and plates, Elder
Redding asked, "you found some research material?" Franklin nodded, handing him a cup for his flat soda, "I didn't
understand it any further than I coulda thrown it." "You see any names attached to it," the elder asked. "Uh, dunno,
it's in Erik's pack."

Franklin reached for one of the chocolate-frosted snacks as the elder leafed through the ancient papers. Franklin
gobbled down his first cake and reached for a second. The elder squinted at unassailable pages of code interspersed with
hand-written notes. He didn't immediately recognize anything. "This'll keep the scribes busy for a while."

Franklin washed down his second snack cake with a swig of soda, "you think we should go lookin' after 'em?" The elder
looked up from the codes, "They're adults. Enjoy the break while you can. I have the feeling that you're going to have
your hands full once we get back home. How've you been holding up?" Franklin thought of Erik's photo of their hunting
trip and his brother. "Been better. I kinda know how the big guy feels, losing family like that."

The Elder closed the manila folder, "if you ever feel like you need to drink yourself to death again, please talk to me.
I know you don't like talkin' with your daddy about things that." "Aye, sir", he mumbled before shoving a third cake
into his mouth.

* * *

After inspecting his brother's tender cranium, Erik decided that some swelling had gone down and nothing seemed
especially broken. Harmony and Ian sat opposite of Erik and Rush propped up against the cool metal desk. They thought
back and forth to one another, "What do you think of him?" "I dunno." "Me either, he's nice though, reminds me of Cousin
Rush." "Yeah, me too. Why's he not got fur like us? What's that glowy thing on his arm?" "Dunno&hellip;" Harmony paused
their conversation. "Cousin Erik?"

Erik looked up from his Pip-Boy. "Yeah? Y'all okay?" Harmony and Ian nodded simultaneously. "How is Cousin Rush?" Rush
wobbled, but vomiting and dry heaving. Erik pulled Rush's bile-stained muzzle to one side to widen an eyelid. Rush's
eyes were still heavily, unevenly dilated. "He's got a concussion, but he ain't gettin' any worse or bleedin' from his
ears or anything, so I don't think there's any _serious_ swelling going on. He's just got to shake it off."

The two cubs nodded slowly in tandem; Erik found it was unnerving. Harmony resumed up her line of inquiry, "Why do you
smell like Cousin Rush?" "Like I said earlier, it's probably 'cause we've been been spending a lot of time together."
She shook her head, "no. You smell like us, our clan." "I reckon it's because I'm your kin. Rush and I are
half-brothers. Apparently you two, me and Rush are cousins. I think. I didn't know I even had a sibling let alone
cousins."

"What happened to grandfather?" Ian's ears perked up. "I dunno, but I'm pretty sure he couldn't've made it through that
blast. It was part of his body's programming to protect its secrets by exploding like it did&hellip;there's *no way* his
brain could've survived that."

Ian looked at his lap after Erik responded, but Harmony pushed on. "Are there more like you and Doctor Jenkins?" Rush
nodded to Harmony's question, making the room slosh. Erik patted Rush on the back and replied for him, "yeah. There're
two more folks like us that came here. One's my&hellip;adopted father. The other is a man Jenkins and I grew up
with; John Franklin. He taught us to track animals and shoot weapons to defend and feed ourselves. Protect people. He's
rough on the outside, but he's a good guy at heart."

"Can I ask *you* some questions?" Harmony stowed hers and nodded, "Uh, okay." "Have y'all lived down here forever? Have
you ever left?" The siblings shook their heads. "Not allowed," Harmony said sadly. "Only a few were allowed to leave.
Mommy and Daddy went all the time to go hunting and scavenging for the clan. Not anymore though&hellip;"

Erik nodded, "what about the Elder? What's she do?" Ian sighed deeply, "she made do school. A lot. Made us read and do
math and boring stuff." Harmony smiled, "the math part was boring, but I liked the science though. Mixing all those
things together was fun." Harmony nodded and continued, "She is our teacher. She also settled all of the arguments in
the clan." Harmony paused and smiled mischievously at Rush, "cousin Rush knows more than *anyone* about that."

Rush, fighting to shake of the concussion mumbled, "Never agreed," he grumbled. "You? Really? I never woulda guessed.
You've quiet until something makes you upset," Erik replied. Rush nodded to the children, "Must be good example."

"Cousin Erik, what's the outside like?" Ian and Rush both perked up at Harmony's question. "That's a tough question. The
outside's a lot of things: at night, it's beautiful. You can see millions of bright, shining stars twinkling in the sky.
During the day, if the weather's not bad, you can see for miles across the swamps in some places. My favorite time's
the fall when the trees start to change colors and drop their leaves."

Harmony sighed at the romantic landscape. Erik continued, "it's also a _very_ dangerous place. Since the Great War,
people have been hurting each other to get what they want. There're murderers, rapists, psychos, slavers, druggies, and
worse out there. Not everyone knows how to live with others&mdash;some folks only care about themselves."

The children stared at Erik; Ian whimpered slightly, "I&hellip;don't want to go outside anymore&hellip;" Rush spoke on
Erik's behalf, "not all are bad. Erik and his clan protect others." "Yeah! My 'clan', the Brotherhood, protects people
from those bad people. When we're not scouting for supplies or technology we help anyone that's hungry or sick or
homeless. We're not really like many other clans in the wastes."

Ian asked, "Do your elders not bring things back for you?" Erik shook his head, "no&hellip;not really. We've
lost&hellip;a lot of members in the last few years due to those dangerous things I told you about. It's my job to find
new things for our clan that we can use to keep us safe. That's why I came here. I was told there was some sort of
something down here that we could use to help a lot of people on the outside&hellip;I'm starting to wonder if that's
true though."

Rush reached uncertainly to tousle Erik's hair, "found us, brother." Erik genuinely smiled and nodded, "Yeah, found my
family, too."

Jenkins slogged through another dark hall when he finally decided to radio Erik. "Erik, come in. It's Jenkins.
I think&hellip;I'm lost." No reply. "Damn it Erik. I _really_ need a hand here." Nothing . Jenkins sighed a low
"fuuuuuck" to himself. "Wish I'd asked Erik if he had a copy of the schematics. Stupid scribes mangled my maps again.
'Contact an administrator' my ass." As he continued to walk through the stuffy hallway, he tried a third time. "Erik?
Jenkins. Are you there?"

As he whining to his radio, his feet carried him through the halls on their own. He nearly tripped on the guts of a
terminal that were scattered across the concrete floor. Small rivulets of oxidizing blood dripped on wall. He was
absentmindedly poking at his Pip-Boy's screen when it notified him of a new radio signal. An emergency broadcast:
a prerecorded statement not unlike something that belonged on a holotape.

::: {.audio_output}
Notice: Specimen tank number twelve contaminated.\
Warning: Specimen number twelve health stats borderline. Repeating&hellip;\

Notice: Specimen tank number twelve contaminated.\
Warning: Specimen number twelve heath stats borderline. Repeating&hellip;\
:::

"The hell's this all about," Jenkins asked himself. He peeked in through the door into a lab, complete with large vats
of glowing&hellip;stuff. One of the ones in the far corner was different from the others; it was much dimmer. "That's
odd," he walked into the lab to investigate. It held a specimen that looked vaguely like Rush&mdash; longish limbs,
healthy musculature, but it was more slender and much older. Maybe it was female? Was it alive?

He tapped on the tank inquisitively, "Hello?" The occupant reached out to place a palm against the inside of the glass
door. Jenkins recoiled at the response&mdash;he wasn't expecting one. "Uh..hello? Are you&hellip;alive in there? Do you
need help? I'm a medic&hellip;I can try to help&hellip;" The female inside the specimen tank tapped twice on the glass
sluggishly gesturing through the thick gel toward the attached terminal.

Jenkins suspiciously tapped at the keyboard, waking the screen.

::: {.terminal_output}

Station Magnolia Terminal Server (07)\
Specimen Management :: Tank 12

&gt; Specimen Statistics\
&gt; Release Specimen\
&gt; Cleanse Specimen Tank\

:::

"'Release Specimen' sounds about right, he mumbled."

A vacuum pump nearby spun up with a slurping, sucking sound. Within a minute,, the tank was nearly completely drained of
its azure, oxygen-rich gel and given a quick spray down with sterilized water. After its rinse cycle was complete, the
door on the cylindrical tank slid forward and up, revealing a grey-furred female. The specimen stumbled across the thick
threshold, grumbling and blinking. "*Wer stort meinen Schlaf? Wer sind Sie?!*" Jenkins was flabbergasted, "Uh, sorry.
What? I wanted to make sure you were okay&hellip;"

She glared at him, repeating her question slowly, "*Wer. Sind. Sie?*"

"I don't understand &hellip;" She shook her head exasperatedly, sending droplets of water flying from her fur. She
patted her chest, "Elena." "Oh! I'm Jenkins, Elena." She nodded and pointed to the Brotherhood of Steel logo on his
chest plate. "Erik?"

"You know Erik?" She answered his question with a nod and a question of her own, "*Wo ist Rush und my kinder? Sind Sie
mit Erik?*" "You know Rush too? Who _are_ you," Jenkins asked. She shoved past Jenkins, shaking the last of the water
from her coat. She muttered "*Muss meine Kinder zu finden&hellip;*"

"Ms. Elena, let me look at those lacerations. Some of them look pretty deep." One was already dribbling blood into her
coat. She completely ignored the lesser creature's bleating it grabbed her left shoulder. Spinning around with a snarl
and a pant-soiling growl, she snapped her maw near the end of Jenkins' nose, "*Nicht!*" He froze in place and stared,
wide-eyed at her momentarily before whispering "I'm sorry. Just doing my job." The elder squinted at him, then very
slowly waded through some basic English after backing away a few inches. "Where my children?"

Jenkins blinked a few, "Melody and Dorian?" She nodded. "They're with Rush and Erik, I was on my way here to get some
medicine for the lot of them." A flash of panic streaked across her maw. "Show me!" "Uh, okay, sure. Just remember,
I can't show you where they are if you kill me or anything," Jenkins mumbled sheepishly. He started to walk the hall and
paused, "Do you know where the 'back' area is here? Erik said there's some medicine in there&hellip;" The elder silently
pointed Jenkins toward the North wall where the double doors hung slightly ajar. "Thanks. Be right back." Jenkins jogged
off to the back room while the elder stood in place and waited on the irritating human.

Jenkins scrambled around the back room&mdash;his stomach churned at the scents of vomit and coppery blood. He finally
Erik's trench coat in the bathroom. He donned the coat and rushed out to meet the Elder, gripping the stimpacks. He
stopped to her left, "I'm ready, if you are, ma'am." She nodded through a knitted brow waiting for him to lead.

It was a long walk back to the office. The elder refused to answer any of Jenkins' questions, or look at him in a way
that didn't make him think he was going to bleed to death in some gruesome way. After a few wrong turns, they finally
found the correct hall.

As they grew closer, the duo heard the delightful laughter of children. Jenkins smiled to himself, there were few
children in town. Erik was telling his furred companions about the time they got drunk and lost in the red light
district of a trading hub far southwest of their base, the Crescent. The city had once been known as New Orleans, before
it flooded; the city now consisted of buildings interconnected across the Crescent Lake by boats and bridges.

"Franklin an' I had gone to the Pirate's Booty, one of hotel-bars down in the Crescent. We didn't know it was _that_ of
hotel. Jenkins had gone in and demanded a room, thinking that he was back at our hotel." Erik chuckled, "well, in
a place like that, you get a _friend_ to go with a room. By the time we'd found 'im, his _friend_ had gotten half of his
clothes off and was about to&hellip;."

Erik paused, noticing Jenkins at the door, huffing angrily. "Hey there," he said sheepishly, "glad you made
it&hellip; you find the stimpacks?" Jenkins glowered at Erik as he fought to get out of the trench coat. "Yeah, here,"
he threw the trench coat at Erik, "Do you have to tell _everyone_ that story?" Erik smiled, "not _everyone_. We just
needed something to lift everyone's spirits. It's been a shitty day so far."

The elder pushed past Jenkins to stand in the middle of the small office and stare daggers at Rush. He looked up from
his lap, causing his vision to dip and swim. Their Elder was displeased, and she let him know mentally in very stern
German. He dipped his head, pinning his ears. She turned her attention to Erik and did the same. Erik shook his head
violently. "Ma'am, I don't understand, but I gather that you ain't happy." She quickly snapped her gaze back to Rush and
demanded verbally with a sneer, "*Du übersetzt.*" He nodded, avoiding her gaze.

Rush thought to Erik, "We are a disappointment. She is displeased we permitted injury to the pups, not including our
own injuries." Erik imitated Rush's averted gaze and spoke softly to the elder. "Ma'am. I've got no excuse other than we
did our best to keep 'em alive." She glared at them both and then nodded slightly, her demeanor softening a fraction.

The Elder's entire attitude flipped she knelt between Harmony and Dorian. She looked at them and smiled as she patted
their heads lovingly. "*Wie sind meine Kinder?*" Harmony replied verbally to their aunt. "We're okay. Cousins Rush and
Erik did their best. Please don't be mad! Their friend Mr. Jenkins helped too, he put Ian's leg back together!" The
elder nodded at Harmony and then looked over to Dorian. "*Wie ist dein Bein?*" He looked down, ashamed. "It hurts,
but&hellip;they _did_ do their best, auntie." She nodded and patted him softly on the cheek with a warm smile.

She spun around, dissolving her kindness to stare at Jenkins. "Repair. Now." "That's what these are for," he replied
quietly, pulling out the stimpacks. "One for you," he pointed to Dorian, "and the other for you," he pointed the same
stimpack at Rush. "Children first," he mumbled aloud as he knelt down next to Dorian under the critical eye of their
Elder.

"This is gonna burn, but not for long. Give it ten minutes, and you'll be able to run around after we get that sling
off," Jenkins said sweetly. Dorian looked up at the elder and whimpered. She nodded at him, instantly stifling his
complaints. "Ready? On three&hellip;" Jenkins gently placed a hand on Ian's trembling leg to steady it as he deftly
jabbed the pup's thigh with the stimpack, "three." Dorian cried loudly but tapered off quickly as his fractured leg
healed itself. "You said it was on three&hellip;," Ian whined. "Sorry about that," Jenkins apologized, "Better if you
don't see it coming," as he freed Dorian's leg from the sling.

"Walk around a bit; should be good as new, little man." Dorian looked up at the elder for permission, she nodded.
Harmony helped her brother up to limp around on his freshly-mended leg. The elder returned her gaze to Jenkins "Rush."
"Right, right. Rush's turn," Jenkins muttered with an underscore of sarcasm, "I'm working on it".

Jenkins slid across the floor to Rush and Erik. "He's still concussed?" "Yeah, I think so," Erik replied. Jenkins nodded
as he pried open Rush's eyelids for inspection. "Bleeding? Nose, ears? Open up." Rush yawned wide for Jenkins. "Good,
good. You won't need this stimpack&mdash;it's a minor concussion. You can have some painkillers, though." Rush nodded
his thanks after Jenkins finished probing his ears and mouth. "Here," he offered Rush a dose of Buffout and a canteen
of water.

Rush gobbled the pills and drowned them with water. Jenkins took back the empty canteen and returned it to his bag. He
looked over his shoulder to their elder, "All better." She watching Harmony and Ian playing as if nothing
Earth-shattering recently happened. They found a toy car in the desk and were currently racing it along the top. The
elder smiled, offering Jenkins a hand up. "*Danke, Herr Jenkins.*" "Of course," he nodded, noticing her bleeding had
worsened as she pulled him up.

Erik helped Rush to his feet. He was still unsteady, but able to wobble along under his own power. The elder looked at
Rush, then her two cubs, Erik, and finally Jenkins, "*wir gehen Zuhause.*" She didn't wait for any questions or
complaints as she waltzed through the door. Ian and Harmony quickly followed suit Rush fumbled after them, using the
walls and Erik for support. Jenkins stood alone in the office momentarily before grabbing the toy car, stashing it in
his medical bag.

[Next Chapter &rarr;](111.html)
