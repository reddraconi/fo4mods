#!/bin/bash

if [ "$#" -lt 1 ]; then
  echo "Usage: $0 <markdownfile to convert>"
  exit 1
fi

if grep -q "TODO" $1;  then
  echo "Found TODOs in $1. Not converting!"
  exit 1
fi

filepath=$(basename -- "$1")
filename="${filepath%.*}"

pandoc -i "$1" -f markdown -o "../chapter_exports/$filename.docx" \
  --reference-doc ./_fob2_template.docx
