param(
    [parameter(Mandatory=$true)]
    [String]
    $input_file
)

if (get-content $input_file | select-string -pattern "TODO" -quiet){
  write-warning "FOUND TODOs IN $input_file. Not Converting!"
  get-content $input_file | Select-String -pattern "TODO" |
    Select-object LineNumber
}else{
  $newfile =
  [System.IO.Path]::GetFileNameWithoutExtension($input_file)+".docx"

  pandoc -i $input_file -o ../chapter_exports/$newfile --reference-doc .\_fob2_template.docx
}
