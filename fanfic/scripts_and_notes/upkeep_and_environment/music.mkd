---
Country music from 1940s and 1950s
...

- 1930 - 1939
  - Body and Soul - Johnny Green, Louis Armstrong
  - Georgia On My Mind - Hoagy Charmichael
  - Frenesi - Alberto Dominguez

- 1940 - 1949
  - How Deep Is The Ocean - Coleman Hawkins
  - Swinging On a Star - Bing Crosby
  - Chattanooga Choo Choo - Glenn Miller
  - Tangerine - Jimmy Dorsey
  - I've Got a Gal In Kalamazoo - Glenn Miller
  - Swinging On a Star - Bing Crosby
  - Rum and Coca-Cola - The Andrews Sisters
  - You're Breaking My Heart - Vic Damone
  - C Jam Blues - Duke Ellington
  - Ev'ry Time We Say Goodbye - Cole Porter
  - Everything But You - Duke Ellington, Harry James
  - Do You Know What It Means to Miss New Orleans - Louis Alter
  - Yardbuird Suite - Charlie Parker
  - C'est si bon - Henri Betti
  - My Foolish Heart - Victor Young

- 1950 - 1959
  - The Tennessee Walts - Patti Page
  - Don't Let the Stars Get in Your Eyes - Perry Como
  - Mr. Sandman - The Chordettes
  - The Yellow Rose of Texas - Mitch Miller
  - Heartbreak Hotel - Elvis Presley
  - Don't Be Cruel / Hound Dog - Elvis Presley
  - (Let Me Be Your) Teddy Bear - Elvis Presley
  - Puppy Love - Dolly Parton
  - Fly Me To The Moon - Bart Howard
  - Four - Miles Davis
  - The Best Is Yet To Come - Cy Coleman, Crolyn Leigh
