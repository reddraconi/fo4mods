#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;
use Lingua::EN::Words2Nums;

open(OL, "<", "./outlineOfEvents.ol")
  or die "Unable to open \"outlineOfEvents.ol\"\n$!\n";

my (%chapters, %dates);
my (@locations, @characters, @items) = ();
my ($title, $data, $currentChapter, $currentDate, $currentTime) = "";

# chapters{
#    title => "",
#    dates{
#				'<time>' => data,
#				'<time>' => data,
#				...
#    }
#    @items;
#    @characters;
#    @locations;
# }

while(<OL>){

  # Ignore informational stuff, comments, and blank lines
	if($_ =~ /\s+\+.*/   or
	   $_ =~ /\s+\|.*/   or
		 $_ =~ /^#.*/      or
		 $_ =~ /.*#TODO.*/ or
	   $_ =~ /^$/        ){ next; }

	# Grab chapter headings (markdown H1) when encountered
	if(/^=/){
		chomp;
		# If chapters are combined, then we need to split them up.
		# We expect chapters to be " / " separated. Max of two.
		if(/.*\/.*/){
			my @chapters = split("/",$_);
			foreach (@chapters){
				$_ =~ s/= Chapter (.*)/$1/g;
				$currentChapter = "Chapter_".words2nums($_);
				$chapters{$currentChapter}{'title'} = "Chapter $currentChapter";
			}
		}else{
			$_ =~ s/= Chapter (.*)/$1/g;
			$currentChapter = words2nums($_);
			$chapters{$currentChapter}{'title'} = "Chapter $currentChapter";
		}

		# Empty the children objects, since this is a new chapter.
		%dates = ();
		(@items, @characters, @locations) = ();
		next;
	}

	# Grab date portion, when encountered
	if(/^-/){
		chomp;
		$currentDate = $_;
		$currentDate =~ s/- (.*)/$1/g;
		chop;
		$currentDate =~ s/ /_/g;
		next;
	}

	# Grab time portion
	if(/^.{4} ::/){
		chomp;
		($currentTime, $data) = split("::",$_);
		$currentTime =~ s/ //g;
		$chapters{$currentChapter}{$currentDate}{$currentTime} = $data;
		next;
	}

	# If the line is wrapped, then we need to append the data to the current
	# hash entry.
	if(/^\s+.*/){
		chomp;
		$_ =~ s/\s+ (.*)/$1/g;
		$_ =~ s/ +/ /g;
		chomp;
		$chapters{$currentChapter}{$currentDate}{$currentTime} .= " $_";
		next;
	}

	# We need to pull characters from the current Data entry to input them into
	# an array. If it's already there, then skip it!
	#		Format: {[a-zA-Z _-}
	#
	# We need to find items from the current $data entry to input them into an
	# array.
	#   Format: (general item type : quantity, general item type : quantity, ...)
	#   hash: items{
	#           general_item_type{ item => qty,
	#                                   => desc
	#           }
	#         }
#	print "DEBUG :: $_";
	sleep 1;
}

print Dumper \%chapters;

close OL;
exit 0;
