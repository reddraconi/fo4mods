#  +----------------------+  +----------------------+  +----------------------+
#  |       JUN 2280       |  |       JUL 2280       |  |        AUG 2280      |
#  | Su Mo Tu We Th Fr Sa |  | Su Mo Tu We Th Fr Sa |  | Su Mo Tu We Th Fr Sa |
#  |        1  2  3  4  5 |  |              1  2  3 |  |  1  2  3  4  5  6  7 |
#  |  6  7  8  9 10 11 12 |  |  4  5  6  7  8  9 10 |  |  8  9 10 11 12 13 14 |
#  | 13 14 15 16 17 18 19 |  | 11 12 13 14 15 16 17 |  | 15 16 17 18 19 20 21 |
#  | 20 21 22 23 24 25 26 |  | 18 19 20 21 22 23 24 |  | 22 23 24 25 26 27 28 |
#  | 27 28 29 30          |  | 25 26 27 28 29 30 31 |  | 29 30                |
#  +----------------------+  +----------------------+  +----------------------+

= Chapter One

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |          <9 10 11 12 |
  | 15>                  |
  +----------------------+

- 09 JUN 2280

0900 :: {Erik} given mission to investigate abandoned [Magnolia Research
        Facility].

- 10 JUN 2280

0600 :: {Erik} leaves Delta station [The Burg], about three day's north of the
		    [Magnolia Research Facility].

- 13 JUN 2280

0400 :: {Erik} takes cargo elevator down to basement, alarms ring

0730 :: {Erik} finds terminal to mainframe, downloads corrupted info

0845 :: {Erik} running on 5th subfloor, away from "something"

0910 :: Attempts to contact Brotherhood of Steel Delta station via shortwave on
        Pip-Boy. Signal breaks down due to depth underground & interference

0920 :: {Rush} drops off "meal" at storage closet door for {Erik}, only knowing
        that {Erik}'s hungry. Was able to "smell the hunger."

1000 :: {Erik} records Pip-Boy message number 003 as a "if I die down here"
        message for scavengers to later find.

1020 :: {Erik} Leaves storage closet, heads North toward offices

1025 :: Arrives at [Storage 1] Finds (Documents: 4 holodisks 3 bad & 1 Project
        Maia 6/7)

1100 :: {Erik} Listens to (Document: Project Maia 6/7).

1230 :: Rummages in [Storage 2] Nothing found.

1430 :: Enters [Security], hacks RobCo Terminal. Finds & saves corrupted message
		    from {John Hawthorne}, his uncle.

1500 :: Rummages through lockers finds (10mm ammo: 10 rounds, "Cat's Paw": 1,
        10mm pistol silencer)

1520 :: Kills radroach

1600 :: Takes a piss

2000 :: Finds items left by Penny Hawthorne:
        (beef jerky: 2 bags, Nuka-Cola: 2, holodisk: Project Maia 7/7, Document:
        Penny Hawthorne Photograph, Document: Genetic Code, Document: map,
        Pre-War cash: $100)

2015 :: Eats (jerky: 1 bag, Nuka-Cola: 1)

2030 :: {Erik} sees {Rush} for the first time

2035 :: Listens to (Project Maia 7/7 holodisk)

2050 :: {Erik} sees {Rush} for a second time

2100 :: {Erik} leaves toward freight elevators in cargo storage

2130 :: {Erik} enters [Apartment Complex]

2140 :: {Erik} enters [Apartment: "Robin's Egg Blue"]. Finds (Nuka-Cola: 6)
        Consumes one immediately.

2200 :: {Erik} enters [Apartment: "Emerald Green"]. Finds (Bag of Chips: 3)

2205 :: Finds (Document: Hawthorne's photo).

2225 :: {Rush} finally finds his "prey."

= Chapter Two

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |<13 14>               |
  +----------------------+

- 13 JUN 2280

2230 :: First encounter /w {Rush}. No bullets fired. (jerky) offered & shared

2235 :: {Erik} begins his investigation. {Rush} speaks for the first time.
        {Erik} continues investigation.

2255 :: Handshake between {Erik} & {Rush}.

2310 :: {Rush} leaves to get "mom."

2325 :: {Rush} returns /w (urn).

2355 :: {Rush} & {Erik} bond while mourning over dead grandmother

- 14 JUN 2280

0000 :: {Erik} attempts to contact Delta again, no luck.

0005 :: {Erik} requests a bed, {Rush} calls pack to inspect

0015 :: Pack arrives, {Elder Elena} inspects {Erik}. She gives {Rush} the OK
        to let {Erik} stay. {Erik} attempts to introduce himself to {Elder
        Elena}, almost gets his ass kicked.

0030 :: {Erik} follows {Rush} to his home / workspace.

0045 :: {Erik} meets {Dr. Hawthorne}- a RoboBrain model robot. Interrogates
        him.

0130 :: {Dr. Hawthorne} leaves to his [charging station] in the center of
        complex

0135 :: {Rush} invites {Erik} into his home, {Erik} requests a shower

0145 :: {Erik} gets ready for shower, {Rush} investigates {Erik}.

0150 :: {Erik} gets a good sniffing from {Rush}, scolds him immediately
        afterward.  Apologizes & requests {Rush} leave for his shower time.

0200 :: After some head scratching, {Erik} obliges {Rush} joining him. Cleaning
        & accidental abuse ensue.

0230 :: {Rush} & {Erik} are clean, go to bed.

= Chapter Three

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |      <15>            |
  +----------------------+

- 15 JUN 2280

0745 :: {Rush} wakes up & gets {Erik} up, morning wood issues.

0750 :: {Rush} goes to get {Erik}'s things, realizes he smells like "mom" did.

0755 :: {Erik} apologizes, begins to clothe himself.

0800 :: {Rush} gets pissed & demands {Erik}'s help.

0815 :: {Rush} & {Erik} sit down to a breakfast of (Nuka-Colas: ?, chips: ?).
        {Erik} realizes that {Rush} resembles a werewolf.

0835 :: {Erik} reviews his inventory. The duo leave toward the [center of town]
        to find {Dr. Hawthorne}.

0840 :: {Erik} & {Rush} catch up /w {Dr. Hawthorne} at the entrance to the
        [offices].

0850 :: {Dr. Hawthorne} re-enables power to the offices

0855 :: The trio begin their trek back toward where {Erik} started the morning
        before. {Dr. Hawthorne}, {Erik}, & {Rush} commune on their way to the
        [genetics laboratory].

0905 :: Pass old "meal" by storage closet. Also pass some traps.

1015 :: {Erik}'s restroom break.

1028 :: The trio arrive at the [genetics laboratory] entrance.

1035 :: {Erik} finds out he's {Dr. Hawthorne}'s grandson, hints that he's kin to
        {Rush}. Uses some of his blood to open biometric lock on lab door.

1055 :: The trio enter the lab. They turn their attention toward the chalkboards
        with names. {Erik} finds more leads into his lineage- confirms
        familial relation to Dr. & {Rush}. Sorts out some issues, finds more
        time line-related questions.

1145 :: The trio enter the [genetics laboratory].

= Chapter Four

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |      <15>            |
  +----------------------+

- 15 JUN 2280

1200 :: {Erik} begins flashing back, becomes ill. {Rush} & the Dr. take him back
        to the [on-call room].

1210 :: {Rush} bathes {Erik}, places him in a bunk.

1215 :: Dr. injects {Erik} /w (serum) to enable his latent abilities.

1245 :: {Erik} comes to, immediately begins to hallucinate.

1300 :: {Rush} sings "I Will" to {Erik}, as their mother had done long ago.

1330 :: {Erik} finally goes passes into a drug-addled sleep. {Rush} leaves to
        find the Dr. & answers. Dr. explains their creation, a bit of BG
        on the elder of the pack, their mother, a bit of {Rush}'s history.

1400 :: {Erik} seizes. {Rush} sprints to help. {Erik}'s bleeding from his ears
        ad nose, seizing more. {Rush} constrains {Erik} who beings shrieking.

1404 :: {Erik} stops seizing, goes into weak state

1410 :: {Rush} goes to find Dr.. Unable to do so, he remembers that
        adrenaline should help fix {Erik}'s condition. {Rush} finds some
        adrenaline & sprints back to {Erik}.

1420 :: {Rush} injects (adrenaline) directly into {Erik}'s heart.  {Erik}
        understandably panics, has no idea what's going on, scratches the hell
        out of {Rush} in panic.

1435 :: {Erik}'s body calms down a bit, he comes into mostly-full consciousness.
        {Erik} begins the vestiges of telepathic communication, "geistlink".
        {Erik} & {Rush} talk for a bit. {Rush} grabs {Erik} some clothes to sate
        him.

1500 :: {Erik} & {Rush} arrive at (bathroom).

1515 :: {Erik} explains condoms & lube to {Rush}. {Erik} learns that {Rush} can
        hear his thoughts, feels mentally raped.  {Rush} apologizes for the
        intrusion. {Erik} tries to do the same, not much of a result, but good
        start. {Rush} continues to search through lockers.

1525 :: {Erik} picks the lock on the second locker finds items.  None are taken,
        but {Erik} wears the (grey jumper) & (boots).

1535 :: {Erik} enters shower, {Rush} finds joys of ("Cat's Paw").

1600 :: {Erik} gets out of shower, {Rush} catches him half clothed.  They talk
        about {Erik}'s embarrassment /w his body, Rus jumps into shower.

1610 :: {Rush} jumps out of shower. The duo head back toward the lab.

= Chapter Five

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |      <15>            |
  +----------------------+

- 15 JUN 2280

1630 :: {Rush} & {Erik} make it back to the lab.

1635 :: They find the Dr. rambling about stuff. They attempt to get some
        sense out of him. No dice, he's got issues.

1650 :: {Erik} opens the Dr. up & finds that his brain suspension gel is
        infected, Dr. got into fight /w elder of clan- attempted "cleanse"
        routine. {Erik} attempts a quick fix on Dr., works. {Erik} & {Rush}
        leave him in the safety of the on-call room.

1715 :: The duo leave toward the apartment complex, attempt to find the clan.

1720 :: They stop by the previous bathroom to collect items. Found (personal
        lube: 1 [70% full], condoms: 2, Document: "Cat's Paw", greasy shorts: 1
        pair, bobby pins: 3, jumpers: 2[grey, red], sunglasses: 1 pair, pre-war
        money: $20, boots: 1 pair)

1725 :: Duo resumes their journey to [apartment complex]. Chat for a bit,
        possibility of getting {Erik} inducted into the clan as a proper
        memeber. Stopped for this chat.

1746 :: They reach the door back into the [apartment complex].  Doesn't open.
        {Erik} fiddles /w the control panel, {Rush} decides to thrash a crowbar
        in it instead. They squeeze through the half-open door into a world of
        madness.

1800 :: Quick chat, {Rush} howls for clan. Only one howl in response. It's
        {Elder Elena}-- she's trapped under rubble /w the two youngest members
        of the clan.  Others have been slain.

1810 :: {Erik} is re-inducted into the clan.

= Chapter Six

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |      <15>            |
  +----------------------+

- 15 JUN 2280

1813 :: {Erik} directs {Rush} to take the remainder of the clan to a safer area.
        They go back toward the [office spaces].

1815 :: {Erik} gets comms from Delta Station. They confirm hostile attack by
        Enclave. Promise backup.

1817 :: Enclave forces exit elevator. Five men. 1 sniper, 2 standard troops, 2
        scientists in pre-war suits.

1825 :: Enclave troops split up, two scientists set up black box

1830 :: First soldier taken down by blow to head /w (crowbar).  {Erik} drags him
        into an apartment, strips him down & takes (plasma rifle: 1, MF cells:
        7, caps: 15, laser pistol: 1, Enclave dog tags: 1)

1840 :: {Erik} wastes a scientist /w his new plasma rifle, gives his position
        away. Tosses rifle under bed, runs to another apartment. Nearly takes a
        .308 round to the head.

1847 :: {Rush} gets back, converses /w {Erik} mentally, serves as a distraction.
        Cargo elevator is on the move again.

1850 :: {Rush} tracks down another solider, rips his throat out /w glee. {Erik}
        runs to another aparment. The cargo elevator dings.

1853 :: Sniper clips {Rush}. {Erik} freaks out & runs back in his direction.
        Cargo elevator arrives again unkown contents.

1855 :: BoS pallies spill out of elevator, {Erik} yells the enemy's positions to
        them. One of three pallies help {Erik} drag a bleeding {Rush} into a
        nearby apartment.

1900 :: {Erik} speaks /w {Elder Redding}, thinks its a good idea to tourniquete
        a bullet wound in the shoulder causes more pain for {Rush}. {Elder
        Redding} removes tourniquete & scolds {Erik}. Fire is intermittently
        exchanged between BoS & Enclave.

1915 :: {Elder Redding} gives {Erik} his favorite (plasma rifle).

1917 :: {Erik} leaves & distracts, taunts scientist. Scientist follows.

1925 :: Scientist still following Eirk, pissed by this point as he can't catch
        him yet. Pulls (10mm pistol), rifle is out of ammo. Scientist unloads 6
        rounds, last one hits {Erik} in the leg, removing a chunk of flesh & a
        bit of muscle.

1935 :: BoS takes down sniper. {Erik} calls attention to himslef.  Soldiers stop
        by to pick up {Erik}. {Erik} passes out on the way to assistance.

1945 :: Soldiers drop off {Erik} /w elder, step outside. {Erik} comes to. Talks
        to {Elder Redding}, finds out that he's been in contact /w Dr., but not
        in the way the Dr.  hinted.

1955 :: BoS soldiers go to investigate black box left by Enclave as {Erik} &
        {Elder Redding} talk.

= Chapter Seven

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |      <15>            |
  +----------------------+

- 15 JUN 2280

2015 :: {Elder Redding} & {Erik} talk, {Erik} gets very pale & is forced to lie
        down. Elder calls the men back from their walkabout.

2020 :: {Jenkins} begins to de-debris & stitch up {Erik}'s leg as well as
        possible.  {Franklin} instructed to remove gear for his sutures as well.

2030 :: {Jenkins} leaves to find clean bandages. {Rush} comes to.  Scares the
        shit out of everyone- he's mourning the loss of his clanmates, then
        talks to elder.

2045 :: Elder begins to patch up {Franklin}, goes to get supplies to do so.
        Finds out {Rush} is afraid of needles, goes back to finish {Franklin}.

2055 :: Elder finishes up {Franklin}'s wounds. Talks /w {Franklin} & {Jenkins}
        about {Rush}, decides that he's going to invite {Rush} into the BoS.

= Chapter Eight

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |      <15>            |
  +----------------------+

- 15 JUN 2280

2100 :: {Franklin} & {Jenkins} argue /w the elder, lose.

2110 :: {Rush} accepts, requests rest.

2125 :: Elder takes first watch /w {Jenkins}, orders {Franklin} to bed as well.

2230 :: {Rush} attempts to visit {Franklin}. {Franklin} freaks out, yells at
        {Rush}.

2235 :: {Rush} returns to {Erik}. Elder Redding & {Jenkins} investigate
        mysterious Enclave box.

2240 :: {Elder Redding} gets letter from Enclave box.

2300 :: {Elder Redding} & {Jenkins} return to a hysterical {Franklin}.

2305 :: {Jenkins} takes (bowie knife) from {Franklin}. Takes him for a walk.

2320 :: {Elder Redding} goes to talk w/ {Rush}.

2340 :: {Elder Redding} & {Rush} visit {Erik} again.

= Chapter Nine

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |         <16>         |
  +----------------------+

- 16 JUN 2280

0000 :: {Jenkins} & {Franklin} return. {Elder Redding} begins mission briefing &
        introductions.

0025 :: Everyone to bed. {Erik} & {Rush} chat about Enclave.

0045 :: Everyone to sleep.

0500 :: {Erik}'s up, spoke to {Elder Redding}. He & {Rush} go for a walk.

0530 :: {Erik} & {Rush} find open Enclave. Investigate & move it back to the
        apartment.

0600 :: {Erik} & {Rush} return. Talk to {Elder Redding}. {Rush} gets his first
        cup of "coffee" & a talking-to from the Elder.

0625 :: {Jenkins} & {Rush} chat for a bit.

0645 :: They + {Erik} leave to explore toward [lab annex].

0700 :: Elder finds (Bozar), give to {Franklin}.

0730 :: Back to the 3, chatting about BoS occupying base.

0740 :: 3 hear whimpering. Find Dr. strangling {Harmony}. {Dorian} nearly passed
        out

0742 :: Fight ensues. {Jenkins} mending children.

0750 :: Fight done, {Jenkins} nearly done w/ children. {Rush} & {Erik}
        interrogate Dr.

0800 :: {Jenkins} splints {Dorian}'s leg.

0810 :: Doc begins protocol 47b.

= Chapter Ten / Eleven

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |         <16>         |
  +----------------------+

- 16 JUN 2280

0811 :: Sprint & get to another office. Get concussions, more damage. {Rush} is
        concussed. {Jenkins} checks everyone out.

0820 :: {Erik} sends {Jenkins} to retrieve his stimpacks from his coat in the
        [lab annex].

0830 :: {Franklin} finds [Elder Elena's apartment]. He & {Elder Redding} have
        lunch.

0900 :: {Rush} gets a bit better, stops vomiting. {Erik} chats /w the pups.
        {Jenkins} finds his way to the [lab annex]. {Elder Redding} & {Franklin}
        investigate new info from (holo tapes: ?, papers: ?).

0920 :: {Jenkins} investigates emergency radio signal.

0930 :: {Jenkins} find Elder Elena, chats /w her. Finds (stimpacks: 3).  They
        leave together.

        #TODO: Find office number for this scene

0950 :: {Jenkins} & {Elder Elena} return to (office ###). Walk in on {Erik}'s
        rendition of Jenkin's embarrassing trip to the gay bar in [The
        Crescent].

1000 :: {Rush} & {Erik} receive a verbal ass kicking from {Elder Elena}.

1010 :: {Jenkins} fixes {Dorian}'s leg a second time. Assesses {Rush}.

1015 :: Everyone starts walking back to [apartments].

1025 :: {Jenkins} warns {Rush} of {Elder Elena}'s wounds.

1035 :: {Franklin} waves {Jenkins} & everyone in. Elders meet.

1045 :: {Elder Redding} begins to offer protection in exchange for use of base.
        {Elder Elena} refuses to barter until dead are honored.

= Chapter Twelve

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |         <16>         |
  +----------------------+

- 16 JUN 2280

1230 :: Take care of dead. {Erik} & {Rush} fight over {Elder Elena} who wants to
        commit ritual suicide to pass down Alpha title. {Jenkins} intervenes.

1240 :: {Elder Elena} gives individual eulogies & burns bodies.

= Chapter Thirteen

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |         <16>         |
  +----------------------+

- 16 JUN 2280

1315 :: Everyone back to [Elder's Elena's apartment]. Elders start bargaining.

1400 :: Elders finish. {Elder Redding} agrees to take {Rush} & send envoys
        regularly to take care of the clan remnants. {Rush} & {Erik} sleep on
        couch.  Elders go for a walk to chat.

1430 :: {Franklin} watches elders from second deck porch, proceeds to get very
        drunk. Has internal dialog /w his dead brother.

= Chapter Fourteen

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |         <16>         |
  +----------------------+

- 16 JUN 2280

1530 :: Terminal in apartment finished recovery op. {Jenkins} investigates.

1600 :: {Erik} wakes up, investigates w/ {Jenkins}.

1700 :: {Rush} wakes up, investigates w/ {Jenkins} & {Erik}. They learn of
        {Rush}'s conception. Also learn of second attack on facility.

1730 :: {Rush} & {Erik} chat about nicknames & {Franklin}'s poor mental status.

= Chapter Fifteen

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |         <16>         |
  +----------------------+

- 16 JUN 2280

1745 :: {Erik} & {Rush} track down {Franklin} by scent. Take {Jenkins} /w them.
        End up in [ravaged apartment]. {Rush} learns of "rock, paper,
        scissors"--he loses & had to help clean up {Franklin}.

1800 :: {Rush} runs a bath & plops {Franklin} in it, he comes to, hallucinates
        his dead brother, starts to click w/ {Rush} after talking for a while.

1845 :: {Rush} joins {Erik} & spins him up to date.

1900 :: Radio from {Jenkins} to get {Franklin} in order. They're invited to
        dinner.

1910 :: 3 arrive to [Elder Elena's apartment]. Food is served. Elders toast.
        {Elder Redding} briefs them on next plans. Lots of chatter, bonding.

= Chapter Sixteen

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |         <16>         |
  +----------------------+

- 16 JUN 2280

1915 :: Brotherood members leave. {Erik} & {Rush} go to {Rush}'s to gather stuff.

1940 :: Leave {Rush}'s to cargo elevator.

1955 :: Arrive to [first floor], exit [Station Magnolia].

2000 :: Meet (Henrietta), load up & drive.

2045 :: Find fell tree & huge deer. Exterminate the deer. Move tree, clean
        (deer), get back on road.

= Chapter Seventeen

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |         <16 17>      |
  +----------------------+

- 16 JUN 2280

2130 :: Trio walks from drop-off point to [country club]. Starts
        thunderstorming.

2140 :: Thunder scares {Rush}, they all drink under the table.

2200 :: {Rush} comes out from under the table. {Erik} goes to find tinder.

2210 :: {Rush} & {Jenkins} go outside to the porch while {Erik} fights books.
        {Rush} & {Jenkins} start roughhousing in the rain.  {Erik} digs through
        kitchen to find a fire source. {Rush} & {Jenkins} come back in, {Erik}
        starts a fire & dinner.  {Erik} & {Rush} go to find more tinder,
        {Jenkins} babysits the fire, finds safe.

2300 :: Everyone eats. Converses, drinks a lot. {Erik} yells at {Rush}

- 17 JUN 2280

0030 :: {Rush} & {Erik} drop a passed-out {Jenkins} to bed. {Erik} & {Rush} go
        to bed.

= Chapter Eighteen

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |            <17>      |
  +----------------------+

- 17 JUN 2280

0530 :: {Rush} wakes up from nightmare. Vomits. Reads "Wasteland Survival Guide"

0630 :: {Rush} falls asleep again. {Jenkins} wakes up. They grab a snack.

0745 :: {Rush} shows {Jenkins} geistlink.

0830 :: {Rush} wakes up {Erik}. They all eat breakfast.

0900 :: {Erik} cracks safe. Finds snuff, {Rush} tries it, {Erik} freaks out.

# Flash forward

= Chapter Nineteen

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |            <17>      |
  +----------------------+

- 17 JUN 2280

1500 :: The three are out hunting, {Rush} dozes off & has a daydream /
        nightmare.  {Rush} finds buck, kills it.

1530 :: BoS does the hunting ritual for {Rush}, confuses him. Teach him to field
        dress a (deer: 1). They leave to drag the deer back.

1630 :: They arrive to the [border of the pine forest], find strangers on the
        horizon. Wait for them to leave or darkness.

1930 :: They get into the [country club], pack up & get out. They trek to the
        East /w their dead deer.

= Chapter Twenty

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |            <17>      |
  +----------------------+

- 17 JUN 2280

2145 :: Find [forest clearing]. Set up tent & get to cooking.

2200 :: Cook, & eat. {Rush} & {Erik} argue again. {Jenkins} makes them eat.

2215 :: They begin drinking again. {Erik} pees on {Rush}'s tree. They wrestle.

2240 :: They all turn in. {Rush} finds out that {Jenkins} is gay.

2245 :: They all sleep. {Rush} wakes up shortly thereafter to meet a mother yao
        guai. He helps her. Goes back to bed, finds {Jenkins} fighting in his
        sleep. Wakes him up, does another geistlink. They chat about {Jenkins}'
        history & preferences

2330 :: Everyone finally gets to bed.

= Chapter Twenty One

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |               <18 19>|
  +----------------------+

- 18 JUN 2280

0700 :: {Rush} gets up, accidentally wakes up {Jenkins}. {Jenkins} gets
        embarrassed seeing {Rush}'s morning wood. {Rush} teases {Jenkins}, he
        runs off into woods to berate himself. {Rush} & {Jenkins} chat,
        {Jenkins} requests to be left alone.

0730 :: {Rush} returns, wakes up {Erik}. They chat about {Jenkins}, {Erik} asks
        that {Jenkins} take it easy on {Jenkins}. {Jenkins} returns. They pack
        up camp & head East.

0830 :: They stop mid-trail to find the yao guai. She brings her cubs by to
        visit & thank {Rush} before they leave for good. Tells him about a
        "cave." They start traveling again.

1130 :: {Rush} drops a deuce. {Jenkins} & {Erik} chat further down the trail as
        they wait. {Erik} asks {Jenkins} about his earlier behavior. {Jenkins}
        evades, steels himself to tell {Erik} how he feels about {Rush}, but
        {Rush} shows up just in time to ruin it. Rain threatens, they decide to
        go to cave the yao guai told {Rush} about.

1230 :: Rain starts, they get into "cave." [abandoned facility]. They try to
        break in to locked door, unsuccessful. {Erik} hot wires panel, gets them
        in. Rain intensifies outside. They get into [main ops floor].

1345 :: {Rush} finds secondary terminal, investigates after fixing terminal.
        {Jenkins} gets a bit creepy. {Jenkins} & {Rush} grab bags. {Erik} gets
        live data.  {Jenkins} gets creepier.

1430 :: {Erik} yells at {Jenkins}. {Rush} borrows {Erik}'s PipBoy, tries to get
        it to copy local programs for later use. {Erik} drags {Jenkins} off for
        a talking-to.

1440 :: {Erik} berates {Jenkins}, storms off. {Jenkins} is shocked, stays
        behind, but later comes up by himself.

1455 :: {Rush} yells at {Erik} for lying to him, again. {Erik} fesses up. They
        yell at each other, {Rush} hits {Erik} & knocks him out tempof orarily.
        {Erik} gets back up swinging, hits {Rush}, turns on {Jenkins}, {Rush}
        pins him down. {Erik} degrades quickly into something more bestial.

1505 :: {Rush} tries to giestlink, fails, asks {Jenkins} to medicate {Jenkins}.
        He does w/ (ketamine: 5cc IM). {Erik} blacks out quickly

1510 :: {Rush} performs a second geistlink. Successful. {Erik} begins to unblock
        his memories, has internal struggle between his old self & the new one
        waking up.

1700 :: {Erik} comes out of ketamine. They all chat. {Rush} tells {Erik} about
        his coming of age, almost killed himself. More chatting & reassurance.

1730 :: {Jenkins} goes to explore remaining rooms in facility. Finds [mainframe
        room], [barracks], & [restroom]. Returns to {Erik} & {Rush}.

1750 :: {Erik} goes to bed. {Jenkins} & {Rush} eat dinner then drink in
        [mainframe room]. {Rush} puts a handsy {Jenkins} to bed. Goes to
        investigate [mainframe room].

1815 :: {Rush} investigates terminal. Finds network connection attempts from
        [Station Magnolia] from unknown user. Finds live messages from {Dr.
        Hawthorne}; thinks he is hallucinating.

1930 :: {Rush} goes to [restroom] for a break. Wakes up {Erik}. Learns about
        radiation, survival. {Erik} is running a fever, his eyes change color
        temporarily to amber like {Rush}'s.  Goes to bed, thinking he's in a
        dream.

2000 :: {Rush} goes to bed.

= Chapter Twenty Two

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |                  <19>|
  +----------------------+

19 JUN 2280

0900 :: {Rush} rolls off of the top bunk & into a minor concussion Wakes up
        {Erik} & drunken {Jenkins}. {Jenkins} inspects {Rush}, follows him into
        the [bathroom] & checks {Rush} out, gets embarassed. They talk about
        last night.  {Jenkins} continues to be embarassed about his drunken
        behavior.

0920 :: {Rush} checks on {Erik}. Grabs breakfast for everyone. {Erik} skips food
        for now, continues being angry. {Jenkins} & {Rush} talk about {Erik}'s
        behavior.

0945 :: {Rush} goes to see what {Erik} is yelling about.

0946 :: {Erik} tries to cave {Rush}'s skull in. Gets two good hits in.
        {Jenkins} yells at him, {Rush} bear hugs {Erik} into submission.  {Erik}
        figts, is having an "episode."

1015 :: {Rush} initiates geistlink, helps calm {Erik} down.

1030 :: {Erik}, {Rush}, & {Jenkins} chat.

1100 :: {Erik} takes inventory. Finds out about satellites, gets excited. The
        three go to the [comms room]. Investigate mainframes & radio. {Jenkins}
        calls Delta, informs them of their new location, but not before {Dr.
        Hawthorne} sends them another message

1145 :: {Erik} loses it again.

1155 :: {Jenkins} explains Eileen. {Rush} & {Erik} go out for a walk & chat.
        {Erik} assaults a tree. {Rush} repairs {Erik}'s hands.  {Erik} starts
        remembering things from his past, stored & hidden memories. Tells {Rush}
        about the memories that aren't his--from his fater about the attack at
        RedCreek.

1230 :: {Erik} & {Rush} get rained on, get back into facility soaking wet after
        washing in the [creek]. {Rush} guides him into the showers to clean up.
        They run into {Jenkins} who is showering at the time. {Erik} realizes
        the memory from his father was baby {Jenkins} & his adoptive mother
        {Marie}.

1300 :: {Rush} showers up. Shocks {Erik}, get's pelted /w gear.

1315 :: {Rush} & {Jenkins} chat. {Erik} comes in from the shower, they
        collaborate on a plan for their remaining four trip days.

1400 :: {Jenkins} makes {Rush} some (leather clothing).

1430 :: {Rush} goes for a walk in his new clothes, {Jenkins} joins him. {Erik}
        reads his (leather journal).

1445 :: {Erik} gets called out for snooping.

1500 :: {Erik} goes to bed. {Rush} & {Jenkins} start drinking & chat.

1900 :: Everyone goes to bed.

= Chapter Twenty Three

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |                  <19 |
  | 20>                  |
  +----------------------+

- 19 JUN 2280

2200 :: {Erik} & {Rush} share a dream of those long past.

- 20 JUN 2280

0600 :: {Rush} & {Erik} wake up. They wake {Jenkins} & get their day started.

0630 :: Inventory complete. {Rush} pokes fun at {Jenkins}.

0800 :: They leave the [abandonded facility].

1000 :: Walking for two hours.

1300 :: Walking for three hours.

1320 :: Arrived in [Fruitland].

1330 :: Arrive at [hotel].

1400 :: Bellhop arrives /w food.

1415 :: Lunch: complete. Bellhop arrives /w (Document: Letter from Olivia) for
        {Erik}.

1420 :: {Rush} goes to find odd jobs from Ernie.

1435 :: {Rush} finishes fixing the jukebox, gets a (Beer: 1).

1545 :: {Rush} finishes his beer, fixes a bunch of crap.

1600 :: Gets paid (Caps: 300). Linda explains currency. {Erik} stops down to see
        how things are going. {Erik} asks Ernie about anything suspicious, gets
        a bit of info about the new tribals.

1620 :: {Erik}, {Rush}, & {Jenkins} go to the [Fruitland bazaar].

1800 :: {Erik} gets angry at vendor, gives them the (Scythe: 1) as an apology.

1810 :: The trio arrive back at the hotel. Then go to [Fruitland bar
        Shipwrecked]

1820 :: The boys start drinking.

2350 :: They find out about the tribals while heading back to the [hotel].

2355 :: Ernie helps {Rush} bring {Erik} & {Jenkins} up to the [hotel room].

= Chapter Twenty Four

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |   <21>               |
  +----------------------+

- 21 JUN 2280

0000 :: Shared dream about past stuff.

0600 :: Trio awakens.

= Chapter Twenty Five

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |   <21>               |
  +----------------------+

- 21 JUN 2280

NOTM :: meeting

NOTM :: {Rush} & {Olivia} get medical tests done.

NOTM :: went to bed

= Chapter Twenty Six

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |      <22 23 24 25 26 |
  | 27>                  |
  +----------------------+

- 22 JUN 2280> 27 JUN 2280

---- :: General training

- 27 JUN 2280

0900 :: Breakfast

1015 :: {Olivia} goes to help {Jenkins} & {Willis}.

1100 :: {Jenkins} / {Olivia} get kicked out of the lab- contaminated

1110 :: {Rush} / {Erik} go to bazaar

1230 :: {Rush} / {Erik} go to garage, meet Tom

1245 :: Tom / Marie find out about the new "tribals." Receive (10 mm pistol) via
        package from the Elder.

1300 :: {Olivia} / {Jenkins} see {Rush} working on (Corvega Highwayman), go to
        investigate. They chat and find out that {Erik}'s talking w/ {Tom} &
        {Marie}

1308 :: Marie leaves garage to go home, runs into {Jenkins}, lies about why
        she's crying. {Jenkins} goes to talk to his dad, finds out he's adopted,
        how his original family died and he was saved by Marie.

1330 :: They all go home, except {Rush}, who gets kicked out of his own room as
        {Erik} & {Olivia} want to have sexy times. {Rush} wanders around town.

1345 :: {Rush} walks up the road, visits kennel to get yelled at by dogs. Runs
        into {Willis}, chats for a bit.

1400 :: {Rush} wanders around and takes a nap under an oak tree near the lab
        building

1705 :: {Rush} wakes up after inhaling a gnat. Goes to find elder for permission
        to sleep with the commons.

1715 :: {Rush} wakes up the elder accidentally. They chat tactics for a while.

2045 :: {Elder Redding} lets {Rush} crash on the couch.

2100 :: {Elder Redding} leaves to go chat w/ {Maj. Artemis}. They chat & drink
        for a while.

2300 :: {Elder Redding} goes back to his own room to find {Rush} on the floor,
        asleep. He lets {Rush} have his bed, he crashes on the couch.

= Chapter Twenty Seven

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |   <28>               |
  +----------------------+

- 28 JUN 2280
0500 :: {Elder Redding} goes to sleep.

0600 :: {Rush} gets up, goes to get {Maj. Artemis}. Finds him trying to get
        {Jenkins} up. {Rush} fetches tools and pops of their door. Helps
        {Jenkins} over {Willis} to go see the {Elder Redding}.

0630 :: {Rush} announced {Jenkins}' arrival and is sent off to go eat breakfast
        before retrieving his brother and {Olivia}.

0635 :: {Maj. Artemis} & {Jenkins} have meeting w/ {Elder Redding}.

0650 :: Random BoS cook drops of food for {Elder Redding}. {Rush} goes down to
        cafeteria; finds drunk {Franklin}, finds out about dead cooks.

0655 :: {Rush} returns with news of murders. He & {Jenkins} are sent to go find
        {Erik} & {Olivia}. They find them jogging on the [highway]. Meet up in
        field.  Learn that {Erik}'s bad in bed while {Erik} goes to retrieve
        some scouts for explaining.

0720 :: {Erik} arrives with scouts who explain what happened on their patrol &
        bring the 4 to the murder scene.

0727 :: They look over the murder scene lightly before {Elder Redding} & {Maj.
        Artemis} arrive, save {Rush} from persecution. {Elder Redding} orders
        scouts to bring the dead to the chapel for a burial.

NOTM :: Burial and formal ceremony for the deceased.

1930 :: {Elder Redding} calls another meeting to finalize plans. He kicks them
        out of his room.

1950 :: {Olivia}, {Jenkins}, & {Franklin} go to their rooms. {Rush} & {Erik} go
        to the [scene of the murder] again.

2000 :: {Rush} & {Erik} go to the [sniper tower] for info. None found.

2015 :: {Rush} & {Erik} get to the [scene of the murder]. They talk to a
        {squirrel}.  Find out some preliminary info.

2030 :: Go to [cafeteria]. Disappointed by food. Eat and go to visit the {Elder
        Redding}.

2050 :: They chat for quite a while, learn how the [BoS Delta Base] was
        established and how their BoS chapter got started.

2155 :: {Rush} & {Erik} go to their room. Both go to sleep quickly.

= Chapter Twenty Eight

  +----------------------+
  |       JUN 2280       |
  | Su Mo Tu We Th Fr Sa |
  |   <28>               |
  +----------------------+

- 28 JUN 2280

NOTM :: {Erik} has recurring nightmare. Fights repeatedly.

2300 :: {Erik} gets out of {Rush}'s bed and into his own.

- 29 JUN 2280

0005 :: {Rush} wakes up {Erik}. He looks like a real Hawthorne!

0010 :: {Erik} inspects himself in the bathroom mirror, learns about the slim
        chance he could be turned by his grandfather's serum, loses his shit.
        He attacks {Rush}.

0014 :: {Franklin} bangs on their door. {Willis} and {Jenkins} knock the door
        down.  {Willis} and {Franklin} tear {Erik} off of {Rush}.

0018 :: {Willis} knocks {Erik} out. {Jenkins} patches {Rush} up. {Willis} and
        {Jenkins} kick {Rush} out of his room to sleep w/ {Franklin}.

0120 :: {Erik} comes around. Chats then gets very angry again. Transforms.

0122 :: {Erik} is nearly completely feral. Has a lucid moment, long enough for
        {Jenkins} to shoot him with (ketamine: 10cc IM); knocks him out.
        They keep watch over him.

0600 :: {Rush} brings everyone breakfast. {Rush} goes to retrieve {Olivia} and
        fill her in.

0610 :: {Rush} & {Olivia} arrive. {Olivia}'s pissed about {Erik} beating on
        {Rush}.  Attempts to take it out on {Willis}--uneffective. {Rush}
        introduces {Olivia} to geistlink. {Jenkins} & {Willis} go back to
        [Jenkins' apartment] to have sexy times.

0612 :: {Rush} & {Olivia} trek through {Erik}'s dream.

0830 :: They find him, calm him down and all come out of geistlink. Discover
        that {Erik}'s transformation back to human is very nasty. {Rush} goes to
        find {Jenkins} & {Willis}.

0833 :: {Rush} lets himself into {Jenkins}' room; wakes up {Jenkins} and
        {Willis}. He's furious, yells at them. Gets mutant foot in his face and
        yelled at.

0840 :: {Rush} talks to {Erik} & {Olivia}. Grabs his go-pack and leaves to the
        [lab].

0845 :: {Rush} makes it to the lab; it's locked. Gets run down by a scribe.
        Wanders around aimlessly.

0900 :: {Rush} go to [Tom's Garage]. Chat's with {Tom} for a few minutes.
        Leaves and runs into the scribe from before.

0915 :: {Scribe Matthias} and {Rush} go to [Scribe's Laboratory]. Gets the
        lowdown on what the scribes do for the BoS.

0920 :: {Rush} meets up with {Olivia}, gets a nosebleed. Goes to mop it up
        after learning they're going to get trained for RobCo repair.

0935 :: {Rush} returns. They learn about the RobCo terminals.

1100 :: {Rush} & {Olivia} break for lunch.

1200 :: {Rush} & {Olivia} return. {Olivia} leaves to [Scribes Laboratory: 3rd
        floor] for weapons training and repair. {Rush} receives (Book:
        RobCo Programmer's Handbook)

1400 :: {Rush} attempts to repair terminal. {Olivia} learns about plasma rifles.

1440 :: {Erik} stops by to chat with the scribes conceding {Olivia} and
        {Rush}'s progress. He requests a (Pip-Boy 3000) for {Olivia}. He goes
        downstairs to see {Rush}.

1442 :: {Rush} and {Erik} chat about his mission. {Rush} watches as {Erik}
        leaves to go on his mission, then goes to wash his face.

1800 :: Dinner time. {Scribe Matthias} rolls around to check on {Rush}.
        {Scribe Matthias} brings {Olivia} and {Rush} to the cafeteria.

1825 :: {Rush} & {Olivia} meet a pair of scribes. {Rush} immediately threatens
        one when he makes a lewd joke about {Olivia}'s previous vocation. Learn
        that {Olivia} was born in [Jackson, MS], sold into slavery. Killed
        ring-leader of slavery guild. Got her sister killed, moved to [The Burg]
        to work in bathhouse. Met {Erik}, joined the BoS.

1900 :: {Rush} invites {Olivia} to stay with him in his room. Run into {Willis}
        and {Jenkins}. They quickly leave to Jenkins' parents home on the North
        side of town.

1910 :: {Rush} runs up to his room to let {Olivia} in. She complains about the
        state of their room. {Rush} cleans up quickly. They chat, {Olivia} shows
        her emotional side momentarily. Shows her the photo of his and Erik's
        family.

        They chat about {Olivia}'s past:
        Her mom died during her birth
        She and her sister are sold into slavery after drug dealers kill their
          father who had a nasty jet habit.
        She and her sister are sold into prostitution from [Jackson, MS] to
          [The Crescent, LA]
        She kills the leader of their slaver guild, leads a riot. Her sister
          died that day. Starts a vigilante group and runs it for a few years.
        Joins up with BoS, quits shortly afterward and works at local
          bathhouse [The Stumpjumper]
        Joined the BoS after {Erik} returned with {Rush}

2330 :: {Rush} & {Olivia} go to bed to the tune of a summer thunderstorm.

= Chapter Twenty Nine

  +----------------------+  +----------------------+
  |       JUN 2280       |  |       JUL 2280       |
  | Su Mo Tu We Th Fr Sa |  | Su Mo Tu We Th Fr Sa |
  |      <29 30>         |  |            <01 02 03>|
  +----------------------+  +----------------------+

- 29 JUN 2280

0530 :: {Rush} wakes {Olivia} to inform her of the thunderstorms outside. {Rush}
        explains his lack of experiencing weather.

0550 :: {Olivia} takes a shower.

0615 :: {Rush} takes a shower, gets spied on by {Olivia}.

0645 :: {Rush} & {Olivia} go down to breakfast. Find that {Franklin} is back in
        the kitchen. Find out that he's been going to church frequently to help
        with his alcoholism and depression.

0700 :: {Willis} joins them for breakfast, in a sour mood. He yells at {Rush}'s
        blunt attempts of kindness.

0800 :: {Erik} & {Jenkins} have a jerky fight in the car, wake up {Maj.
        Artemis}.

0815 :: They run into a thunderstorm on the abandoned highway.

1000 :: They arrive at Hwy. 67 and the old access road to [Facility One] and
        chat as they walk toward the facility.

1030 :: {Rush} & {Olivia} are receiving basic medic training. {Rush} makes
        things weird by asking sexual reproduction questions. Also cover
        pheromones and immorality of being insincere.

1045 :: {Rush} tells {Olivia} that he's still not very comfortable with her.
        Makes her angry; she yells at him. {Rush} runs off to beat up on himself
        in aggravation. {Olivia} tracks him down, apologizes.

1050 :: {Ida} & {Willis} track them down, yell at them a little, then lead them
        back to [Ida's Laboratory]. {Ida} gives them (Erik's Note) and (Plasma
        Rifle: 2). {Willis} informs them that they're going on their first
        official snipe hunt.

1130 :: {Jenkins} questions {Erik}'s obvious favoritism toward {Rush} & {Olivia}
        {Erik} shrugs it off. They enter [Facility One] and kill a Rad Roach.
        They pick up their conversation and continue exploring [Facility One:
        1st Floor]

1200 :: {Erik} finds booby traps, almost becomes a boob. {Jenkins} finds a
        suicide victim.

1215 :: {Erik} finds {Roy}, attemps to kill him. Failes (uknown).

1245 :: {Erik} gets power going. {Jenkins} investigates radio signal. Meets up
        with {Erik}. Get's knocked cold.

1330 :: {Duke} & {Jerry} tie up {Jenkins}. {Jenkins} comes around, gets
        interrogated. {Erik} to the rescue.

1345 :: {Erik}, {Jenkins}, {Duke}, {Jerry}, and {Roy} go to VAX. {Erik} &
        {Jenkins} get to work.

1700 :: {Rush} & {Olivia} go to [Ida's Laboratory] to go hunting. They hike out
        and set up camp.

1900 :: {Rush} & {Olivia} return from snipe hunting, empty handed. {Rush} learns
        a bit about super mutants. Party eats and beings drinking. {Ida} and
        {Willis} give the initiates some bg info about themselves.

1920 :: {Rush} & {Olivia} fight, then go to bed.

2020 :: {Jenkins} & {Roy} work on dish. Sends radio back to Delta. {Rush} picks
        up.

2045 :: {Erik} & {Jenkins} go to deer camp for the night. Go to sleep.

- 30 JUN 2280

0600 :: {Rush} finishes up radio. {Olivia}, {Willis}, & {Ida} leave camp; all go
        back to [Ida's Laboratory]. {Olivia} & {Rush} train with the scribes
        again for the remainder of the day.

---- :: {Erik} & {Jenkins} chat w/ village people, get drunk, then leave. Set up
        camp that afternoon/night.

- 01 JUL 2280

0430 :: {Olivia} wakes up, wakes up {Rush}, they chat then eat breakfast.

0500 :: {Olivia} & {Rush} eat, meet {Elder Redding}. {Rush} scalds himself.
        {Olivia} & {Franklin} chat.

0600 :: {Olivia} & {Rush} go to gather supplies for {Franklin}'s trip.

0700 :: {Olivia} & {Rush} meet {Franklin} & {Father Murphy} at the [Chapel].
        They meet the father and get a quick blessing before getting on the
        road.
1200 :: {Rush}, {Olivia}, & {Franklin} kill a large hog. {Olivia} is in shock,
        she'd never seen the killer side of Rush. They gut and skin it in place.

1220 :: They return with firewood for {Franklin} and start cooking lunch.

1250 :: Yao guai lumbers in, {Rush} & {Olivia} patch it up as best they can.
        Learn about the tribals' movements. They're searching for something...

---- :: {Rush}, {Olivia}, & {Franklin} finish day without incident.

- 02 JUL 2280

1200 :: {Rush}, {Olivia}, & {Franklin} return to [Delta Base]. Initiates see
        Ida, she shooes them off to clean up and eat lunch.

1210 :: {Rush} & {Olivia} are denied entrance to the [Common Hall, 3rd Deck].
        {Elder Redding} clears things up. They shower up, and eat lunch.

1250 :: {Rush} & {Olivia} return to [Ida's Laboratory].

1315 :: {Rush} & {Olivia} receive keys to their new apartments and begin moving
        in.

1330 :: {Willis} helps {Rush} move in, brings him a bed. Hangs out a bit.

1430 :: {Willis} & {Rush} finish sorting and chat.

1440 :: {Olivia} barges in. They all chat for some time.

1600 :: {Willis} leaves.

1610 :: {Olivia} leaves. Gives birthdate to {Ida}.

1700 :: {Erik} & {Jenkins} arrive to [Station Gamma] to find it full of
        radiation. It's collapsed and they can't do any searching at this time.

1900 :: {Elder Redding} goes to [Common Hall, Room 201] to visit {Olivia}

1915 :: {Elder Redding} and {Unnamed scribe} visit Rush. Normal awkwardness
        occurs when {Rush} forgets his clothing. {Elder Redding} gives him his
        (birthday present) and he oes to sleep on his couch.

2200 :: {Erik} & {Jenkins} travel through swamper territory. Engage a "pack" of
        them, but manage to lure them into a HUGE grenade trap. Kill 4 with
        that, and two more by hand before escaping.

- 03 JUL 2280

0500 :: {Erik} & {Jenkins} get up and get on the road

1200 :: {Erik} & {Jenkins} arrive in [Caterpillar] and meet up with traders from
        The Burg. Get a ride home.

1520 :: {Erik} & {Jenkins} arrive at [The Burg].

1600 :: {Rush} & {Olivia}, at [Rush's Apartment], working on map of upcoming
        trip {Erik} & {Jenkins} stop by. They chat, {Rush} gives a quick tour.

1700 :: {Franklin} finds them at [Rush's Apartment].

1745 :: {Franklin} & {Jenkins} get into a verbal conflict. {Willis} hears them
        and stops by to /help/. {Olivia} & {Franklin} get into a fist fight,
        destroying Rush's (Urn). {Willis} stops {Rush} from killing {Franklin}.

1800 :: {Franklin} taunts Rush some moreabout being a man-made monster. {Willis}
        knocks him out cold. {Rush} scoops up ashes. {Erik} explains
        {Franklin}'s PTSD. {Willis} moves semi-conscious {Franklin} to couch,
        threatens him.

1810 :: {Willis} & {Ida} talk about {Franklin}'s perscription of (Calmex). Can't
        up dosage, will nuke his liver. {Willis} fetches a bottle of liquor and
        {Father Murphy}, brings them both to [Rush's Apartment]

1825 :: {Rush} & {Father Murphy} speak. {Franklin} defends himslef,
        unsuccessfully. {Father Murphy} requests urn an ashes be brought to
        [Chapel]. {Franklin} & {Father Murphy} argue, {Father Murphy} defends
        {Rush}, {Erik} argues against {Franklin} as well. {Franklin} kicks
        over {Rush}'s (Coffee Table) and storms out.

2000 :: {Jenkins} takes urn and ashes to [Chapel]. {Rush} wishes {Olivia} a
        happy birthday. Gives her and {Erik} (Beer: 2) and her a (Gift:
        Nuka-Cola Quantum, Black Pencil, Small Black Notebook, Bracelet)
        They chat for a bit before she and {Erik} leave to [Erik's Apartment]

2030 :: {Rush} cleans up a bit more and goes to bed.

2100 :: {Franklin} comes to [Rush's Apartment], severly drunk. {Rush} tries to
        send him home quickly, finds out that {Elder Redding} has kicked him out
        as a desparate measure to straighten him up. {Rush} offers {Franklin} his
        bed. {Rush} sleeps on his couch.

= Chapter Thirty

  +----------------------+
  |       JUL 2280       |
  | Su Mo Tu We Th Fr Sa |
  |<04 05 06>            |
  +----------------------+

- 04 JUL 2280

0500 :: {Rush} gets up after three hours' sleep. Finds {Franklin} vomited on
        himself and Rush's bed.

0515 :: {Rush} storms off to {Ida}'s lab and finds she's gone, but {Willis}
        answers. {Jenkins} soon appears and drags {Rush} off to breakfast.

0600 :: {Franklin} rolls out of Rush's bed and stumbles into the bathroom.

0615 :: {Erik} and {Olivia} join {Willis}, {Jenkins}, and {Rush} at breakfast.
        {Rush} explains that {Franklin}. They kinda defend {Franklin}'s
        behavior, leaving {Rush} to storm off.

0620 :: {Rush} sees {Franklin} moving his soiled bedding to the back. Confronts
        him and receives an apology. {Rush} nabs his bag and they go to get
        {Olivia}

0630 :: {Erik} and {Jenkins} meet the three at the crossroads and see them off
        before taking over the radio station. {Jenkins} leaves {Erik} there. He
        calls them shortly afterward and tells them to call in every 12 hours.

1100 :: The trio stops at an abandonded KOA to eat lunch.

1700 :: The trio followed a trail to a creek and find a hog. {Franklin} angers
        it, and {Olivia} kills it with a plasma rifle. Something stalks them,
        {Rush} and {Olivia} run off to find out what it was -- they lose it.
        {Franklin} cooks the hog while they get their tents set up. They find
        out {Franklin}'s favorite color is yellow.

1830 :: {Rush} and {Olivia} return to camp to find {Franklin} asleep. They chat
        for a while before going to bed.

- 05 JUL 2280

0545 :: {Rush} wakes up and packs in his tent before taking a piss. He
        accidentally wakes up {Olivia}. He falls asleep again standing up and is
        reawoken by {Olivia}. She packs up by letting him pull down the tent on
        himself then orders he go exercise to wake up. They all chat before
        {Franklin} gets ready.

1130 :: They arrive at the outskirts to a Raider town. Turns out their first
        stop has been taken over by {Vipers} for a long time.

1145 :: {Rush} tries to call back to Delta, radio call is intercepted. He heads
        back to find them and nearly gets his face blown off. {Franklin}'s
        pep talk scares the bejessus out of {Rush}.

1220 :: They split up and {Olivia} acts as bait.

1225 :: She's abducted. {Rush} and {Franklin} invade, taking out a patrol.

1230 :: {Rush} and {Franklin} find their [Love Shack]. {Rush} attacks the patrol
        he rips out their jugulars, ingesting drug-tainted blood. He pounced
        {Franklin}, then comes back to his senses. {Franklin} makes him clean
        up the dead bodies.

1240 :: {Franklin} goes out and finds the [Fun House]. Kills the drug-addled
        Vipers there. Two Viper chemists come down the stairs to investigate the
        commotion and get killed by him too. {Rush} comes in and find the
        carnage.

1250 :: {Rush} runs down the street in broad daylight, garnering the attention
        of the non-addled Vipers. He runs to {Olivia's} side. A machine gun-
        wielding Viper mows down a path toward them. {Franklin} soon breaks his
        way into their building.

1310 :: {Rush} runs out behind the crowd and catches them off-guard, gutting
        half of them. The remaining half scatters, except for the machine gun
        woman. He spears her with his arm and kicks her to the ground.
        {Franklin} takes her out with a shot to the head. {Rush} runs after the
        Vipers that ran off, hunting them down one-by-one.

2030 :: {Olivia} and {Franklin} stop searching for {Rush} and return to town to
        find that the Viper leader broke out and killed their slaves, leaving
        them in a pile in the middle of town.

2100 :: {Olivia} and {Franklin} burn the dead slaves. {Olivia} sends a report
        back to Delta. Chem storage explodes after they leave.

2120 :: They walk a few miles North and take refuge under an underpass from the
        impending summer storms. {Franklin} and {Olivia} chat for a while about
        {Rush} and {Franklin}'s medication while he cooks dinner and they have
        drinks.

2150 :: {Rush} finds the burning town and skirts it to the West.

2230 :: {Rush} stumbled upon their camp and alerts them by shooting (Olivia's
        Plasma Rifle) into the sky. After yelling at him, {Olivia} realizes its
        him and runs into the irradiated rain to drag him under the overpass.
        {Franklin} orders {Olivia} to feed and put {Rush} to bed. After a warm
        dinner and meds, he comes around. {Olivia} gives him a pep talk to feel
        better about being a drug-crazed killer.

0000 :: {Rush} and {Olivia} go to bed.

- 06 JUL 2280

0500 :: {Erik} is awoken by a scribe and ordered to see {Elder Redding}. He and
        {Jenkins} get a talking-to about doing their jobs and not ignoring
        orders.

0520 :: They join {Elder Redding} to the [Cafeteria] for and their new
        assignments. {Jenkins} will re-join {Ida} and {Willis} in the
        [Laboratory]. {Erik} will go on a reconnaissance mission.

0535 :: {Willis} joins them at their normal table and begins to bother {Jenkins}
        about his solo quickie in [Rush's Quarters].

0540 :: {Erik} fetches ketchup and quickly returns when {Willis} begins yelling.

0545 :: {Erik} finds out, unwillingly, about {Jenkins}' solo quickie in [Rush's
        Quarters] and leaves, in search of eye bleach.

0600 :: {Erik} walks to the [Laboratory] to find {Ida} and her disheveled lab.

0645 :: {Willis} and {Jenkins} join them and {Elder Redding} at the
        [Laboratory], find out that it was broken into the early hours of the
        morning. Someone was searching for research on Rush's clan.

0710 :: {Erik} leaves to investigate Scribes for suspicious behavior. {Ida},
        {Willis}, and {Jenkins} begin rebuilding experiments.

0720 :: {Elder Redding} goes back to his quarters to find {Scribe Terry Summers}
        waiting for him. He dodges him and goes to [Tom's Garage] to retrieve
        his official documents.

0730 :: {Elder Redding} gets caught breaking into [Tom's Garage] by {Tom}.

0745 :: {Scribe Terry Summers} tracks him down, gets dragged into [Tom's Office]
        for an interrogation. He reveals that he's a double-agent for the
        Enclave and was pushed to spy to keep his sister safe.

1800 :: {Erik} joins {Willis} and {Jenkins} at their regular table during
        dinner. They eat a depressing dinner with an equally depressing
        conversation.

1830 :: {Jenkins} and {Willis} go back to the [Laboratory] to continue their
        work. {Erik} wanders around for a while before crashing in [Rush's
        Quarters]

2030 :: {Erik} passes out in Rush's bed.

= Chapter Thirty One

  +----------------------+
  |       JUL 2280       |
  | Su Mo Tu We Th Fr Sa |
  | 04 05 06<07 08 09 10 |
  | 11>12 13 14 15 16 17 |
  +----------------------+

- 07 JUL 2280

---- :: {Erik} has a nightmare inside of a nightmare. He watches shadows melt
        into dark stains on the asphalt. A nuclear explosion followed.

0800 :: {Willis} awoke a screaming {Erik} and demands he quit.

1100 :: {Erik} meets {Elder Redding} for lunch and is instructed to resume his
        typical duties.

1200 :: {Olivia}, {Rush}, and {Franklin} continue to hike to their second LZ.
        She's pissy that {Rush} slept so hard. {Franklin} chimes in too.
        {Franklin} stops to take a piss and {Rush} sees the [Corvega Henge]

1210 :: {Olivia} finds the (Corvega Henge Trigger holodisk) and nearly kills
        them in an explosion before {Rush} stops her from playing it.
        {Franklin} agrees with {Rush} that it's weird and they should clear the
        area before playing the tape, just be safe.

1215 :: {Olivia} plays the (Corvega Henge Trigger holodisk), making her Pip-Boy
        a remote trigger and causing a horrendous explosion of Corvegas.

1235 :: {Olivia} and {Franklin} nearly get into a fist fight while {Rush}
        wanders toward the recent car-splosion to explore. {Olivia} drags him
        away and browbeats the shit out of him. She makes him take some Rad-X.
        Another awkward conversation later, and {Franklin} puts them back on
        track.

1530 :: {Olivia} realizes she's pregnant and {Rush} nearly passes out from
        heat exhaustion. The three have an awkward conversation about {Olivia}'s
        incoming child and its godparents.

1800 :: The three reach  a settlement their second LZ and stay at the local
        tavern. They give {Olivia} a room of her own and share one between
        themselves.  {Franklin} mocks {Rush} for having canine genetics and how
        he won't get laid.  They fight, and the proprietor makes Olivia break up
        their tussle. She douses them with a fire extinguisher. They make up and
        do as she instructed.  {Franklin} and {Rush} split the twin bed.

- 08 JUL 2280

0700 :: {Olivia} goes to (Franklin and Rush's Hotel Room) to wake them up.
        {Rush} catches her checking out {Franklin} before she goes downstairs
        without them.

0735 :: They join her downstairs. {Olivia} and {Rush} pay for their two rooms
        and damage from the night before.

0745 :: They go to the town's bazaar -- {Olivia} demanded {Franklin} pay for
        ammo to reimburse her for their destruction last night. He refuses.
        {Rush} pays and splits them with her. She sends {Rush} to buy jerky. He
        finds a kid selling (mysterious meat) and refuses to buy it, warning the
        kid that he'll make people sick like that. {Franklin} tries to buy some
        small-caliber ammo, but finds it lackluster.

1200 :: They arrive at their second LZ. {Rush} tries to radio in, but is unable
        to due to radio interference.

1210 :: They enter the [Enclave Shipping & Receiving Warehouse] on the Northwest
        side of the base.

1220 :: {Rush} finds (President John Henry Eden's holodisk to Colonel Summers),
        reads it, and hands it off to {Jenkins}.

1245 :: {Scribe Terry Summers} and his {Splicer Crew} show up and whoop the
        Brothers.  {Franklin} has a PTSD attack and gets the crap kicked out
        himself after leaving the {Splicers} with some new scars. {Scribe Terry
        Summers} shoots him in the head and leaves him for dead.

- 09 JUL 2280

1300 :: {Jenkins} finds {Erik} in [Rush's Apartment], studying (Dr. Hawthorne's
        Experiments). They lock up and go to the [Stump Jumper].

1315 :: {Ernie} greets them and serves them some beers. {Major Artemis} arrives
        and sits with them. After a beer, {Erik} leaves and sprints back to
        [Rush's Apartment], trying to lock himself in -- he's transforming.
        {Willis} breaks in after him in concern and ends up ripping him free of
        his armor so he can finish transforming.

1321 :: {Willis} helps {Transformed Erik} into the tub to clean up.

1345 :: {Jenkins} finds {Willis} and {Erik} in [Rush's Apartment] and recognized
        the disgusting mess of {Erik}'s transformation on the floor.
        {Transformed Erik} nearly passes out from a calorie deficiency and
        {Willis} makes him eat his old husk.

1355 :: After {Transformed Erik} recovers, the other two resume investigating
        him. {Erik} and {Jenkins} argue over him drawing five (mutated blood
        samples).  They leave back to the lab.

1500 :: {Elder Redding} and a scribe stop by to check on the damage done to
        [Rush's Apartment]. The elder runs into {Transformed Erik} and
        interrogates him.

1505 :: {Jenkins} awkwardly explains what's happening. {Transformed Erik} nearly
        loses his temper with {Elder Redding} before they learn of the radio
        report from the podunk town about a shot Brotherhood member and their
        lost goods.

1515 :: {Major Artemis} leaves to retrieve {Franklin}'s body and their goods.

1815 :: {Major Artemis} arrives and pays the to scavengers (10,000 caps) for
        their recovered goods. He learns the good news about {Franklin} and
        follows them the [Podunk Clinic].

2130 :: {Major Artemis} arrives back at [The Burg] and is met by {Jenkins},
        {Father Murphy}, and {Elder Redding} at [Ida's Laboratory]

2140 :: {Father Murphy} goes to [Rush's Apartment] and chats with {Transformed
        Erik} whom he believes is Rush. They chat about their differing beliefs.
        He also returns (Penny's Urn).

- 10 JUL 2280

0140 :: {Erik} and {Father Murphy} are met by {Elder Redding} and receive the
        good news that Franklin's stable, but not out of the woods. The Father
        returns to the [Chapel]. {Erik} and {Elder Redding} go to [Ida's
        Laboratory].

0200 :: {Ida} performs a blood transfusion from Erik to Franklin. He soon passes
        falls asleep.

0640 :: {Transformed Erik} wakes in pain. {Jenkins} monitors him and realizes
        he's transforming back. {Ida} watches and takes notes.

0643 :: {Erik} completes his transformation back. {Jenkins} helps him to [Rush's
        Apartment] to clean up.

1600 :: {Erik} meets with {Elder Redding} and {Major Artemis} in [Elder
        Redding's Quarters]. They chat and find out that their network has been
        infiltrated and the Elder was left a message. Erik decodes (Dr.
        Hawthorne's Letter to Elder Redding). They argue and {Erik} storms off.

1700 :: {Colonel Summers} retrieves {Terry Summers} and brings him to the
        [Splicer Laboratory] on the [USS Alabama] to see his sister {Emily} for
        the first time in over five years. {Colonel Summers} shoves him in her
        glass enclosure and forces her to kill him.

1725 :: (Terry Summer's Last Message) arrives at Delta.

1730 :: {Colonel Summers} goes to interrogate {Olivia} and {Rush}. She fails to
        cooperate and gets Mesmertron'd and brought to [Laboratory One].

1740 :: {Erik} and {Jenkins} join {Elder Redding} and {Major Artemis} in [Elder
        Redding's Quarters].

2230 :: {Rush} mentally meets {Number 86} who promptly gets the shit kicked out
        of him by an {Enclave Soldier} for hesitating when brining Rush his
        gruel.

- 11 JUL 2280


0750 :: The scientists bring {Olivia} back to her cell after abducting her
        unborn child.

1300 :: {Number 86} brings their meal. He slips a lockpick into {Rush}'s.
        {Colonel Summers} stops buy to gloat about stealing {Olivia}'s child.

1315 :: {Rush} breaks out and into {Olivia}'s cell. She's in bad shape.

= Chapter Thirty Two

  +----------------------+
  |       JUL 2280       |
  | Su Mo Tu We Th Fr Sa |
  |<11 12 13 14 15 16 17 |
  | 18 19 20 21 22>      |
  +----------------------+

- 11 JUL 2280

1930 :: {Erik} and {Jenkins} leave [Elder Redding's Quarters] and head off to
        [Requisitions].

1950 :: Rather than follow their approved plan, the two go to [Tom's Garage] and
        "borrow" the (Caravan Pickup Truck).

2030 :: {Tom} reports the theft, as he promised. {Elder Redding} goes to
        [Major Artemis's Quarters] to give him the news about their Paladins.
        {Willis} comes up and demands to be brought on their rescue mission to
        [Station Magnolia]. They all leave for [Station Magnolia].

2300 :: A huge pack of deer block the road. {Willis} picks the alpha, shoulder
        bashes it and rips off an antler, sending the others into panic and
        fleeing.

2320 :: They arrive at [Station Magnolia].

2335 :: {Elder Redding} updates {Major Artemis} on the current state of affairs
        before the arrive to [Sub Basement 5].

- 12 JUL 2280

0010 :: {Elder Elena}, {Dorian}, and {Harmony} accompany {Elder Redding} and
        {Major Artemis} jump in the car and try to sneak their way out as
        {Willis} fights in the skies.

0030 :: {Willis} takes down the Enclave Vertibirds and watches over their car
        as they drive home.

0040 :: {Jenkins} and {Erik} arrive behind the [USS Alabama] and work their way
        in. {Rush}, {Olivia}, and {Number 86} go to the [Splicer Laboratory] and
        meet {Dr. Norman Reed}. They enlist his help to free the splicers.

0240 :: {Willis} and the car of Brotherhood arrive at [The Burg] safely.

0500 :: {Number 86} and {Rush} go to fetch {Number 87}, but are intercepted by
        {Lieutenant Stevens}. {Number 86} lies to make him leave.

0520 :: {Number 86} and {Rush} run into {Erik}, {Jenkins}, and {Number 92} in
        the cafeteria. {Dr. Norman Reed} and {Olivia} fetch {Emily} and return
        to the [Splicer Laboratory]. The two ladies chat and {Olivia} mentally
        meets her son, {Alto Marshall Rade} for the first time.

0545 :: Everyone meets up in the [Splicer Laboratory] as {Doctor Norman Reed}
        and {Doctor Clifford Reed} argue in the stairwell. A heated argument
        ensues. {Doctor Clifford Reed} stumbles and falls over the railing to
        his death.

0600 :: {Lieutenant Colin McDaniel} and {Number 87} arrive to the [Splicer
        Laboratory]. They split into two team to get everyone above deck to the
        helipads.

0618 :: {Erik} and {Rush} head to the [Power Core] to insert a mininuke and
        initiate a catastrophic failure. {Doctor Norman Reed} makes data
        backups.

0620 :: {Lieutenant Stevens} runs into team one with {Doctor Norman Reed} and
        {Lieutenant McDaniel}. They nearly get shot up, but talk him into
        letting them continue on, under the auspices of taking the splicers out
        and killing them with the Colonel's permission.

0623 :: {Colonel Summers} storms out of the [Control Center] to investigate the
        power problem. He runs into {Lieutenant Stevens}.

0628 :: {Erik}, {Rush}, and {Doctor Norman Reed} are assisted by {Dr. Hawthorne}
        and successfully escape to the truck.

0630 :: Both teams are in Vertibirds in the air and away to their predetermined
        meeting location.

0635 :: {Jenkins}, {Erik}, {Dr. Norman Reed}, {Emily}, {Number 92}, and {Olivia}
        escape the base at [USS Alabama].

0645 :: The [USS Alabama]'s power core explodes, causing a catastrophic failure.

0720 :: They meet up with the splicers at the Vertibirds, chat, and get back on
        the road.

0800 :: {Jenkins} calls [The Burg] and talks to {Elder Redding} to prep him for
        their arrival.

1000 :: They all arrive to [The Burg].

1010 :: Splicers are processed and physicals performed.

1130 :: {Rush} and {Number 92} chat while he gets a physical. {Number 87} and
        {Jenkins} do the same. {Rush} and {Jenkins} find out that {Number 92}
        has a mental deficiency.

---- :: Everyone pitches in to make barracks for the new Hawthornes in the
        [Sniper Tower] near the [Laboratory].

1830 :: {Elder Redding} calls a public meeting of everyone in [The Burg]. He
        introduces each new member and doles out punishment for {Erik} and
        {Jenkins}. They're stripped of rank and put in the brig.

1840 :: {Erik} and {Jenkins} are escorted to [The Brig]. They chat with a
        generic guard.

1950 :: {Knight Penn} escorts {Number 92} to [The Brig] with their dinners.
        {Number 92} gives each of them a sweet roll.

- 13 JUL 2280

---- :: They start their new routine as prisoners.

- 20 JUL 2280

---- :: Same 'ol, same ol'.
1930 :: {Elder Elena}, {Ian}, and {Harmony} accompany {Number 92} and {Knight
        Penn} to the prisoner's dinner meal for a quick visit.

2030 :: {Knight Penn} makes everyone leave.

- 22 JUL 2280

1930 :: {Elder Redding} stops by to give them good news: they're being
        reassigned and lays out their new less-restrictive ground rules.

= Chapter Thirty Three

  +----------------------+
  |       JUL 2280       |
  | Su Mo Tu We Th Fr Sa |
  |               <23 24 |
  | 25 26 27 28 29 30 31 |
  +----------------------+

- 23 JUL 2280

0630 :: {Erik} and {Jenkins} head to the cafeteria for their first breakfast,
        free from incarceration. They see {Franklin} and {Number 92} as they get
        served and meet {Emily}, {Olivia}, and {Willis} at their table. {Erik}
        and {Jenkins} find out about {Number 77}'s suicide.

0640 :: {Erik} leaves to [The Graveyard], east of the church. He runs into
        {Father Murphy} before {Rush} and {Number 86}. The latter two are busy
        digging a fresh grave. {Erik}, {Rush}, {Number 86}, and {Father Murphy}
        have a chat about morality. And the most recent events.

0650 :: {Father Murphy} leaves. {Erik} leaves to help {Ida}. She waves him off:
        he's temporarily demoted. She tells him to have a Hawthorne fetch her
        after curfew.

0715 :: {Erik} goes to [The Sniper Tower] to try and help console his new
        clansmen and continue his investigation.

0900 :: {Erik} goes to [The McDaniel's] on the north side of town to contiune
        investigating. He chats with {Colin} and {Number 86} for a while before
        learning {Number 77} was suffering suicidal tendencies since they left
        the Enclave. {Colin} and {Number 77} argue for a bit about her joining
        the active portion of The Brotherhood. He eventually relents. {Erik}
        learns how {Summers} died -- by {Emily}'s hand.

---- :: {Rush} and {Number 86} chat about funeral customs.

1000 :: {Erik} runs into {Marie} on the way back to the tower. She greets him
        with a full backhand to the face before bear-hugging him. She accosts
        him for getting her husband and child in trouble, {Erik} explains part
        of it away.

1015 :: {Ida} returns the body of {Number 77} over to the clan. {Number 86} and
        {Rush} accept it: they lower the cadaver into the grave in front of the
        waiting grieving party.

1030 :: Everyone available that cares arrives for the funeral. {Elder Redding},
        {Father Murphy}, {Erik}, {Jenkins}, {Olivia}, {Willis}, {Ida}, {Elder
        Elena}, {Doctor Reed}, and all of the members of the clan. Funeral
        proceeds. {Elder Redding} gives a heart-felt speech, followed by {Father
        Murphy}'s Lord's prayer and sermon.

1115 :: Funeral ends with a light summer shower. {Number 86} leads the clan back
        to the [Sniper Tower]. {Erik} joins them. They both take turns along
        with {Number 87} lecturing and comforting the clan. {Elder Elena} rips
        into the clan for their weakness in an attempt to persuade them into
        being stronger.

1140 :: {Erik} moves in with {Number 86} and {Number 92} on the [Sniper Tower:
        Third Deck] across the hall from {Elder Elena} and his cousins. {Erik}
        and {Number 86} chat about the morning's event.

1245 :: {Number 92} comes in, visibly emotional. Angry about not being told
        about {Number 77}. {Number 77} leaves, {Number 86} gives {Erik} more
        backstory of their abuse at the hands of the Enclave. {Number 86} takes
        the name {Seth}. {Seth} tells {Erik} that {Number 77} took the name
        {Ada}. They chat for a while.

1330 :: {Erik} and {Seth} head try for a late lunch. Cafeteria runs out of food.
        {Seth} tries to skeeve on {Erik} and {Rush}'s mental conversation and
        quickly gets reprimanded. {Franklin} kicks {Erik}, {Seth}, and the
        straggling Hawthornes out of [The Cafeteria]. {Erik} drags them all to
        [Linda's Hotel]. Things get awkward quickly when when she refuses to
        serve them. She sends them to [The Stump Jumper].

1340 :: {Seth} sends his small crew off to [The Stump Jumper] without he and
        {Erik}. {Erik} loses control for a moment and mentally abuses {Seth}
        for information. {Seth} mentions that no one can safely transform and
        that {Erik} is a danger to himself and others.

1345 :: {Erik} and crew begin lunch with {Ernie} at [The Stump Jumper].

1420 :: {Seth} and the rest of the Hawthornes leave back to [The Sniper Tower].
        {Erik} thanks {Ernie} for putting their 8 meals on his tab and promises
        to pay him back later before leaving.

1430 :: {Seth} leaves his clan to go to [The Laboratory] in search of {Rush} to
        warn him about {Erik}'s violent mood swings. He finds {Willis} and
        {Jenkins} who tell him a lame biology joke before sending him off to
        [Rush's Apartment]

1435 :: {Seth} asks {Rush} to chat and tells him about {Erik}.

1450 :: {Erik} arrives with a strange temper to [Rush's Apartment] and begins
        shouting about speciesists in town, clearly aggravated at their shunning
        from Linda's earlier.

1315 :: {Doctor Reed} arrives at {Rush}'s behest. He requests {Ida}'s presence.
        They fill {Erik} with depressants for a controlled transformation.
        {Rush} and {Seth} argue over what it means to be a *real Hawthorne*.

1340 :: {Ida} and {Doctor Reed} come back to [Rush's Apartment] to administer
        the next set of drugs. {Doctor Reed} applies the Overdrive and slowly
        chokes {Erik} out to trick his body into fighting for his life.
        The transformation begins slowly.

1350 :: Transformation complete. Action ensues as {Doctor Reed} goes alpha on
        {Erik} and puts him in his place.

1400 :: {Rush} and {Seth} go to [Rush's Office] to escape the fresh bile and
        urine {Erik} left on his kitchen floor. {Seth} tells {Rush} there are
        one or two hundred more Hawthornes at the [Enclave Mobile Base]. That's
        not true any more, but he doesn't know that.

1420 :: {Doctor Reed} checks in on them. The three chat for some time.

1700 :: The three leave [Rush's Apartment] and meet up with {Wills} and
        {Jenkins} on the way to dinner. Erik is still transformed.

1705 :: The five of them get to the entrance and are denied by the guards on
        duty. They reported that a theft by some of the Hawthornes has them on
        lockdown and will not be allowed to enter. They all argue with the
        guards until {Elder Redding} breaks up the growing crowd.

1710 :: {Elder Redding} ushers everyone outside for corporal punishment. Verbal
        bashing of a young scribe further fuels his anger. Everyone gets
        punished, Initiates get it even worse. {Elder Redding}, {Eric}, and
        {Seth} fetch dinner. {Elder Redding} compliments {Number 92}'s job well
        done with Franklin.

1745 :: {Major Artemis} joins the crew at their table. {Elder Redding} announces
        Rush's decision to take the role of Knight Scribe. He's out of the
        scouting parties. Olivia is also leaving the scouting parties to be
        Rush's equivalent in the weapons department.

1755 :: {Rush} and {Seth} chat about {Number 92}. {Seth} goes to fetch his
        brother from the kitchen and gets some free advice from {Frankiln} about
        his brother.

---- :: {Seth} wanders around for a while find finds himself back at the
        cafeteria, now closed.

2015 :: {Seth} leaves the cafeteria and gets into a verbal fight with an armored
        knight who's clearly not okay with their *kind*. {Elder Redding} breaks
        the infantryman down, makes him disrobe, and dishonorably discharges him
        immediately.

2020 :: {Elder Redding} invites {Seth} to [Elder Redding's Apartment] and shoos
        {Franklin} and {Number 92} from the kitchen with {Seth} in tow. {Elder
        Redding} and {Seth} chat for a while about {Number 77}'s death.

2040 :: {Seth} goes home to weep a bit before {Number 92} and {Franklin} come
        by. {Seth} and {Number 92} chat about his little brother's predeliction
        for {Rush} and how that's not going to work. {Seth} deralis that
        conversation by asking {Number 92} if he's chosen a *real* name. He
        chooses "Rick" from *Do Androids Dream of Electric Sheep?*. {Number 92
        -> Rick}. They chat on their way to [Rush's Apartment].

2045 :: They arrive and chat with {Rush} for a while before they start
        drinking whiskey to celebrate {Rick}'s new name. Things go sideways when
        {Rick} gets drunk and tries to make a move on {Rush}. He and {Seth} talk
        him down with talks of cake.

2100 :: {Jenkins} and {Willis} take owership and move the party to [Willis'
        Apartment] down the hall. They find out about the Enclave's abuse of
        their Hawthornes.

2130 :: {Rush} returns to his apartment after {Seth} and {Rick} leave, only to
        find them in [Rush's Office]. {Rush} consoles them again sends them back
        to the [Sniper Tower].

2150 :: {Rush} showers and has the remainder of his Whiskey. {Erik} stops by to
        chat and drop off Rush's copy of Cat's Paw. {Rush} passes out on his
        couch.

= Chapter Thirty Four

 +----------------------+
 |       AUG 2280       |
 | Su Mo Tu We Th Fr Sa |
 |  1  2  3 <4  5> 6  7 |
 |  8  9 10 11 12 13 14 |
 +----------------------+

- 4 AUG 2280

0300 :: {Rush}'s awoken by line printer unscheduled job in [Rush's Office]. He
        investigates the prints and runs straight to [Willis' Apartment] to
        knock the door down.

0315 :: {Rush} wakes {Willis} and convinces him to visit [Rush's Office]. They
        investigate the new prints. Line printer breaks down.

0330 :: They move to [Rush's Apartment].

0630 :: {Rush} and {Willis} finish researching and head directly to the
        [Scribe's Library].

0635 :: {Olivia} spies {Rush} and {Willis} on their way to the Library before
        heading to [Emily's Apartment] for their morning ritual of tea and
        chatting.

0640 :: {Rush} and {Willis} enter the [Scribe's Library] and are made to wait
        for the officer on duty. {Rush} tells them about the unprompted prints
        and the Scribes lose it. Guards are called in from the back and weapons
        are drawn on the two men.

0650 :: {Rush} is sent to the [Brig].

0715 :: {Olivia} leaves [Emily's Apartment] and heads to [Scribe Library].
        {Emily} and head to [Elder Redding's Apartment].

0720 :: {Emily} waits, then enters. She and {Elder Redding} chat about normal
        administrative stuff, planning scout missions and how to quell the
        unrest between the Hawthornes and non-Brotherood civilians.

0735 :: {Irritating Scribe} enters and beings accusations of a mole in their
        computer network to implicate Rush. {Elder Redding} won't stand for her
        bullish, speciesist attitute toward the Hawthornes.

0740 :: {Willis} busts in to annount that {Rush} has been incarcerated. {Elder
        Redding} gives {Irritating Scribe} a verbal beat down and sends her back
        to {Captain Penn}. {Willis} explains the events of a few moments ago.

0745 :: {Emily} takes her new orders to send the scouts into the field and to
        stay with {Major Artemis} for protection. They plan for scrambling the
        scouts and lunch.

0800 :: {Erik} starts class.

0815 :: {Captian Penn} arrives at [Elder Redding's Apartment] with {Irritating
        Scribe}. Scribe leaves and the two argue. {Captain Penn}'s trying to set
        up Rush for stealing data and inciting violence with the other
        Hawthornes.

0945 :: {Emily} arrives at the [Classroom - Sniper Tower] and announces that
        classes will henceforth be cancelled as the Hawthornes return to their
        regular duties. They resume class. {Emily} leaves back to find {Major
        Artemis} and they execute the Elder's remaining orders.

1045 :: {Emily} and {Major Artemis} stumble upon a group of scribes accosting a
        Hawthorne. They sort him out and enter the [Scribe's Library]. {Major
        Artemis} accosts the {Irritating Scribe} and makes her fetch {Knight
        Penn}. {Emily} and the other Hawthorne leave before they start fighting.
        {Major Artemis} rescinds {Knight Penn}'s title and kicks her out of
        their chapter.

1048 :: {Olivia} enters and catches {Knight Penn} off guard, getting herself
        shot with her own prototype rifle. She's thrown back and knocked out.
        {Major Artemis} orders a nearby guard to fetch {Ida} and {Willis}, the
        other to disallow entry.

1100 :: {Ida}, {Willis}, and {Emily} arrive with medical supplies and begin
        taking care of {Olivia} and prying the Major's bullets of out {Knight
        Penn}'s shoulder. {Olivia} slides into shock. {Knight Penn} and the
        {Irritating Scribe} take that cue to exit with the (Plasma Rifle
        Prototype).

1230 :: {Olivia} is delivered to [The Laboratory].

1140 :: {Erik} leads his class to the cafeteria for lunch. He and {Seth} leave
        them to gather supplies and find {Ada} and {Jenkins}.

1200 :: They get their supplies in order and head to the [McBride's House]. They
        find {Jenkins}, {Ada}, and {Colin} there and join them for lunch.

1700 :: {Erik}, {Jenkins}, {Ada}, and {Seth} arrive at the [Burg Southern
        Detachment], later than planned. {Ada} and {Jenkins} chat awkwardly.

1710 :: {Emily} and {Major Artemis} leave to take care of {Rush} and explain the
        morning's events to {Elder Redding}.

1720 :: {Emily} goes to [The Brig] and chats with {Rush} and returns to the
        [Common Building] to see how {Elder Redding} took Major Artemis' report.

1730 :: {Emily} catches up to the older men and chats. She learns that {Elder
        Redding} has brain cancer and their plans to make Erik suceed him as
        Elder after his passing.

1750 :: {Emily} and {Major Artemis} go to the [Magnolia Hotel] to grab a bite,
        but are rejected by {Linda} due to Emily's presence. They're redirected
        to [The Stump Jumper] instead. They eat left over lunches for
        near-dinner and exit.

1755 :: They find a growing crowd of angry traders along their route. {Major
        Artemis} sends {Emily} to free {Rush}. The Major breaks into the
        vendors' argument. He eventually argues with the wealthiest of the
        vendors and punches him in the face, sending him to the ground. They
        argue some more and eventually drags the angry, beaten man to [The
        Laboratory].

1815 :: {Rush} and {Emily} arrive at [The Laboratory]. She leaves him with
        {Willis} and goes to find {Major Artemis} again. He and {Willis} begin
        working on their patients.

1820 :: {Major Artemis}, {Emily}, and the rich caravan owner show up at [The
        Laboratory]. {Rush} sends {Emily} on an errand to his apartment to
        gather data to be provided to {Elder Redding}. {Rush} does surgery and
        his patient wakes up. Nothing major.

1840 :: {Rush} meets the angry caravan leader. He then helps the remaining
        scribes. After setting bones -- painfully, he sends them back home.

1900 :: {Major Artemis} takes the {Angry Caravan Leader} out of [The Laboratory]
        and out to the [The Burg - Western Woods]. They argue for a while, the
        angry dude thinks the Hawthornes will be the downfall of their town,
        based on previous experience.

1920 :: {Major Artemis} and the {Angry Caravan Leader} make it to the clearing.
        They argue for a bit before {Major Artemis} coherces the man to sign an
        agreement to hand over his caravan to the Brotherhood before killing
        him. They're ambused by Enclave-controlled Yao Guai. One eats the angry
        dude, the other attack the Major. Major kills one, but it falls on his
        leg, fracturing it.

1925 :: {Rush} finishes plastering up the scribe's broken bones and his work is
        inspected by {Willis}. He also wants blood from {Rush}, who tells him
        that he and {Olivia} are incompatible and eventually tells his superior
        'no.' That doesn't go over well.

1930 :: {Rush} heads out to the [Scribe's Library]. He argues a bit with the
        guards on duty and enters to find a small group of Brotherhood members
        have corraled up the scribes and are holding them at gunpoint.

1932 :: {Rush} pulls the fire alarm to incite panic and confusion. He breaks
        in to save the scribes and quickly gets into a fight with one of the
        three armed men. They roll around for a bit and he knocks the man out.
        The other two went from complacent to holding hostages. {Rush} fires on
        the left-hand hostage-taker: his stolen plasma rifle overloads and
        vaporizes the man. The one he knocked out came to and jumped on his
        back. They fight. The man slices Rush's neck wide open.

1935 :: A scribe vaporizes the wolf-slicer with a stolen lazer rifle. The last
        hostage-taker kills his hostage in the confusion and escapes. The woman
        who killed the hostage-taker boldly jams her hand into Rush's wound to
        slow the flow of blood while another scribe fetches Willis.

1942 :: {Willis} arrives in record time to save {Rush}'s life with quick
        surgery. The freed scribes free their counterparts in the basement, turn
        on the power and begin cleaning up. {Willis} orders them to clean up,
        take notes of the dead captive's diagram, and to fetch {Father Murphy}
        before taking Rush back to [The Laboratory].

        {Major Artemis} reappears from [The Burg - Western Woods] and makes his
        way to [The Bath House], escorted by a patrolling infantryman. He meets
        Franklin there. After a while, Franklin gets orders to fetch the Major's
        clothing from back home. He gets caught leaving [Major Artemis'
        Apartment] by {Elder Redding} who quickly sizes up the situation. He
        ignores the fact that Franklin snuck some of the Major's liquor at his
        behest of speeding up the Major's return.

2035 :: The proprieter of [The Bath House] runs Major Artemis out and charges
        him extra for making a mess of the place. He argues, but realizes he's
        wrong and pays extra to make up for it after Franklin fetches him some
        clothing.

2045 :: All scribes and engineers arrive to [The Laboratory] for Willis'
        meeting. He talks down the few remaining separationists and sends them
        all on their way.

2100 :: {Ida} sends a message to [The Laboratory] via terminal. The scribes
        notify Willis that they can acces the Library's network from the Lab and
        start researching Knight Brown's plasma rifle with hopes of a cure.

2130 :: {Willis} persuades the remaining Scribes that wanted to separate that it
        was a terrible idea and to stay. He's awesome.

        {Major Aretmis} hobbles back to the [Common Building] and meets with
        {Elder Redding} after a quick swig of Whiskey to ease the pain from his
        leg. He quickly debriefs him and heads to [The Laboratory] for doctoring
        of his own. He walks in to the second lab to find {Rush} and {Olivia}
        napping on the floor after their day of excitement. He wakes up Rush.

2135 :: {Rush} x-rays {Major Artemis}' leg; confirms a fracture. {Rush} fills
        him in on the afternoon's events. They eventually load up {Olivia} into
        a wheelchair and {Rush} delivers them both to [Rush's Apartment].

2200 :: {Major Artemis} goes to [Willis' Apartment] and wakes him up to get an
        update on their experiment's progress. No progress -- {Rush} won't give
        up his blood for fear of being exploited. {Major Aretmis} goes home to
        the [Common Building].

2210 :: {Franklin} breaks into [Major Artemis' Apartment] form him. He finds
        {Emily} asleep in his bed. He instead crashes in [Erik's Apartment].

- 5 AUG 2280

0600 :: {Major Aretmis} is awoken by {Franklin} and {Emily} for a meeting with
        {Elder Redding} and the {Council of Tradesmen}. {Emily} and {Major
        Artemis} head to [Elder Redding's Apartment].

0605 :: They're greeted by {Elder Redding}. Traders asked about missing leader
        {Heath}. {Major Artemis} tells them a great tale about trying to save
        them man's life after the Enclave attacked them and gives them the
        blood-stained agreement to Heath's daughter. The vendors agree to open
        for the day while everyone reviews the agreement.

0630 :: {Emily} and {Major Artemis} chat about the strange meeting they just
        left and went to tend to {Olivia} and the medical crews, respectively.
        {Emily} tells him that she officially accepted the position of Historian
        for the Brotherhood.

0640 :: {Emily} goes to [The Laboratory] to find {Rush} and {Willis}. Neither
        are there, so she tries [Rush's Apartment]. No luck. She tries [Willis'
        Apartment] and finds them arguing about something. The apartment owner
        denies her entrance. She tries again, recommending that {Willis} tell
        {Rush} about {Elder Redding}'s cancer to coerce him into agreeing to
        becoming an experiment again. {Rush} storms off to go speak with
        {Elder Redding}.

0800 :: {Rush} arrives at [Elder Redding's Apartment] to find it empty. {Emily}
        catches up with him. They argue about the Elder's right to know what his
        medical team has planned. Emily breaks him down and drags him back to
        [Willis' Apartment].

---- :: {Willis} draws a half-pint of {Rush}'s blood. {Major Artemis} completes
        his medical rounds. {Knight Olivia} visited in a borrowed wheelchair.

1100 :: {Historian Emily} and {Knight Brown} go to the [Cafeteria] for lunch and
        chat for a while. {Rush} and {Willis} chat. The dog asks the super mutant
        to accompany him to the [Bathhouse] that night.

1300 :: {Seth} and {Franklin} deliver food to {Willis} and {Rush} at [Willis'
        Apartment]. They chat about possible solutions to the Elder's cancer.
        {Rush} takes their dishes to the [Common Buildling] while Willis heads
        to [Dr. Reed's Home] to ask a favor. he and {Dr. Reed} have a heated
        argument after Willis admins to defiling {Number 77}'s corpse in the
        name of science. {Rush} meets Willis just outside and they head to
        [The Laboratory - Basement].

1325 :: {Willis} shows {Rush} (Number 77's Remains). He gets very angry, but
        agrees that a dead Hawthorn cannot complain. The two medics and {Dr.
        Reed} work to get samples from the donation.

1700 :: They complete working on the samples. {Dr. Reed} agrees to meet the
        other two at the [Bathhouse] later, after dinner. They clean up and head
        to [The Cafeteria].

1720 :: {Willis} and {Rush} queue up and get dinner. {Rick} is distracted by his
        long-dead brother's scent lingering on Rush. They finish dinner and
        leave after a conversation on the loneliness of being left behind.

1800 :: They meet {Dr. Reed} and head south and to the [Bathhouse]. They chat
        for a while, get some backstory for the Doctor. {Willis} negotiates a
        special "visit" for {Rush}.

2100 :: {Rush} finishes up. He and {Willis} head home.

= Chapter Thirty Five

 +----------------------+
 |       AUG 2280       |
 | Su Mo Tu We Th Fr Sa |
 |  1  2  3  4 <5  6> 7 |
 |  8  9 10 11 12 13 14 |
 +----------------------+

- 5 AUG 2280

1830 :: {Erik}, {Jenkins}, {Ada}, and {Seth} run into a small caravan on their
        way to Lucydale. They chat for a bit and buy some supplies. {Ada} spends
        most of her cash on a pair of wedding bands for her and McDaniel.

1920 :: They arrive at the [Lucydale Diner] and chat with the owner {Jen} and
        the town's doctor {Jimmy}.

1950 :: Mechanic comes into the bar, claiming to have heard ghosts threaten the
        lives of the town's inhabitants. {Jimmy} brings him back home. He
        returns after {Jen} asks them to take care of their Deathclaw problem.
        They set them up with rooms for the night.

- 6 AUG 2280

0400 :: {Erik} and crew meet in the main room of the diner and plan their next
        moves. They drink real coffee and get into the woods.

0430 :: {Seth} trips and wakes up slumbering acid-spitting plants. They head
        toward a set of buildings marked on the Paladins' maps. {Jenkins} gets
        hit by the acid on the way into the [Army's OGRE Facility].

0530 :: {Seth} and {Ada} break in while {Jenkins} and {Erik} investigate around
        back. Eventually a Mr. Handy comes by to interrogate them, taking the
        two Hawthornes and Erik into custody. He mistakes {Jenkins} to be Dr.
        Hawthorne due to the RFID badge in his bag. The robot locked them all
        up and at Jenkins' behest, made them "full access" RFID badges.

0600 :: {Ada} and {Jenkins} get the broken generator running. {Erik} is freed
        from the holding cell. They split into teams: {Erik} and {Seth} go to
        interrogate the Mr. Handy. The other two continue exploring.

0615 :: {Ada} and {Jenkins} find the [Army's OGRE Facility - Laboratory],
        familiar rejuvenation ponds,  and a pair of long-dead scientists. The
        other two find the robot, and after struggling for a bit, finally get it
        to talk. They get a posthumous video from the Army's Doctor Hawthorne
        and a new way point showing them the [Army's OGRE facility - Satellite
        Downlink Station].

0715 :: The two men catch up with {Ada} and {Jenkins} in the [Army's OGRE
        Facility - Tunnels]

0820 :: The Enclave patrol nears their position. {Seth} and {Ada} easily shoot
        them. The free the two enslaved Hawthornes and interrogate away. {Erik}
        transforms upon learning of the Enclave's "fight for your freedom"
        events on Fridays and transforms to squish one of the Enclave soldiers
        into paste.

0900 :: {Erik} re-joins the group, half-way transformed back to human. They chat
        about their plan for infiltration.

= Chapter Thirty Six

 +----------------------+
 |       AUG 2280       |
 | Su Mo Tu We Th Fr Sa |
 |  1  2  3  4  5 <6> 7 |
 |  8  9 10 11 12 13 14 |
 +----------------------+

0930 :: The crew sends the young men back to the base to find help for
        infiltration. {Erik} and {Jenkins} fight again. {Erik} storms off, {Ada}
        follows. {Jenkins} and {Seth} chat. The younger calls out his superior
        and gets punished with push ups. {Ada} intervenes.

1000 :: The slaves they met earlier go back to their base.

1100 :: The two boys meet {Penny} in the [Corvega R&D Laboratory]. They follow
        her to her office. She chats with them and eventually takes the Holotape
        to play it. It loads the trojan game that will deliver the final blow to
        their network.

1400 :: The boys break the news to {Penny} that their handlers died due to their
        own stupidity. They eventually head back to notify the Brotherhood of
        their progress.

1430 :: {Seth} and {Ada} go with the boys to [Penny's Apartment]. They chat and
        convince {Penny} to betray the Enclave and help them help the enslaved.

1700 :: {Penny} figures out how to disable the collars. She, {Seth}, and {Ada},
        head to the [Corvega R&D Laboratory] to start stage two.

1730 :: Angry scientist barges in to find that {Penny} has freed {Aaron
        Hawthorne} and {Don Hawthorne}. {Ada} locks him in a rejuvenation pod
        to keep them safe.

1745 :: All five escape back to the tunnels. {Erik} meets his biological father
        for the first time. They chat for a while.

1830 :: The two boys from before come running and warn the crew that the Enclave
        has noticed their slaves are missing or unguarded. The squad splits up:
        everyone goes back to the [Army OGRE Facility] except {Penny}, {Erik},
        and {Seth}, who go to cause some mayhem.

1840 :: The three emerge back through the laboratory to find that {Colonel
        Summers} and {Lieutenant Stevens} were manning the pen and ordering a
        growing crowd of Enclave soldiers. They give a speech about how the
        slave should be grateful to them for their "protection" and "kindness"
        before ordering the human-deathclaw hybrids they found out behind the
        pen and shot to be buried in a mass grave.

        Erik spies his own Brotherhood scouts among those captured. Enclave
        soldiers clear out the holding pen and blow up [Penny's Apartment] and
        her neighbors. The remaining, ill or damaged slaves were also blown
        up. The Lieutenant shot into the growing crowd of slaves in the pen,
        sending them fleeing. Erik's ill and nearly-dead scouts walked between
        the man and the other slaves to protect them, all dying in the process.
        The Colonel allows him to kill 2/3 of them, before stopping him for the
        crowd.

        He disperses the crowd and orders the Lieutenant to dispose of the
        remainder. He chose to electrocute the ones wearing collars to death,
        then added insult to injury by smashing the explosion button. He went a
        bit crazy, then left the gate wide open before leaving for his barracks.

1920 :: They wait for the Lieutenant to leave and {Erik} sprints to investigate
        the pen's remnants. He finds a handful of his own men barely alive. He
        accepts their holotags and lays them to rest. He, {Seth}, and {Penny}
        enter the barracks past the less-than-bright guards. {Penny} arranges
        for {Erik} to get cleaned up and a set of Lieutenant's armor to help
        hide his identity. She and {Seth} divert through the kitchen and into
        the basement.

        {Ada}, {Jenkins}, {Aaron H.}, and {Don H.}, and the boys make it through
        the tunnels and into the [Army OGRE Facility - Satellite Downlink
        Station]. Ada hands out water and jerky while Jenkins fiddles with the
        satellite uplink terminal. Jenkins makes contact with the BoS Radiotower
        and is informed that the site is currently under SNAFU (Situation Now
        Absolutely Fucked Up). That, plus weather, will delay a pickup until
        sometime tomorrow. Ada brings the refugees into the unlocked cells and
        bids them to get comfy.

1940 :: A British AI introduces himself as Dr. Hawthorne's execution of his last
        will and testament to keep his children alive. Seth and Ada plan to go
        mess with the tanks left in the tunnel as a last-ditch effort for
        protection.

2030 :: {Erik} socially engineers the Enclave's subordinates into giving him
        entrance into the [Enclave Barracks - Lieutenant Stevens' Suite]. He
        takes his time, disassembling and disemboweling the horrid man. Another
        quick shower and looting leads the transformed Erik to the kitchen.

2100 :: Transformed {Erik} meets {Seth} and {Penny} at the basement. They
        initiate next phase of their plan: have the remaining splicers meet them
        in the laboratory before escaping.

2130 :: Five more splicers meet them at the [Corvega R&D Laboratory]. {Penny}
        initiates a silent countdown and is ordered by the AI to escape into the
        tunnels.

= Chapter Thirty Seven

 +----------------------+
 |       AUG 2280       |
 | Su Mo Tu We Th Fr Sa |
 |  1  2  3  4 <5  6> 7 |
 |  8  9 10 11 12 13 14 |
 +----------------------+

- 5 AUG 2280

0600 :: {Rush} wakes up late to find the lab barren of staff. He wanders to
        [Willis' Apartment] to find he's not there.

0630 :: He goes to the [Cafeteria] to find that it, too, is barren with
        exception of {Rick} in the kitchen. Rick explains through heavy sobs
        that he needs to finish {Elder Redding}'s morning omelet before...
        something happens. Rush sprints up stairs to find {Willis} posting
        guard.

0630 :: Rush goes to visit {Elder Redding} for the last time.

0650 :: {Elder Redding} passes, called by {Ida}. {Major Artemis} is named
        temporary Elder until a new one is voted in. {Willis} is ordered to
        send news to the radio tower for broadcast.

0730 :: The entire base goes to [The Chapel] to attend Elder Redding's funeral.
        Major Artemis and Rush both give eulogies.

0830 :: {Brotherood Separatists} call {Major Artemis} to reopen negotiations for
        removal of the Hawthornes. When he declines, the initiate a
        guerrilla-style attack on their own. Pre-war cars are exploded. Gunfire
        rips through the campus. The Major and ring leader have a good
        old-fashioned fist fights. Olivia runs to the [Scribe's Library].

0832 :: {Willis} runs to the [Commons Building] in the direction of the
        explosions to check on the occupants. {Franklin} runs toward the
        laboratory. {Emily} and {Colin} protect {Father Murphy} in [The
        Sanctuary].

0834 :: {Willis} crashes into the [Commons Building]. He finds some Hawthornes
        captured by the {Brotherhood Separatists} and kills the rogues to save
        them. He escorts them to the [Commons Building - Kitchen] for safety.

        {Rush} runs to the [Scribe's Library] and finds {Olivia} in a defensive
        position. She fires to distract them while {Rush} searches frantically
        for the bomb they planted.

        {Major Artemis} and the leader of the {Brotherhood Separatists} get into
        a gang fight.

0840 :: {Franklin} is shot in the heart by one of the {Brotherhood
        Separatists} while distracting them from [Rush's Apartment] where
        {Rick} was hiding.. The killer is shot in the head by Ida. Both die.

        {Olivia}'s marksmanship and with the help of the other scientists, sends
        the {Brotherhood Separatists} running.

0848 :: {Rush} locates the biological bomb and disarms it with {Olivia}'s help.

0850 :: {Willis} goes to [The Laboratory] and learns about {Franklin}. He breaks
        down in tears.

        The captured scribes are freed. {Rush} sprints off to [The Laboratory]
        while {Olivia} heads to the [Chapel]. She meets {Colin} and {Emily},
        protecting {Father Murphy}. She sets off to find {Major Artemis.}

        {Rush} returns to his apartment to calm {Rick} and gets damaged in the
        young man's panic.

0855 :: {Olivia} finds {Major Artemis}, severely damaged and shot. She
        conscripts some Bathhouse employees to help her ferry {Major Artemis} to
        [The Laboratory] for medical attention. {Ida} orders {Olivia} to find
        Rush and protect/distract Rick. All civilians and minor-injury patients
        are ordered back to their quarters and to lock themselves up.

        In Willis' absence, the bathhouse proprietor helps Ida with minor
        gopher-chores. {Olivia} is sent to relieve {Rush}.

0900 :: {Rush} comes to {Ida}'s aid in Willis' absence to perform surgery on
        {Major Artemis}.

1030 :: {Rush} leaves to fetch {Father Murphy} to bury his son.

1400 :: The dead are all buried: Franklin, his attacker, the {Brotherhood
        Separatists}' ring leader, three of his guard, and a Hawthorne Scribe.

1445 :: {Rush} delivers speech to non-bedridden Brotherhood and civilians. He
        leads them to another funeral to remember the freshly fallen. {Olivia}
        and {Ida} worked on the injured. {Willis} and {Rick} were in {Willis'
        Apartment} getting blackout drunk.

1530 :: {Rush} and {Father Murphy} chat about {Franklin} and {Rick}'s impact on
        him. {Rick} is fetched to visit the Father to help with his sudden
        depression, manifested by muteness.

1630 :: {Rush} returns to [The laboratory] to assist {Ida}. He visits and cares
        for each patient ending with {Major Artemis}. He informs Rush about
        drunk Willis. He persuades the giant back to his apartment.

1730 :: {Rush} goes to the [Radio Tower] and catch's {Jenkins}' radio
        transmission for pickup. He finds {Colin} and {Emily} at [The McDaniels'
        Home] and informs Colin that he has a job early in the morning pick up
        Jenkins, Ada, and the most-injured refugees.

1900 :: {Rush} assists the kitchen staff to prep a late dinner for everyone and
        ensures they eat or have dinner delivered. He delivers plates to
        {Olivia} and {Rick} in his apartment last. He works to help the young
        man through the loss of his best friend and offers to let him stay with
        him until Seth returns from their trip.

1945 :: {Rush} wanders around town and finds a heap of bodies left by {Major
        Artemis}. With {Father Murphy}'s help, he buries them in an unmarked
        mass grave.

2030 :: They return to [The Chapel] and share a bottle of scotch during the
        thunderstorm.

2200 :: {Rush} runs through the rain back to his apartment.

2350 :: {Ada} finds a holotape game and distracts herself with it an her new
        Pip-Boy. {Jenkins} scolds her for staying up, they chat.

- 6 AUG 2280

0010 :: {Jenkins} returns to the radio room to send another message and request
        ETA for pickup.

0030 :: {Erik}, {Seth}, {Penny} and a dozen refugees flow out of the underground
        tunnels into the radio room. Everyone heads upstairs to get some sleep
        while {Erik} and {Seth} chat.


0300 :: {Erik} sets off the planted explosives, collapsing the tunnel and
        setting fires to the [Mobile Enclave Base].

0315 :: {Colin} arrives with a Vertibird to ferry some folks away. {Donald},
        {Aaron}, {Jenkins}, {Ada}, and the two young Hawthorne boys fly to [The
        Burg].

        {Erik}, {Ada}, and {Penny} lead the refugees on a hike.

0530 :: The vertibird arrives back at [The Burg]. {Ida} meets them at the pad
        and escorts them all to [The Laboratory]. She gets {Jenkins} up to speed
        on the past day's events.

0550 :: {Rush} brings fresh breakfast for the staff and patients. Jenkins'
        rucksack trips him up. {Jenkins} informs tell him about his father and
        uncle being in the back. After awkwardly "seeing" them, he informs
        Jenkins that he has a drunk Super Mutant to take care of.

0600 :: {Erik}, {Penny}, and {Seth} arrive to [Lucydale] and find the mayor in
        the [Diner]. She orders him to pay up since he didn't "fix" their
        problems. He does without complaint and buys a *shit ton* of of supplies
        for their trip. (4k caps worth). They return, distribute their fresh
        supplies and head off again.

0630 :: {Rush} returns to help {Jenkins} with {Willis}. They find him in [The
        Laboratory - Brewery]. They chat, {Jenkins} guides Willis back to his
        apartment. {Rush} heads to his own.

0645 :: {Rush} enters his office to find {Olivia}. They enter [Rush's Apartment]
        and find his father, uncle, and the two boys from the Enclave.

0700 :: {Olivia} brings them all to [The Sniper Tower]. {Rush} runs to help
        {Jenkins}. Willis has deeply lacerated his arm in drunken clumsiness.
        They chat for a while and after Rush leaves, Jenkins tells Willis about
        Erik's odd behavior and how he's worried about it leading to the young
        man possibly committing suicide.

1700 :: {Rush} meets {Jenkins} and {Willis} in the [Graveyard]. They chat.

1800 :: {Seth} meanders to find stragglers for last dinner and meets {Father
        Murphy} to "talk" to Franklin's grave.

1900 :: {Willis} and {Jenkins} head off toward [The Laboratory]. {Rush} heads
        home behind them. He's seen by {Ida} and invited to a council
        meeting/poker. He declines.

1915 :: He chats with {Rick} then climbs up to [The Laboratory - Roof Club] and
        weeps himself to sleep.

= Chapter Thirty Eight

 +----------------------+
 |       AUG 2280       |
 | Su Mo Tu We Th Fr Sa |
 |  1  2  3  4  5  6 <7 |
 |  8> 9 10 11 12 13 14 |
 +----------------------+

- 7 AUG 2280

0400 :: {Rush} climbs back into his apartment to find {Rick} cooking breakfast
        for them as thanks for letting him stay.

0445 :: {Rush} leaves into to do some work at the [Scribe's Library], but finds
        it's locked. {Emily} finds him and redirects him to the [Elder's
        Apartment].

0500 :: {Rush} and {Emily} file into the [Commons Buildling] behind {Rick} and
        head up to the [Elder's Apartment]. They discuss how the next elder will
        be chosen.

0630 :: {Rush}, {Emily}, and {Major Artemis} clean up the [Elder's Apartment].
