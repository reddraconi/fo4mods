" Vim Syntax File for 'Outline of Events' *.ol files.
" Language: Outline of Events (*.ol)
" Maintainer: Reddraconi c/o RedSav Studios
" Latest Revision: 01 October 2017

if exists("b:current_syntax")
  finish
endif

" Keywords
syn keyword todos contained TODO FIXME XXX NOTE

" Matches
syn match chapters '^=.*' fold transparent contains=dates,times,comments,characters,datesOfChapter,items,locations
syn match dates '^-.*'
syn match times '\d\d\d\d :: '
syn match times '---- :: '
syn match comments '#.*$' contains=todos

" Regions
syn region characters start="{" end="}" skip="\s+"
syn region datesOfChapter start="<" end=">" skip="\||\s+"
syn region items start="(" end=")"
syn region locations start="\[" end="\]"

" Highlight Rules
hi def link chapters        Constant
hi def link dates           Statement
hi def link times           Type
hi def link comments        Comment
hi def link todos           Todo

hi def link characters
hi def link datesOfChapter
