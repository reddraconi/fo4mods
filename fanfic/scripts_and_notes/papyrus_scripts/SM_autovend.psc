ScriptName SM_autovend extends ObjectReference
; Comments! They're great!



  function check_caps(int cost)
    if (Player.getItemCount Caps001 >= cost)
      Player.RemoveItem f cost
      return true
    else
      showmessage SM_AutoVend_Error
      return false
    endif

  function get_player_barter()
    barter = player.getav barter


  function buy_item(string item_code, int qty)
    if (check_caps(item_code.getGoldValue() * qty))
      Player.AddItem qty item_code
      showmessage SM_AutoVend_Success
    endif
