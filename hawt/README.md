# Hawthornes

This small ESP is a series of matswaps, NPCs and other race-related stuff.
This is a subset of [Furry Fallout][] to just some of the lykaios race to use as
"werewolves" in our main mod. This subset has been made mostly SFW, however the
"naughty" bones, normal maps, and specular maps have been left in place because
those are scary and a pain to remake.

[Furry Fallout]: https://www.nexusmods.com/fallout4/mods/80471

## Attribution

The material swaps are all mine. Meshes in `Meshes/hawthorne/*.nif` started off as content from the
Lykaios race but (are &hellip; not yet) modified or are based on vanilla fallout 4 content.

## Contents

The `reddraconi_hawt.esp` contains the following:
- Meshes
  - Hawthorne {Child,Male,Female} Eyes (NIF and faceBones)
  - Hawthorne {Child,Male,Female} Head (NIF, TRI, and faceBones)
  - Hawthorne {Child,Male,Female} "Naked" Body
  - Hawthorne {Male,Female} "Naked" Hands
  - Hawthorne Tail
  - (TODO): Hawthorne Fur Tufts
  - (TODO): Hawthorne Hair Styles
  - (TODO): Hawthorne Beards
  - Clothing: (These are modified vanilla meshes to fit the Hawthorne/Lykaios bodies)
    - Armored Coat + Gloves
    - Army Fatigues + (TODO) Army Helmet
    - Baseball Uniform + (TODO) Baseball Cap
    - Bathrobe
    - Brotherhood of Steel Squire (Adult-ified)
    - Children of Atom Rags (with and without skirt)
    - Coveralls (based on Sturgiss)
    - Greaser Jacket and Jeans
    - House Dress
    - Knit Cap (regular and hunter variant from my FoFF Mod)
    - Institute Jumpsuit
    - Institute Lab Coat
    - Institute Worksuit
    - Longshoreman Outfit + (TODO) Slicker Hat
    - Pastor's Vestments
    - Postman Uniform
    - Red Dress (Sequined Dress)
    - Red Rocket Jumpsuit
    - Resident 03 &ndash; 08 (06 includes the hood)
    - Short Sleeves and Slacks 01 and 02
    - Sliver Shroud Outfit
    - Sweatervest
    - Synth Uniform Underarmor
    - Tuxedo and (TODO) Hat
    - Valentine's Trench Coat and Suit (no hat)
    - Vault Suit 111 (and no-number)
    - Vault-Tec Salesman Suit
    - Vault-Tec Scientist (and Cabot variant)
    - (TODO) Visor
    - Wastelander Rags 01 and 02 (with optional gloves)

- Material Swaps
  - (TODO): Timberwolf Markings
  - (TODO): Doberman Markings
  - (TODO): 

## Re-Use

Please don't re-use any of the includes meshes. I haven't been able to
reach KrittaKitty, the original author of the Skyrim Lykaios race that was used in
BadPup/BadDog's Furry Fallout mod. I don't think I've used any of BadPup/BadDog's
content in here.

The matswaps are free game for re-use, just don't put them in any paid-for-mods and
give me a shout-out to know how things go.