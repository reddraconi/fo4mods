# /usr/bin/env python

import argparse
import textwrap
import random
from enum import Enum
from os import path
from math import floor


def decide(prob):
  return random.random() < prob


class substitutions(str, Enum):
  # Inspired by Robert Ecker's "Universal Leet Converter v15.04.27", Ultimate
  # Leet mode.
  a = "4"
  b = "6"
  c = "("
  d = "|)"
  e = "3"
  f = "|#"
  g = "6"
  h = "|-|"
  i = "!"
  j = "_)"
  k = "|<"
  l = "1"
  m = "/\\|\\"
  n = "|\\|"
  o = "0"
  p = "|>"
  q = "0"
  r = "|2"
  s = "5"
  t = "7"
  u = "|_)"
  v = "|/"
  w = "|/|/"
  x = "%"
  y = "\\|"
  z = "2"


valid_corruption_types = [
  "network",
  "recovery_omit",
  "recovery_sub",
  "bit_flip",
  "memory",
]

data = {}
separator = "-" * 50
header = ""
body = ""
footer = ""
width = 50
percentage = 0.0
corrupt_all = False
message_to_corrupt = ""
corruption_type = ""
output = ""
chance = 0

parser = argparse.ArgumentParser("Corrupt some ASCII text")
parser.add_argument(
  "-c",
  "--corrupt_all",
  help="Corrupt all output, not just the body.",
  type=bool
)
parser.add_argument(
  "-i", "--input", required=True, help="Input YAML or JSON file"
)
parser.add_argument("-o", "--output", help="Output text file")
parser.add_argument("-t", "--corruption_type", choices=valid_corruption_types)
parser.add_argument(
  "-p",
  "--percentage",
  help="The percentage of the message to corrupt.",
  type=int
)
parser.add_argument(
  "-w",
  "--width",
  help="The terminal or max column width of the output.",
  type=int
)

args = parser.parse_args()

if path.splitext(args.input)[-1].lower() in [".yml", ".yaml"]:
  from yaml import safe_load

  try:
    with open(args.input, "r") as f:
      data = safe_load(f)
  except Exception as e:
    raise (e)

if path.splitext(args.input)[-1].lower() in [".json"]:
  import json

  try:
    with open(args.input, "r") as f:
      data = json.load(f)
  except Exception as e:
    raise (e)

if "separator" in data:
  separator = data["separator"]
else:
  separator = "-" * 50

if "corruption_type" in data["options"]:
  corruption_type = data["options"]["corruption_type"]

if "corrupt_all" in data["options"]:
  corrupt_all = bool(data["options"]["corrupt_all"])

if "percentage" in data["options"]:
  percentage = float(data["options"]["percentage"])

if "width" in data["options"]:
  width = int(data["options"]["width"])

# Overwrite file settings with commandline arguments

if args.corruption_type is not None:
  corruption_type = args.corruption_type

if args.corrupt_all is not None:
  corrupt_all = args.corrupt_all

if args.percentage is not None:
  percentage = args.percentage

if args.width is not None:
  width = args.width

# Check settings for validity

if corruption_type not in valid_corruption_types:
  print(f"WARN: Invalid corruption_type '{corruption_type}'. Using 'network'.")
  corruption_type = "network"

if percentage < 2 or percentage > 90:
  print(
    "WARN: Desired percentage was out of range [2-90]. "
    "Defaulting to '25'."
  )
  percentage = 0.25
else:
  percentage /= 100

if corruption_type == "memory":
  print(
    "WARN: Desired percentage was out of range [10,20,33,40,50,66,70,80] "
    "      for corruption_type of 'memory'. Defaulting to 30."
  )
  percentage = 0.33

if width < 10 or width > 120:
  print("WARN: Width is out of range [10-120]. Defaulting to 50.")
  width = 50

###############################################################################
# Message Creation Time!
###############################################################################

if "header" in data:
  header = textwrap.fill(data["header"], width=width)

if "footer" in data:
  footer = textwrap.fill(data["footer"], width=width)

if "body" not in data:
  print("ERROR: 'body' is required in input file!")
  raise ValueError
else:
  body = textwrap.fill(data["body"], width=width)

# Get configuration file settings first, then overwrite with args if they
# exist.
if corrupt_all:
  message_to_corrupt = f"{header}\n{separator}\n{body}\n{separator}\n{footer}"
else:
  message_to_corrupt = body

new_message = ""

if corruption_type != "memory":
  total_char = len(message_to_corrupt)
  lines = len(message_to_corrupt.splitlines())
  char_to_corrupt = floor(total_char * percentage) - lines
  chance = char_to_corrupt / total_char

  message = message_to_corrupt.splitlines()

  for line in message:
    for char in line:
      if decide(chance):
        if corruption_type == "recovery_omit":
          continue
        elif corruption_type == "recovery_sub":
          try:
            new_message += substitutions[char.lower()].value
          except KeyError:
            new_message += char
      else:
        new_message += char
    new_message += "\n"
else:
  raise NotImplementedError

if not corrupt_all and args.corruption_type != "memory":
  new_message = f"{header}\n{separator}\n{new_message}\n{separator}\n{footer}"

print(textwrap.fill(new_message, width=50))
