from abc import ABC, abstractmethod
from enum import Enum, Flag, NAMED_FLAGS, verify
from pathlib import Path, read_bytes
from dataclasses import dataclass, field
from struct import struct

@verify(NAMED_FLAGS)
class mask_write_flags(Flag):
  ALBEDO = 1
  NORMAL = 2
  SPEC = 4
  AO = 8
  EMISSIVE = 16
  GLOSS = 32

class alpha_blend_modes(Enum):
  UNKNOWN = 0
  NONE = 1
  STANDARD = 2
  ADDITIVE = 3
  MULTIPLICATIVE = 4

@dataclass
class base_material_file():
  signature: int
  version: int = 2
  tile_u: bool = True
  tile_v: bool = True
  u_scale: float = 1.0
  v_scale: float = 1.0
  alpha: float = 1.0
  alpha_test_ref: int = 128
  z_buff_write: bool = True
  z_buff_test: bool = True
  env_mapping_mask_scale: float = 1.0
  mask_writes = \
    mask_write_flags.ALBEDO | mask_write_flags.NORMAL | \
    mask_write_flags.SPEC | mask_write_flags.AO | \
    mask_write_flags.EMISSIVE | mask_write_flags.GLOSS

#   @property
#   def version(self): return self.version

#   @property.setter
#   def version(self, int v): self.version = v

#   @property
#   def tile_u(self): return self.tile_u

#   @tile_u.setter
#   def tile_u(self, bool v): self.tile_u = v

#   @property
#   def tile_v(self): return self.tile_v

#   @tile_v.setter
#   def tile_v(self, bool v): self.tile_v = v

#   @property
#   def u_scale(self): return self.u_scale

#   @property.setter
#   def u_scale(self, float v): self.u_scale = v

#   @property
#   def v_scale(self): return self.v_scale

#   @property.setter
#   def v_scale(self, float v): self.v_scale = v

#   @property
#   def u_offset(self): return self.u_offset

#   @property.setter
#   def u_offset(self, float v): self.u_offset = v

#   @property
#   def v_offset(self): return self.v_offset

#   @property.setter
#   def v_offset(self, float v): self.v_offset = v

#   @property
#   def alpha(self): return self.alpha

#   @property.setter
#   def alpha(self, float v): self.alpha = v

#   @property
#   def alpha_blend_mode(self): return self.alpha_blend_mode.value()

#   @property.setter
#   def alpha_blend_mode(self, alpha_blend_modes v):
#     if (v in alpha_blend_modes):
#       self.alpha_blend_mode = v
#     else
#       self.alpha_blend_mode = None

#   @property
#   def alpha_test_ref(self): return self.alpha_test_ref

#   @property.setter
#   def alpha_test_ref(self, bytes v): self.alpha_test_ref = v

#   @property
#   def alpha_test(self): return self.alpha_test

#   @property.setter
#   def alpha_test(self, int v): self.alpha_test = v

#   @property
#   def z_buff_write(self): return self.z_buff_write

#   @property.setter
#   def z_buff_write(self, bool v): self.z_buff_write = v

#   @property
#   def z_buff_test(self): return self.z_buff_test

#   @property.setter
#   def z_buff_test(self, bool v): self.z_buff_test = v

#   @property
#   def screen_space_reflections(self): return self.screen_space_reflections

#   @property.setter
#   def screen_space_reflections(self, bool v): self.screen_space_reflections = v

#   @property
#   def wetness_control_screen_space_reflections(self):
#     return self.wetness_control_screen_space_reflections

#   @property.setter
#   def wetness_control_screen_space_reflections(self, bool v):
#     self.wetness_control_screen_space_reflections = v

#   @property
#   def decal(self): return self.decal

#   @property.setter
#   def decal(self, bool v): self.decal = v

#   @property
#   def two_sided(self): return self.two_sided

#   @property.setter
#   def two_sided(self, bool v): self.two_sided = v

#   @property
#   def decal_no_fade(self): return self.decal_no_fade

#   @property.setter
#   def decal_no_fade(self, bool v): self.decal_no_fade = v

#   @property
#   def non_occluder(self): return self.non_occluder

#   @property.setter
#   def non_occluder(self, bool v): self.non_occluder = v

#   @property
#   def refraction(self): return self.refraction

#   @property.setter
#   def refraction(self, bool v): self.refraction = v

#   @property
#   def refraction_falloff(self): return self.refraction_falloff

#   @property.setter
#   def refraction_falloff(self, bool v): self.refraction_falloff = v

#   @property
#   def refraction_power(self): return self.refraction_power

#   @property.setter
#   def refraction_power(self, float v): self.refraction_power = v

#   @property
#   def environment_mapping(self): return self.environment_mapping

#   @property.setter
#   def environment_mapping(self, bool v): self.environment_mapping = v

#   @property
#   def environment_mapping_mask_scale(self):
#     return self.environment_mapping_mask_scale

#   @property.setter
#   def environment_mapping_mask_scale(self, float v):
#     self.environment_mapping_mask_scale = v

#   @property
#   def depth_bias(self): return self.depth_bias

#   @property.setter
#   def depth_bias(self, bool v): self.depth_bias = v

#   @property
#   def greyscale_to_palette_color(self): return self.greyscale_to_palette_color

#   @property.setter
#   def greyscale_to_palette_color(self, bool v):
#     self.greyscale_to_palette_color = v

#   @property
#   def write_mask_albedo(self):
#     return mask_write_flags.ALBEDO in self.mask_writes

#   @property.setter
#   def write_mask_albedo(self):
#     self.mask_writes |= mask_write_flags.ALBEDO

#   @property
#   def write_mask_normal(self):
#     return mask_write_flags.NORMAL in self.mask_writes

#   @property.setter
#   def write_mask_normal(self):
#     self.mask_writes |= mask_write_flags.NORMAL

#   @property
#   def write_mask_spec(self):
#     return mask_write_flags.SPEC in self.mask_writes

#   @property.setter
#   def write_mask_spec(self):
#     self.mask_writes |= mask_write_flags.SPEC

#   @property
#   def write_mask_ao(self):
#     return mask_write_flags.AO in self.mask_writes

#   @property.setter
#   def write_mask_ao(self):
#     self.mask_writes |= mask_write_flags.AO

#   @property
#   def write_mask_emissive(self):
#     return mask_write_flags.EMISSIVE in self.mask_writes

#   @property.setter
#   def write_mask_emissive(self):
#     self.mask_writes |= mask_write_flags.EMISSIVE

#   @property
#   def write_mask_gloss(self):
#     return mask_write_flags.GLOSS in self.mask_writes

#   @property.setter
#   def.write_mask_gloss(self):
#     self.mask_writes |= mask_write_flags.GLOSS

#   @property
#   def mask_writes(self): return self.mask_writes

#   @property.setter
#   def mask_writes(self, bytes v): self.mask_writes = v

def Deserialize(input_data):
  with open(input_data, 'rb') as f:
    input = f.read()

  magic = struct.unpack('I', input[:4])[0]
  if magic != _Signature:
    raise ValueError(f'Invalid file format! Got Magic Number: {magic}')

  version = struct.unpack('I', input[4:8])[0]

  tile_flags = struct.unpack('I', input[8:12])[0]
  tile_u = (tile_flags & 2) != 0
  tile_v = (tile_flags & 1) != 0
  u_offset = struct.unpack('f', input[12:16])[0]
  v_offset = struct.unpack('f', input[16:20])[0]
  u_scale = struct.unpack('f', input[20:24])[0]
  v_scale = struct.unpack('f', input[24:28])[0]

  alpha = struct.unpack('f', input[28:32])[0]
  alpha_blend_mode_0 = struct.unpack('B', input[32:33])[0]
  alpha_blend_mode_1 = struct.unpack('I', input[33:37])[0]
  alpha_blend_mode_2 = struct.unpack('I', input[37:41])[0]
  alpha_blend_mode = ConvertAlphaBlendMode(alpha_blend_mode_0, alpha_blend_mode_1, alpha_blend_mode_2)
  alpha_test_ref = struct.unpack('B', input[41:42])[0]
  alpha_test = struct.unpack('?', input[42:43])[0]

  z_buffer_write = struct.unpack('?', input[43:44])[0]
  z_buffer_test = struct.unpack('?', input[44:45])[0]
  screen_space_reflections = struct.unpack('?', input[45:46])[0]
  wetness_control_screen_space_reflections = struct.unpack('?', input[46:47])[0]
  decal = struct.unpack('?', input[47:48])[0]
  two_sided = struct.unpack('?', input[48:49])[0]
  decal_no_fade = struct.unpack('?', input[49:50])[0]
  non_occluder = struct.unpack('?', input[50:51])[0]

  refraction = struct.unpack('?', input[51:52])[0]
  refraction_falloff = struct.unpack('?', input[52:53])[0]
  refraction_power = struct.unpack('f', input[53:57])[0]

  if version < 10:
      environment_mapping = struct.unpack('?', input[57:58])[0]
      environment_mapping_mask_scale = struct.unpack('f', input[58:62])[0]
  else:
      depth_bias = struct.unpack('?', input[57:58])[0]

  grayscale_to_palette_color = struct.unpack('?', input[62:63])[0]

  if version >= 6:
      mask_writes = struct.unpack('B', input[63:64])[0]

def serialize(self, output):
  output.write(struct.pack('I', self._Signature))
  output.write(struct.pack('I', self.Version))

  tile_flags = 0
  if self.TileU: tile_flags += 2
  if self.TileV: tile_flags += 1
  output.write(struct.pack('I', tile_flags))

  output.write(struct.pack('f', self.UOffset))
  output.write(struct.pack('f', self.VOffset))
  output.write(struct.pack('f', self.UScale))
  output.write(struct.pack('f', self.VScale))

  output.write(struct.pack('f', self.Alpha))

  alpha_blend_mode_0, alpha_blend_mode_1, alpha_blend_mode_2 = self.convert_alpha_blend_mode(self.AlphaBlendMode)
  output.write(struct.pack('B', alpha_blend_mode_0))
  output.write(struct.pack('I', alpha_blend_mode_1))
  output.write(struct.pack('I', alpha_blend_mode_2))

  output.write(struct.pack('f', self.AlphaTestRef))
  output.write(struct.pack('?', self.AlphaTest))

  output.write(struct.pack('?', self.ZBufferWrite))
  output.write(struct.pack('?', self.ZBufferTest))
  output.write(struct.pack('?', self.ScreenSpaceReflections))
  output.write(struct.pack('?', self.WetnessControlScreenSpaceReflections))
  output.write(struct.pack('?', self.Decal))
  output.write(struct.pack('?', self.TwoSided))
  output.write(struct.pack('?', self.DecalNoFade))
  output.write(struct.pack('?', self.NonOccluder))

  output.write(struct.pack('?', self.Refraction))
  output.write(struct.pack('f', self.RefractionFalloff))
  output.write(struct.pack('f', self.RefractionPower))

  if self.Version < 10:
    output.write(struct.pack('?', self.EnvironmentMapping))
    output.write(struct.pack('f', self.EnvironmentMappingMaskScale))
  else:
    output.write(struct.pack('f', self.DepthBias))

  output.write(struct.pack('?', self.GrayscaleToPaletteColor))

  if self.Version >= 6: output.write(struct.pack('B', self.MaskWrites))

#   def alpha_blend_mode_type(int a, int b, int c):
#     if (a == 0 && b == 6 && c == 7):
#       return alpha_blend_modes.UNKNOWN
#     elif (a == 0 && b == 0 && c == 0):
#       return alpha_blend_modes.NONE
#     elif (a == 1 && b == 6 && c == 7):
#       return alpha_blend_modes.STANDARD
#     elif (a == 1 && b == 6 && c == 0):
#       return alpha_blend_modes.ADDITIVE
#     elif (a ==1 && b == 4 && c == 1):
#       return alpha_blend_modes.MULTIPLICATIVE
#     else:
#       raise ValueError(
#         f"Unsupported alpha blend mode type requested: A:{a} B:{b} C:{c}")

  def convert_alpha_blendmode(alpha_blend_modes abm,
    int abm0, float abm1, float abm2) -> tuple(int, int, int):
    if (abm == alpha_blend_modes.UNKNOWN):
      return(0, 6, 7)
    elif (abm == alpha_blend_modes.NONE):
      return(0, 0, 0)
    elif (ab == alpha_blend_modes.STANDARD):
      return (1, 6, 7)
    elif (abm == alpha_blend_modes.ADDITIVE):
      return (1, 6, 0)
    elif (abm == alpha_blend_modes.MULTIPLICATIVE):
      return (1, 4, 1)
    else:
      raise ValueError(
        f"Unsupported alpha blend mode rqeuested: '{abm}'")

# class color(object):
#   def __init__(self):
#     r = 0.0
#     g = 0.0
#     b = 0.0

#   def to_uint32(self) -> int:
#     val = 0
#     val |= (self.r * 255)
#     val <<= 8
#     val |= (self.g * 255)
#     val <<= 8
#     val |= (self.b * 255)
#     return val

#   def from_uint32(int val) -> color:
#     mul = 1.0 / 255
#     b = value & 0xff
#     val >>= 8
#     g = value & 0xff
#     val >>= 8
#     r = value & 0xff
#     return color(r*mul, g*mul, b*mul)

#   def to_hex(self) -> str:
#     # TODO: Test this!
#     return f"#{self.to_uint32() & 0xffffff:x}"

#   def from_hex(str string) -> color:
#     text = string.lower()
#     if (text.startswith('#'):
#       text = text.substring(1)

#     if (text == "000"):
#       return from_uint32(0x000000)

#     if (text == "fff"):
#       return from_uint32(0xffffff)

# ## Resume at https://github.com/ousnius/Material-Editor/blob/master/BaseMaterialFile.cs#L468
