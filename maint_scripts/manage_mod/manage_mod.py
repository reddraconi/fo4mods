#!/usr/bin/env python

import argparse
import configparser
import logging
import re
import zipfile
from datetime import datetime
from os import makedirs, walk
from os.path import abspath, dirname, exists, isfile, join, normpath, relpath
from pathlib import Path
from platform import system
from re import IGNORECASE, match
from shutil import copy, rmtree
from string import Template
from sys import exit
from textwrap import dedent

if system() != "Windows":
  from os import mknod

# Globals ######################################################################

APPNAME = "ManageMod"
did_create_robocopy_job = False

# Reusable Functions ###########################################################


def make_dir_path(target: Path) -> None:
  if not exists(target):
    try:
      makedirs(target, exist_ok=True)
    except Exception as e:
      logg("c", f"An error occurred when attempting to create {target}: {e}")
  else:
    logg("w", f"Found {target}. Not overwriting")


def toss_dir_path(target: Path) -> None:
  try:
    logg("d", f"Tossing {target}")
    rmtree(target)
  except Exception as e:
    logg("c", f"Unable to toss directory {target}: {e}.")


def zip_folders(folders, output_name: str) -> None:
  logg("i", f"Archiving project {args.project} to {output_name}.")
  output_file = zipfile.ZipFile(output_name, "w", zipfile.ZIP_DEFLATED)

  for f in folders:
    for dirpath, dirnames, filenames in walk(f):
      for fn in filenames:
        logg("dd", f"Archiving: {abspath(fn)}.")
        output_file.write(
          join(dirpath, fn),
          relpath(join(dirpath, fn), join(folders[0], "../..")),
        )

  logg("dd", "Archival complete.")
  output_file.close()


def confirmation(prompt: str) -> bool:
  response = None
  while response not in ("y", "n"):
    response = input(f"{prompt} (y/n)> ").lower().strip()[:1]

  return True if response == "y" else False


# Parameter Handling ###########################################################

ap = argparse.ArgumentParser()
ap.add_argument(
  "-p", "--project", help="Name of your mod. No spaces!", required=True
)
mxgroup = ap.add_mutually_exclusive_group(required=True)
mxgroup.add_argument(
  "-n", "--new", help="Setup a new project.", action="store_true"
)
mxgroup.add_argument(
  "-d", "--delete", help="Removes a project.", action="store_true"
)
ap.add_argument(
  "-a",
  "--archive",
  help="Archives a project during removal.",
  action="store_true"
)
ap.add_argument("-c", "--config", help="Runtime configuration.")
ap.add_argument(
  "-v",
  "--verbose",
  action="count",
  default=0,
  help="Verbosity of output. Stacks up to '-vv'. Quiet by default.",
)
ap.add_argument(
  "-l", "--log", help="Log file location.", default="./manage_mod.log"
)

args = ap.parse_args()

if not args.new and not args.delete:
  ap.print_help()

# Get Configuration For the Manager ############################################
config = configparser.ConfigParser()

if args.verbose > 0:
  print("Checking for configuration file.")

if args.config is None or not exists(args.config) and not args.delete:
  print("Warning: No config provided or couldn't read it. Using defaults.")
  if not confirmation("Is this okay?"):
    exit("Diving out at the user's request!")

  config["DEFAULT"] = {
    "author":
      "no_author",
    "email":
      "no_email@domain.tld",
    "data_dir":
      "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Fallout 4\\Data",
    "dev_dir":
      ".",
    "log":
      "./managemod.log",
  }
else:
  try:
    config.read(args.config)
  except Exception as e:
    exit("Warning: Unable to parse provided config!")

config["DEFAULT"]["project"] = args.project
conf = config["DEFAULT"]

# Application Logging ##########################################################


class colorized_logging_formatter(logging.Formatter):
  debug = "\x1b[35;20m"  # purple
  informational = "\x1b[38;20m"  # grey
  warning = "\x1b[93;20m"  # yellow
  error = "\x1b[33;20m"  # orange
  critical = "\x1b[31;20m"  # red
  reset = "\x1b[0m"

  ch_format = "%(name)-11s:[%(levelname)-8s] %(message)s"
  FORMATS = {
    logging.DEBUG: debug + ch_format + reset,
    logging.INFO: informational + ch_format + reset,
    logging.WARNING: warning + ch_format + reset,
    logging.ERROR: error + ch_format + reset,
    logging.CRITICAL: critical + ch_format + reset,
  }

  def format(self, record):
    log_fmt = self.FORMATS.get(record.levelno)
    formatter = logging.Formatter(log_fmt)
    return formatter.format(record)


def setup_logging(log_dir: str, debug: bool = False) -> None:

  log_file = Path(args.log)

  if not isfile(log_file):
    try:
      if system() == "Windows":
        open(log_file, "w").close()
      else:
        mknod(log_file, 0o644)
    except Exception as e:
      print(f"Unable to create log file {log_file}: {e}")
      exit(1)

  logger = logging.getLogger(APPNAME)
  logger.setLevel(logging.DEBUG)

  ch = logging.StreamHandler()
  fh = logging.FileHandler(log_file)

  if not debug:
    ch.setLevel(logging.WARNING)
    fh.setLevel(logging.INFO)
  else:
    ch.setLevel(logging.DEBUG)
    fh.setLevel(logging.DEBUG)

  # create basic formatter
  fh_format = logging.Formatter(
    fmt="%(asctime)s %(name)-11s [%(levelname)-8s] %(message)s",
    datefmt="%Y%m%d %H:%M:%S%z",
  )
  # add formatter to ch
  ch.setFormatter(colorized_logging_formatter())
  fh.setFormatter(fh_format)

  # add ch to logger
  logger.addHandler(ch)
  logger.addHandler(fh)

  logger.debug("Logger subsystem initalized.")


def logg(severity: str = "info", message: str = ""):
  """Generates a syslog message.

    Args:
        severity (str, optional): Log level. Defaults to "info". Also accepts
            "debug", "exception", "critical", "warning", and "error".
            For extra logging, "dd" or "ddebug" can be used.
            "critical" will cause the application to exit.
            "exception" is special: it will append a stack trace to the log.
        message (str, optional): The log message. Defaults to "".
    """
  logger = logging.getLogger(APPNAME)

  if match("dd", severity, IGNORECASE) and args.verbose > 1:
    logger.debug(message)
  elif match("d", severity, IGNORECASE) and args.verbose > 0:
    logger.debug(message)
  elif match("ex", severity, IGNORECASE):
    logger.exception(message)
    logging.shutdown()
  elif match("c", severity, IGNORECASE):
    logger.critical(message)
    logging.shutdown()
    exit(1)
  elif match("w", severity, IGNORECASE):
    logger.warning(message)
  elif match("e", severity, IGNORECASE):
    logger.error(message)
  else:
    logger.info(message)


if args.verbose > 1:
  setup_logging(normpath(conf["log"]), debug=True)
else:
  setup_logging(normpath(conf["log"]))

# Validate User Input ##########################################################

restricted_charset = re.compile(r"[a-zA-Z0-9_-]+")

logg("d", "Checking user input.")
logg("dd", "Validating project name")

if not re.fullmatch(restricted_charset, conf["project"]):
  logg(
    "c",
    "Project name can only contain letters, numbers, underscores, and dashes."
  )

data_dir = Path(conf["data_dir"])
dev_dir = Path(conf["dev_dir"])

logg("dd", f"Validating access to data_dir: {data_dir}")

try:
  data_dir.lstat()
  logg("d", f"Found {data_dir}")
except FileNotFoundError as e:
  logg("c", f"Couldn't find or access {data_dir}! Error: {e}")

logg("dd", f"Validating access to dev_dir: {dev_dir}")

if not exists(dev_dir):
  logg("c", f"Unable to access dev_dir {dev_dir}!")
else:
  logg("d", f"Found {dev_dir}")

mod_folders = (
  "_assets",
  "_documentation",
  "Materials",
  "Meshes",
  "OPAL",
  "Textures",
  "Scripts",
  "Sound",
  "Video",
)

# Create a New Project #########################################################

if args.new:
  logg("d", "Creating a new project!")
  logg("d", "Checking for directories.")

  for f in mod_folders:
    current_dev_dir = abspath(join(dev_dir, conf["project"], f))

    logg("dd", f"Checking for {current_dev_dir}")

    make_dir_path(current_dev_dir)

    if not re.match("_", f) and not re.match("OPAL", f):
      current_data_dir = abspath(
        join(data_dir, f, conf["author"], conf["project"])
      )

      logg("dd", f"Checking for {current_data_dir}")

      make_dir_path(current_data_dir)
    elif re.match("OPAL", f):
      current_data_dir = abspath(join(data_dir, "..", f))

      logg("dd", f"Checking for {current_data_dir}")

      make_dir_path(current_data_dir)
    else:
      pass

  ## Make custom paths that don't conform to other Data subfolder layouts

  compiled_scripts_path = [
    dev_dir,
    conf["project"],
    "Scripts",
    conf["author"],
    conf["project"],
  ]

  raw_scripts_path = [
    dev_dir,
    conf["project"],
    "Scripts",
    "Source",
    "User",
    conf["author"],
    conf["project"],
  ]

  object_pallet_path = [dev_dir, conf["project"], "OPAL"]

  make_dir_path(abspath(join("", *compiled_scripts_path)))
  make_dir_path(abspath(join("", *raw_scripts_path)))
  make_dir_path(abspath(join("", *object_pallet_path)))

  # Check for the existance of a robocopy job. (Windows only) ##################

  template_substitution = {
    "author": conf["author"],
    "project": conf["project"],
    "data_dir": conf["data_dir"],
    "dev_dir": conf["dev_dir"],
  }

  if system() == "Windows":
    robocopy_job_template = join(
      dirname(abspath(__file__)), "robocopy.template"
    )
    robocopy_job = abspath(
      join(dev_dir, conf["project"], f"{conf['project']}_rcj.ps1")
    )

    logg("dd", f"Checking for robocopy job {robocopy_job}.")

    if not exists(robocopy_job):
      with open(robocopy_job_template, "r") as rcjt:
        tmpl = Template(rcjt.read())
        rcj_output = tmpl.substitute(template_substitution)

      logg("dd", "Writing robocopy jobs.")
      with open(robocopy_job, "a") as rcj:
        rcj.write(rcj_output)

      did_create_robocopy_job = True
    else:
      logg("w", "Found a robocopy job! Not overwriting.")

  # Instead of robocopy, we'll create a bindmount script for non-Windows systems

  if system() != "Windows":
    bindmount_template = join(dirname(abspath(__file__)), "bindmount.template")
    bindmount_script = abspath(
      join(dev_dir, conf["project"], f"{conf['project']}_bma.sh")
    )

    logg("dd", f"Checking for bindmount script {bindmount_script}.")

    if not exists(bindmount_script):
      with open(bindmount_template, "r") as bmt:
        logg("dd", "Writing bindmount script.")
        tmpl = Template(bmt.read())
        bm_output = tmpl.substitute(template_substitution)

      with open(bindmount_script, "a") as bm:
        bm.write(bm_output)

      did_create_bindmount_job = True
    else:
      logg("w", "Found a bindmount script! Not overwriting.")

  ## OS-dependant solutions complete ###########################################

  readme_file = abspath(join(dev_dir, conf["project"], "README.md"))

  logg("dd", f"Checking for a README.md at {readme_file}.")

  if not exists(readme_file):
    logg("i", f"Creating a default readme: {readme_file}.")
    with open(readme_file, "a") as readme:
      readme.write(
        f"# {conf['project']}\n\nAuthor: {conf['author']} <{conf['email']}>\n"
      )
  else:
    logg("w", "Found README.md! Not overwriting.")

  mod_filename = f"{conf['author']}_{conf['project']}.esp"

  if not exists(abspath(join(dev_dir, mod_filename))):
    logg("i", f"{mod_filename} not found in {dev_dir}.")
    if system() == "Windows":
      try:
        copy(
          abspath(join(data_dir, mod_filename)),
          abspath(join(dev_dir, conf["project"], mod_filename)),
        )
      except FileNotFoundError:
        logg(
          "w",
          f"{mod_filename} not found in {data_dir}! Can't copy to {dev_dir}!",
        )
      except Exception as e:
        logg("w", f"Something unexpected occured: {e}")
  else:
    logg("dd", f"Found {mod_filename}.")

## Remove a Project

if args.delete:
  logg("d", "Deleting a project!")
  if not args.archive:
    print(
      dedent(
        f"""
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! W O A H    T H E R E ,  C O W ( B O Y | G I R L )                      !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    You passed the delete flag (-d | --delete) without the archive flag
    (-a | --archive)! That means your project files will be PERMANENTLY deleted
    from the dev and data directories.

    """
      )
    )

    if not confirmation("Is this what you intended?"):
      logg("c", "Diving out at the user's request!")

  if args.archive:
    print(
      dedent(
        f"""
    Now Archiving.
    Note:
      1. Empty directories are not captured by the python zip formatter.
      2. Robocopy jobs are not archived, as they're automatically created.
    """
      )
    )

    datentime = datetime.now().strftime("%Y%m%d_%H%M")
    archive_name = f"{conf['author']}_{conf['project']}_{datentime}.zip"
    logg("d", f"Creating zip: ./{dev_dir}/{archive_name}")

    try:
      zip_folders([abspath(join(dev_dir, conf["project"]))],
                  f"{dev_dir}/{archive_name}")
    except Exception as e:
      logg("c", "Unable to create archive: {e}")

    logg("dd", "Archival complete.")

  current_dev_dir = abspath(join(dev_dir, conf["project"]))
  toss_dir_path(current_dev_dir)

  for f in mod_folders:
    if not re.match("_", f):
      logg("dd", f"Investigating {f}...")
      if not re.match("OPAL", f):
        current_data_dir = abspath(
          join(data_dir, f, conf["author"], conf["project"])
        )
      else:
        current_data_dir = abspath(join(data_dir, "..", f))
      toss_dir_path(current_data_dir)
    else:
      logg("dd", f"Skipping {f}...")

logg("i", "ManageMod execution complete.")
