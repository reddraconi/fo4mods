# Jumpers

Does your wastelander often find themselves covered in grease, lubricant, or other slimy, visceral coatings? Get 'em
a jumper!

These adult onesies are perfect for mechanics and other greasemonkies.

Each mesh comes with three options: clean(-ish), dirty, and bloody.

## Yellow

It looks like one of the MST3K crew got lost!

## Patriotic

Show the Commies that *these colors don't run* when you're working on your trusty Highwayman.

## Olive

A classic Army green color, perfect for hiding in low-lying brush or wrenching on your "borrowed" tank.

## Black

Show off for the other racers with flames or speed stripes! As a bonus, it hides stains really well.

## Attribution:

I hand-crafted the material swaps myself, but re-used the in-game meshes for the RedRocket jumper. The other two mesh
types are custom, but were both based on the same RedRocket jumpsuit as the one used for the material swaps.
