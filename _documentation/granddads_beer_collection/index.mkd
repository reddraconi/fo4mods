# Grandad's Beer Collection

We've gathered four of our favorites from good ol' Grandad's brewery.

Fresh (or only *slightly* stale) from the swamps of the Southeastern
Commonwealth to your belly!

Each of these beers have empty, warm, and ice-cold variants. Each variant includes an intact and ripped-label version.

This also includes some PackIns to make set dressing easier.

- Moveable beer crates (Bucksweat)
- Moveable beer crates (Pine Tar)
- Moveable beer crates (Swampwater)
- Moveable beer crates (Turpentine)
- Moveable beer crates (empty, mixed ripped labels, 3 variants)

Each beer crate comes in:

- fully-packed warm
- fully-packed ice-cold
- partially-packed warm with some ripped labels

All of the beers have the same base statistics. If there is a deviation,
it will be noted after the item.

```.text
Value: 5c                Weight: 1lb

Effects:                 Addictions:
+ 1 Strength (180s)      Alcohol Addiction (15%)
+ 1 Charisma (180s)      - 1 Charisma
- 1 Intelligence (180s)  - 1 Agility
+ 35 AP (ice cold only)

Crafting Component Breakdown:
2 Glass
```

## Bucksweat Amber Ale

![Bucksweat Amber Ale](./img/label_bucksweat_full.jpg)

> This golden beer is perfect for a beach trip, river rafting, or just kicking
> back after a hard day's work of clearing out ne'er-do-wells.
>
> Great warm or cold, which great, because it's mostly found lukewarm.

## Pinetar Lager

![Pinetar Lager](./img/label_pinetar_full.jpg)

> A classic lager for a classic swamper. Pairs well with everything from roasted
> molerat to fried tatos.

## Swampwater Cream Stout

![Swampwater Cream Stout](./img/label_swampwater_full.jpg)

> If you like beer darker than a slaver's (non-existent) soul, this is the brew
> for you! Chocolately with a Brahmin cream finish, you'll wonder how Grandad
> packed so much flavor into this brown bottle.
>
> On second thought, it'd be best *not* to ask.

```.text
Effects:
- 10 Rads over 2s
```

## Industrial Turpentine IPA

![Industrial Turpentine IPA](./img/label_turpentine_full.jpg)

> Best described as "beer with teeth," this IPA is not for the faint of heart.
> Those that dare pair this elixir with spicy Cajun food appreciate the floral,
> citrus finish.
>
> Also works well for removing paint -- just remember your respirator and don't
> eat the chips. (Does not contain actual teeth).

```.text
Effects:
+ 35 AP
+ 45 AP (ice cold only)
```

* * *

This set also includes material swaps for the hard liquors found in the wastes.

> **TODO**: Here

## Attribution

These are all material swaps for the base game `BeerGwinnettBrew` meshes.
