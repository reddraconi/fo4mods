# Grandfather

These are letters written by the NPC "Grandfather", used within the actual text of the fan fiction.

## Enclave letter

This was an email sent to "The Burg" about Enclave capture of Rush and Olivia.

```.text
TheyhavecapturedRushandhisfemalefri\
endIcannothelpthemfromhereIknowwhat |
theyareplanningPleasegobacktomyhome |
andsave\___________________________\|
therema |
inderof |
mychild |
\______\|
renMrMarshallIbegofyoupleasebefore\
\__________________________________\
theydestroytherestofmyprogenybring\
\__________________________________\
themtoyourhomeandkeepthemsafeYoudon\
\__________________________________\
otoweme\
butyouo |
yourson |
sthatmu |
chIwillcontinuetomonitortheiractivi\
tyandintervienewhenpossibleCoordina |
tesarefurtherencodedinthismessage.  |
___________________________________\|
```
