# Fallout Brotherhood Mod

This is a superset of the smaller projects within this repository.

## [TODO](./todo.mkd)

A list of miscellaneous stuff we need to do to the mod.

## [Vars](./variables.mkd)

A list of variables used within the mod to process script and NPC statuses.

## [Characters](./characters/index.mkd)

Characters (NPCs) that appear in the add-on

## [Quests](./quests/index.mkd)

Quests, sorted by geographic location

## [Dialog](./dialog/index.mkd)

Rough dialog trees for quests and around-town chatter.

## [Items](./items/index.mkd)

DLC-only items that haven't been split out into their own mini-DLCs.

## [Locations](./locations/index.mkd)

Geographic locations including notes, maps, and images.

## [Factions](./factions/index.mkd)

Faction rank backgrounds and rank tables.
