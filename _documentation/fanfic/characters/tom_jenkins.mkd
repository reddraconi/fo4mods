 # Thomas "Tom" Jenkins

 Mechanic and co-owner of M<sup>c</sup>Daniel's Metalworks

|           |                     |
|-----------|---------------------|
| Age       | |
| Birthday  | |
| Gender    | |
| Height    | |
| Weight    | |
| Blood type|  |
| Sexuality | |

## Physical Characteristics

## Background

| Family     |                            |
|------------|----------------------------|
| Mother     | Unknown                    |
| Father     | Unkown                     |
| Sibling(s) | *None*                     |
| Mate       | [Marie Jenkins][]          |
| Child(ren) | [Axel Jenkins][] (Adopted) |

## References

[Axel Jenkins]: axel_jenkins.mkd
[Marie Jenkins]: marie_jenkins.mkd
