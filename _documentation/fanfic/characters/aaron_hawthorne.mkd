# Aaron Hawthorne

Hawthorne Clan Elder &mdash; Station Magnolia Research Facility

![aaron_hawthorne](./img/aaron_hawthorne.jpeg)

## Characteristics

|               |        |                 |                                                          |
| :------------ | :----- | :-------------- | :------------------------------------------------------- |
| **Age:**      | 51     | **Build**       | Medium                                                   |
| **Height:**   | 6' 5"  | **Weight:**     | 240lbs                                                   |
| **Gender:**   | Male   | **Sexuality:**  | 1 Kinsey                                                 |
| **Birthday:** | 25 Aug | **Birthplace:** | [Station Magnolia Research Facility][], The Burg, MS, US |

[Station Magnolia Research Facility]: ../locations/ph/smrf.mkd

|                |                                                                    |
| :------------- | :----------------------------------------------------------------- |
| **Ethnicity:** | Hawthorne                                                          |
| **Skintone:**  | Dark Caucasian                                                     |
| **Eyes:**      | Amber, lumiesce in dark                                            |
| **Hair:**      | Fur medium dark coffee color. Graying along muzzle. Unruly cowlick |

Fur patterns resemble a timber wolf. Not overly muscular like a Supermutant, but has musculature like a swimmer: strong,
not bulky. Like all Hawthornes, he has a maw full of sharp, white teeth with elongated canines. Sports a unique cowlick,
hereditary to males of his clan.

Fur Colors:

-   RGB: 57, 34, 22
-   RGB: 162, 145, 139
-   HEX: #6f4e37

## Background

Third-generation Hawthorne, he is half of a birth pair along with his brother. He and his brother served the clan as
Beta and Alpha, respectively, for twenty-five years. He and his brother were captured after the raid of [Red Creek][] by
an Enclave task force that would eventually splinter off to become the muscle for the [Panther Swamp Slavers][].

[Panther Swamp Slavers]: ../factions/panther_swamp_slavers.mkd
[Red Creek]: ../locations/ph/red_creek.mkd

Aaron and his brother Donald were kept under the care of [Dr. Clifford Reed][] as specimens in-and-out of suspended
animation, unwillingly aiding the Enclave to improve their splicing capabilities at [USS Alabama][] where the Enclave's
"faulty" splicers were kept, tested upon, and "employed".

[Dr. Clifford Reed]: ./clifford_reed.mkd
[USS Alabama]: ../locations/mobile/uss_alabama.mkd

[The Burg Brotherhood of Steel][] led a raid against the [Enclave Mobile Base][], freeing him, his brother, and the
remaining "faulty" splicers.

[Enclave Mobile Base]: ../locations/mobile/tech_park.mkd
[The Burg Brotherhood of Steel]: ../factions/burg_bos.mkd

Years later, after moving back to his birth home at the [Station Magnolia Research Facility][], he and his brother help
[Rush Hawthorne][] and [Erik Rade][] care for their children.

He and his brother live in [Apartment C09][].

[Apartment C09]: ../locations/ph/smrf.mkd
[Erik Rade]: ./erik_rade.mkd
[Ernie Deschamp]: ./ernie_deschamp.mkd
[Rush Hawthorne]: ./rush_hawthorne.mkd
[Station Magnolia Research Facility]: ../locations/ph/smrf.mkd

### Factions and Associations

-   **Hawthorne Clan**: Beta leader _retired_, Elder
-   **Pine Harbor Traders**: Workman and part-time guard

### Family

```mermaid
%%{init: {"flowchart": {"defaultRenderer": "elk"}} }%%
graph TD
  %% NPCs %%

  john_h([John Hawthorne]):::primaryColor
  donald_r([Donald Rade]):::primaryColor
  aaron_h([Aaron Hawthorne]):::primaryColor
  rush_h([Rush Hawthorne]):::primaryColor
  erik_r([Erik Rade]):::primaryColor
  son0([Alto Hawthorne<br/><small>Adopted by Rush</small>]):::primaryColor
  son1([Marshall Rade]):::primaryColor

  mother_0([Mother 0]):::secondaryColor
  mother_1([Mother 1]):::secondaryColor
  olivia_b([<s>Olivia Brown</s>]):::secondaryColor
  emily_h([Emily Reed Hawthorne]):::secondaryColor
  catherine_sims([Catherine Sims]):::secondaryColor
  daughter([Baby girl]):::secondaryColor

  %% Generation Markers %%
  G3>Gen 3]
  G4a>Gen 4]
  G4b>Gen 4]
  G5a>Gen 5]
  G5b>Gen 5]
  G5c>Gen 5]

  john_h & mother_0 --> G3 --> aaron_h & donald_r

  mother_1 & aaron_h --> G4a --> rush_h
  donald_r & mother_1 --> G4b --> erik_r

  erik_r & olivia_b --> G5a --> son0
  catherine_sims & erik_r --> G5c --> son1

  rush_h & emily_h --> G5b --> daughter
```

## Schedule

This is Aaron Hawthorne's default schedule. These are easily overridden by quests and such.

| Day(s)  | Time | Location                    |
| :------ | ---- | --------------------------- |
| M, W, F | 0600 | [Apartment C09][]           |
| M, W, F | 0800 | [Pine Harbor Bazaar][]      |
| M, W, F | 1200 | [Pine Harbor Tres Perros][] |
| M, W, F | 1300 | [Pine Harbor Bazaar][]      |
| M, W, F | 1500 | [Apartment C09][]           |
| M, W, F | 2100 | [Apartment C09][] (Bed)     |
