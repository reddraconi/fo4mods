# Doctor Clifford Reed

Proprieter of Doc's Doughnuts in Pine Harbor

## Physical Characteristics

|                 |                  |                |          |
|:----------------|:-----------------|:---------------|:---------|
| **Age:**        | 51               | **Birthday:**  | 22 July  |
| **Height:**     | 5' 9"            | **Weight:**    | 185 lbs  |
| **Build:**      | Fat/Muscular     | **Gender:**    | Male     |
| **Ethnicity:**  | African American | **Sexuality**  | 1 Kinsey |
| **Birthplace:** |                  | **Blood Type** | A+       |

|               |                          |
|:--------------|:-------------------------|
| **Skintone:** | `[See skintones below.]` |
| **Eyes:**     | Green                    |
| **Hair:**     | Salt and Pepper          |

Older male, mulatto with piercing green eyes. Slightly pudgy belly with well-developed upper body musculature.
Short-cropped salt-and-pepper hair. Stubble and light freckling on the forehead and lower face.


## Background

Doctor Reed and his deceased brother, Norman, were raised by the Enclave to become a doctor and geneticist,
respectively. After the death of his brother, Doctor Clifford assisted the Brotherhood in freeing the splicers on the
USS Alabama.

He returned to The Burg with them as an independent proprietor and baker. He assists the Brotherhood when medical
emergencies occur and they're short-staffed. After the fall of The Burg, he followed the remainder of the Brotherhood
and the splicers to Station Magnolia.

Doctor Clifford is the proprietor of "Slocum Joe's" doughnut shop in Pine Harbor and acts as a vendor and initial way
point for the player.

### Factions and Associations

### Family

```mermaid
flowchart TD
  Q0(Family Tree)
  Q1(goes)
  Q2(here)

  Q0 --> Q1 & Q2
```

**Parents**:
: (Father) _Unknown_
: (Mother) _Unknown_

**Mate**:
: None

**Sibling(s)**:

: (Brother) Doctor Norman Reed, Deceased

**Child(ren)**:
: Mobile and USS Alabama Splicers excluding Emily Hawthorne.

## References

!["Allen Tudyk"](./img/allen_tudyk.jpg){.headshot}
