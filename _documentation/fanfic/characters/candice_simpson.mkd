# Candice "Candy" Simpson

 Bathhouse Proprietor

## Physical Characteristics

|               |            |                 |                |
|:--------------|:-----------|:----------------|:---------------|
| **Age:**      | 34         | **Build:**      | Average        |
| **Height:**   | 5' 4"      | **Weight:**     | 155 lbs        |
| **Gender:**   | Female     | **Sexuality**   | 5 Kinsey       |
| **Birthday:** | `<DD MMM>` | **Birthplace:** | `<birthplace>` |

> Additional physical characteristics go here. Include any prominent details like abnormal patterns or physical
> limitations/deformities or scarring.

|               |                                                  |
|:--------------|:-------------------------------------------------|
| **Skintone:** | Caucasian |
| **Eyes:**     | `[See eyes below. Remove list below when done.]` |
| **Hair:**     | Blonde |

Eyes:

- Amber
- Black
- Blue
- Brown
- Green
- Hazel
- Light Blue
- Light Grey
- Pink
- Purple
- Red
- Steel Grey
- Not Applicable (Robot)

> Additional hair description (fur in the case of Hawthornes, scales/flesh in > the case of anything non- or
partially-human) goes here. In the case of non-humans that need multiple colors to describe their "hair" situation,
create a list of HEX or (better yet) RGB values.

## Background

> Add some short paragraphs about the character. Enough to give you a gist of
> who they are, where they came from, and what they're doing in their specific
> location.

### Factions and Associations

### Family

```mermaid
flowchart TD
  Q0(Family Tree)
  Q1(goes)
  Q2(here)

  Q0 --> Q1 & Q2
```

**Parents**:
: (Father) Unnamed
: (Mother) Unnamed

**Mate**:
: None

**Sibling(s)**:
: None

**Child(ren)**:
: None

## References

![Caption][link_to_file]
