 `<First Name> <Last Name>`
 `<Title/Job>`

## Physical Characteristics

|                 |                           |                |                           |
|:----------------|:--------------------------|:---------------|:--------------------------|
| **Age:**        | `<age>`                   | **Birthday:**  | `<DD MMM>`                |
| **Height:**     | `<feet>`' `<inches>`"     | **Weight:**    | `<pounds>` lbs            |
| **Build:**      | `[See builds below]`      | **Gender:**    | `[See geners below]`      |
| **Ethnicity:**  | `[See ethnicities below]` | **Sexuality**  |                           |
| **Skintone:**   | `[See skintones below]`   | **Ethnicity:** | `[See ethnicities below]` |
| **Birthplace:** | `<birthplace>`            |                |                           |

> Additional physical characteristics go here. Include any prominent details like abnormal patterns or physical
> limitations/deformities or scarring.

Builds:

- Athletic
- Average
- Fat
- Medium
- Muscular
- Obese
- Thin

Genders:

- (A) Androgynous
- (F) Female
- (M) Male
- (N/A) Not Applicable (e.g., robot, etc.)

Ethnicities:

- Hawthorne
- Southerner

|               |                                                  |
|:--------------|:-------------------------------------------------|
| **Skintone:** | `[See skintones below.]`                         |
| **Eyes:**     | `[See eyes below. Remove list below when done.]` |
| **Hair:**     | `[See hair below. Remove list below when done.]` |

Skintones:

- African American
- Asian
- Caucasian
- Dark African American
- Dark Caucasian
- Latino
- Light African American
- Light Caucasian
- Tanned Caucasian

Eyes:

- Amber
- Black
- Blue
- Brown
- Green
- Hazel
- Light Blue
- Light Grey
- Pink
- Purple
- Red
- Steel Grey
- Not Applicable (Robot)

Hair Colors:

- Auburn (#9a553d)
- Blackish (#2c222b)
- Blonde (#e5cea8)
- Blonde (Alternate) (#e6cea8)
- Brown (#6f4e37)
- Coffee (#6f4e37)
- Dark Brown (#3b3024)
- Grey/Blonde (#d1d1c9)
- Light Brown (#b89778)
- Light Coffee (#a7856a)
- Medium Coffee (#6a1e42)
- Medium Grey (#b7a69e)
- Salt and Pepper (#71b35a, #b7a68e)
- Steel Grey (#b7a69e)
- Strawberry Blonde (a56b45)
- Cream (#eff5e1)
- Warm White (#debc99)

> Additional hair description (fur in the case of Hawthornes, scales/flesh in > the case of anything non- or
partially-human) goes here. In the case of non-humans that need multiple colors to describe their "hair" situation,
create a list of HEX or (better yet) RGB values.

## Background

> Add some short paragraphs about the character. Enough to give you a gist of
> who they are, where they came from, and what they're doing in their specific
> location.

### Factions and Associations

### Family

```mermaid
flowchart TD
  Q0(Family Tree)
  Q1(goes)
  Q2(here)

  Q0 --> Q1 & Q2
```

**Parents**:
: (Father) ...
: (Mother) ...

**Mate**:
: ...

**Sibling(s)**:

: (Brother) ...
: (Sister) ...

**Child(ren)**:
: (Son) ...
: (Daughter) ...

## References

![Caption][link_to_file]
