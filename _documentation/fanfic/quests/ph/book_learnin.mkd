# Book Learning

## Qualifiers/Disqualifiers

1. PC must have successfully completed
   [`Stretching Your Legs`][q(stretching)] and
   [`Manual Labor`][q(manual_labor)]

## Environment / Locations

## Quest Steps

### Step 1

## Next Quests

[q(stretching)]: ./stretching_your_legs.mkd
[q(manual_labor)]: ./manual_labor.mkd
