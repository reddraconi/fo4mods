# World Spaces

## Map Data

You can set map data for use in the PipBoy by specifying a DDS file that matches
the dimension ratio (I think...).

## "Invisible Walls"

To set invisible boundaries for a world space, do the following:

1. With your ESP loaded, go to "World" > "Regions".
2. Find your worldspace in the left-hand dropdown and select it.
3. In the right-hand pane, you'll see a grid of the occupied cells in your
   worldspace.
4. Create a new region by right-clicking in the left-hand pane and select "New
   Region". Name the region "Outer Boundary".

    > Right-click on a cell to contain the border for your module.
    > Keep right-clicking on the cells to draw a polygon. When you're nearly
    > done, just right-click and choose "Done". The polygon will
    > automatically close.

5. With the new boundary region selected, click the "Boundary Region" checkbox.

## Navmeshing

**DO THIS AFTER GETTING ALL MAJOR MESHES AND GEOMETRY IN PLACE!**

1. Open your world
2. Turn on the navmesh menu
