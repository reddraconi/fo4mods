# Modding Notes

## Graphics

- [Material Swaps](./mat_swaps.mkd)
- [Patrols and Routes](./patrols.mkd)
- [Textures](./textures.mkd)
- [World Spaces](./world_spaces.mkd)

## Models &amp; Meshes
