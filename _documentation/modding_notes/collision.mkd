# Collision

## Prerequisites

Adding collision requires:

- Blender 3.x LTS or 4.x LTS
- [NifSkope 2.0 DEV v10.01](https://github.com/eckserah/nifskope_updated/releases)
- [PyNifly Blender Plugin](https://www.nexusmods.com/fallout4/mods/52319)
- [Fallout 4/76 Updated XML for NifSkope](https://github.com/jbostrus/YT_NifSkope_XML)
- Elric (Provided as part of the Creation Kit installation under &hellip;\Fallout 4\Tools).

## Installation and Configuration

1. Extract NifSkope to a directory of your choice. (Referenced here as `$NIFSKOPE_DIR`)
2. Install Blender
3. Install the PyNifly Plugin

  - Open Blender and go to Edit > Preferences&hellip;
  - Go to 'AddOns'
    - (Blender 3.x)
    - (Blender 4.x)

4. Update the installed `nif.xml`
  - Return to your downloads folder and find the `nif.xml` you downloaded the requirements above.
  - Copy that over the existing `nif.xml` located at `$NIFSKOPE_DIR/nif.xml`.

  > **Note**: If you have NifSkope open when you do this, you'll need to close and re-open the application to
  > re-register the XML file.

## Adding Collision

This section assumes:
1. You have a `.nif` that hasn't had any work done to it in NifSkope yet
2. You have a simplified version (or segments) that you want to use as collision or collisions, saved in `.obj`
   format.

  > **Note**: The `.obj` files should have no materials or anything other than tri-tessalated geometry.

1. Open your `.nif` in NifSkope
2. Insert a new data block
  1. Right-click on the root `NiNode` and select **Block** > **Insert** > **Bethesda** > **BSXFlags**
  2. Select the new `BSXFlags`node, and go to the **Block Details** area
  3. Find the **Integer Data** field, right-click its **Value** cell and select **Flags**
  4. In the Flags dialog, select at minimum **Bit 1: Havok (2)** and **Bit 7: Articulated (128)**.
3. Return to the root `NiNode`
  1. Find it's **Name** field in 'Block Details', right-click it and update it to a more descriptive name
  2. Find its **Num Extra Data List** and increment it by one.
  3. Click the refresh button next to **Extra Data List**
  4. Expand **Extra Data List**, and in the last **Extra Data List** field, edit the 'value' to be the node ID of the
     newly-created `BSXFlags` node. (That's the number immediately to the left of the node in the **Block List**).
4. Add the collision `.obj`
  1. Click the mesh in the preview window
  2. In the file menu, click **File** > **Import** > **Import .OBJ as Collision**
  3. Click **OK** in the dialog window
  4. Navigate to your `.obj`, select it, and click **OK**
5. Update the collision properties
  1. Expand the `NiNode` root node
  2. Expand the `BSTriShape` (or `BSMeshLodTriShape`, whichever you have) node
  3. Expand the `bhkCollisionn Object` node
  4. Expand the `bhkRigidBody` node
  5. Select the `bhkNiTriStripsShape` node
  6. Go to the **Block Details** section and update the **Material** field to match the collision material you want to
     use for this object.
6. Sanitize, round one
  1. Select the root node again.
  2. In the file menu, go to **Spells** > **Sanitize** > **Reorder Blocks**
7. Update the mesh template (Optional)
  1. Select the root node again.
  2. At the bottom of the **Block List** window, there are three tabs. Select **Header**
  3. Expand **NiHeader**, then expand **Strings**, then find the line that contains `(something).bgsm`.
     Update this to point to your desired `.bgsm` file.

     > **Note**: The path to your `.bgsm` should start from the `Materials` folder from within your game
     > installation's `Data` folder. (e.g., `Materials/<author>/<mod>/static/<whatever>.bgsm`). Do not include
     > quotes.
