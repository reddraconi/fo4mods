# FO4Mods

This is a resting location for my work-in-progress and completed FO4 mods.

!!! warning "IMPORTANT"
    Unless otherwise noted, the meshes and some of the textures in this repo
    may be copied directly from the Fallout 4 archives and cannot be used
    unless you own a full and legal copy of the game!

Please see individual `README.mkd` files for more information in the
subdirectories

## Complete(_ish_)

### Maintenance Scripts

- [manage_mod](./maint_scripts/manage_mod)

This creates and destroys mods, as you see fit, including robocopy jobs to sync
your git repo with the configured Data directory.

### Mods

- [Granddad's Brew Collection](./grandads_beer_collection/README.md)

  A collection of four tasty beers and (planned liquors) from your grand 'ol
  Granddad.

  Includes warm and ice cold variants with solid and ripped labels each as well
  as static collections to make object placement a bit quicker of the beers.

  Liquors are planned into include custom whiskey, gin, and rum.

  - Release Information:
    - Version 0.1Alpha:
      - Initial release

## In Progress

- _Jumpers_

  A fancy collection of onesies for the discerning adult. Great for mechanics
  and fellow grease monkeys alike. This material swap pack includes (some
  number) different swaps. Also includes the necessary stuff to make them
  craftable after the dye recipes have been located.

- _Kinship_

  A large collection of customized and custom meshes I for my fanfic. I'm lame.
  I know. _Fight me._

  This mod uses assets from 'BadPup' and 'KrittaKitty' with permissions to add
  a new race to the swamp wastes. Two worldspaces (for now) are present.

  !!! todo ""
      So very many things. Quests, dialog, more NPCs, etc. This is still in
      early alpha with the worldspaces being inaccessable at the moment.

      If you want to test on PC, add this mod to your load order, pop in to a
      new save, open the console and type `coc PHTownCenter`.

  - Release Information:
    - Version 0.1Alpha:
      - Initial release

- _Robco Term_

  A collection of Ansible playbooks designed to be used on a Fedora installation
  with gnome version 3. This will set up a kiosk user, install
  `cool-retro-term`, and some other terminal applications to make the system
  seem _kinda_ like a RobCo terminal. I'm targeting Fedora 35&plus;.


