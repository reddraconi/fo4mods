# Granddad's Brew Collection

This small ESP is a series of matswaps, an object palette, and some collections for use in other mods. The meshes are modified copies of the standard beer bottle from vanilla Fallout 4.

Included are four refreshing beers (and soon to be liquor!) from your good ol' grandad's brewery!

## Turpentine IPA

Do you like your beer to bite back, but finish with a floral, citrusy note?
Grab one of these and quench that summertime thirst.

Works well for removing paint.

## Bucksweat Amber Ale

This beer is perfect for a beach trip, river rafting, or relaxing after a hard
day's work of ending bandits.

Great warm or ice cold.

## Pinetar Lager

A classic lager for a classic wastelander. Goes well with everything from
roasted molerat to fried tatos.

Enjoy cold for best results.

## Swamp Water Cream Stout

If you like beer as dark as a Slaver's soul, this is for you. Chewy and
chocolately with a creamy finish, you'll wonder how he packed so much flavor
into the bottle.

On second thought, it may be best *not* to ask.

## Attribution

The material swaps are all mine. The bottle meshes are modified and re-used in-game meshes -- specifically the Gwinnett beer mesh.

## Contents

The `reddraconi_grandads_beer_collection.esp` contains the following:
- Meshes
  - Grandad's Buck Sweat (Standard, Cold)
  - Grandad's Pine Tar (Standard, Cold)
  - Grandad's Swamp Water (Standard, Cold)
  - Grandad's Turpentine (Standard, Cold)
- Material Swaps
  - Empty Gwinnet's Beer Bottle -> Grandad's Buck Sweat
  - Empty Gwinnet's Beer Bottle -> Grandad's Pine Tar
  - Empty Gwinnet's Beer Bottle -> Grandad's Swamp Water
  - Empty Gwinnet's Beer Bottle -> Grandad's Turpentine
- Pack-Ins
  - Moveable Beer Crate full of Grandad's Buck Sweat
  - Moveable Beer Crate full of Grandad's Pine Tar
  - Moveable Beer Crate full of Grandad's Swamp Water
  - Moveable Beer Crate full of Grandad's Turpentine

## Re-Use

Feel free to re-use these, just drop me a line and let me know how it goes.
Please do not use these in paid content, however.
