#!/usr/bin/bash

# If we're already in a virtual environment, we need to dip out

if [ -e deactivate ]; then
  deactivate
fi

# Jump into the virtual environment

## Check if we're on Linux or Windows, then use the appropriate
## thing.
case "$(uname -sr)" in
  Linux*)
    source .nix_venv/bin/activate
    ;;
  CYGWIN*|MINGW*|MINGW32*|MSYS*)
    source .venv/Scripts/activate
    ;;
esac
# Serve docs locally
mkdocs serve
